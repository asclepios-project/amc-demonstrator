# Generated by Django 2.2.1 on 2021-04-22 08:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('researcher', '0005_approved_proposal_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='has_researcher',
            old_name='research_group',
            new_name='study',
        ),
    ]
