# Generated by Django 2.2.1 on 2021-03-12 01:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Custom_User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identifier', models.IntegerField(unique=True)),
                ('timestamp_start_shift', models.DateTimeField(null=True)),
                ('timestamp_end_shift', models.DateTimeField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Emergency_Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identifier', models.IntegerField(unique=True)),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='researcher.Custom_User')),
            ],
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identifier', models.IntegerField(unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Team_Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identifier', models.IntegerField(unique=True)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='researcher.Custom_User')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='researcher.Team')),
            ],
        ),
        migrations.CreateModel(
            name='Episode_of_care',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identifier', models.IntegerField(unique=True)),
                ('timestamp_invite', models.DateTimeField(null=True)),
                ('timestamp_start_treatment', models.DateTimeField(null=True)),
                ('timestamp_revoke', models.DateTimeField(null=True)),
                ('timestamp_revoke_extra_time', models.DateTimeField(null=True)),
                ('emergency', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='researcher.Emergency_Section')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='researcher.Team')),
            ],
        ),
    ]
