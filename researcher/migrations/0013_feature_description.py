# Generated by Django 2.2.1 on 2021-08-31 12:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('researcher', '0012_auto_20210831_1044'),
    ]

    operations = [
        migrations.AddField(
            model_name='feature',
            name='description',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
