from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.utils.safestring import mark_safe
from django.core.exceptions import PermissionDenied
from django.core import serializers
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from keycloak import KeycloakOpenID
from researcher.models import *
from login.models import *
import sys, json
from keycloak import KeycloakOpenID
from django.db.models import Q
from asclepiosapi.settings import internal_ip, default_login_url

## Researcher Views!
def check_user_type(type):
	# if (type is not 6) and (type is not 7):
	# 	raise PermissionDenied
	return

@login_required(login_url='{}'.format(default_login_url))
def home(request):	
	print(request.user.pk)
	check_user_type(request.user.user_type)
	return render(request, 'researcher/home.html')

@login_required(login_url='{}'.format(default_login_url))
#def pi_home(request):
#    return render(request, 'researcher/pi_home.html')

@login_required(login_url='{}'.format(default_login_url))
def research_studies(request):
	check_user_type(request.user.user_type)
	queue1 = Has_Researcher.objects.all().filter(researcher=request.user)	
	return render(request, 'researcher/research_studies.html', { "queue1": queue1})

@login_required(login_url='{}'.format(default_login_url))
def research_study(request, pk):
	check_user_type(request.user.user_type)
	study = Approved_Proposal.objects.get(pk=pk)
	features = Has_Approved_Feature.objects.all().filter(approved_proposal=study)
	features_id = []
	keys_id = []	
	for feature in features:
		features_id.append(feature.feature.feature_name);
		keys_id.append(feature.feature.key_ID.replace('"',''))

	trial_features = Has_Feature.objects.all()			
	return render(request, 'researcher/research_study.html', {"study":study, "features":features, "features_id" : features_id, "keys_id" : keys_id, "trial_features":trial_features, "trial":trial_features[0].complete_dataset, "internal_ip": internal_ip})


@login_required(login_url='{}'.format(default_login_url))
def data_sources(request):
	check_user_type(request.user.user_type)
	data_sources = Complete_Dataset.objects.all()	
	return render(request, 'researcher/data_sources.html', { "data_sources": data_sources})

@login_required(login_url='{}'.format(default_login_url))
def data_source(request, pk):
	check_user_type(request.user.user_type)
	data_source = get_object_or_404(Complete_Dataset, pk=pk)
	data_source_features = Has_Feature.objects.all().filter(complete_dataset=data_source)
	return render(request, 'researcher/data_source.html', { "data_source": data_source, "data_source_features":data_source_features})

#@login_required # Redirects to Keycloak Authentication when user is not logged in
#def pi_research(request, pk):
#	return render(request, 'researcher/pi_research.html')

@login_required(login_url='{}'.format(default_login_url))
def manage_research_studies(request):
	check_user_type(request.user.user_type)
	studies = Approved_Proposal.objects.all().filter(principal_investigator=request.user)
	return render(request, 'researcher/manage_research_studies.html', {"studies" : studies})

@login_required(login_url='{}'.format(default_login_url))
def manage_research_study(request, pk):	
	check_user_type(request.user.user_type)
	study = Approved_Proposal.objects.get(pk=pk)
	features = Has_Approved_Feature.objects.all().filter(approved_proposal=study)
	features_id = []
	keys_id = []
	participants_pk = []
	for feature in features:
		features_id.append(feature.feature.name);
		keys_id.append(feature.feature.key_ID)

	participants = Has_Researcher.objects.all().filter(Q(study=study), ~Q(researcher=study.principal_investigator))	
	for part in participants:
		participants_pk.append(part.researcher)	

	
	emp1 = Employee.objects.all().filter(Q(role="Researcher"))	
	researchers = Employee.objects.all().filter(Q(role="Researcher") | Q(role="Principal Investigator"),~Q(user__in=participants_pk), ~Q(user=study.principal_investigator))	

	trial_features = Has_Feature.objects.all()

	study_activities = Research_Proposal_Activity.objects.all().filter(study=study)

	i = 1
	for act in study_activities:    
		if i % 2 == 0:
			act.side = ""
		else:
			act.side = "timeline-inverted"
		i += 1        

	return render(request, 'researcher/manage_research_study.html', {"study":study, "participants" : participants, 
		"features" : features, "trial_features" : trial_features, "researchers" : researchers, "trial":trial_features[0].complete_dataset,
		"study_activities" : study_activities})

@login_required(login_url='{}'.format(default_login_url))
def functional_encryption(request):
	check_user_type(request.user.user_type)
	available_fields = Functional_Encryption.objects.all()

	parse_dict = {
	"subtract" : "Subtraction (-)",
	"sum" : "Sum (+)",
	"average" : "Average (x̅)"
	}

	for obj in available_fields:
		parse_dict[obj.translation] = obj.field_name

	previous_operations = FE_Operation.objects.all()
	for obj in previous_operations:
		obj.operation = parse_dict[obj.operation]
		obj.equation_left_side = parse_dict[obj.equation_left_side]
		obj.equation_right_side = parse_dict[obj.equation_right_side]

	return render(request, 'researcher/functional_encryption.html', {"available_fields":available_fields,
		"previous_operations":previous_operations})

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def create_operation(request):
	print("Creating operation")
	operator = request.POST.get("operator", None)
	equation_left_side = request.POST.get("equation-left-side", None)
	equation_right_side = request.POST.get("equation-right-side", None)
	
	FE_Operation.objects.create(
		operation=operator,
		operation_datetime=datetime.now(),
		equation_left_side=equation_left_side,
		equation_right_side=equation_right_side
	)
	return JsonResponse(data)	

@login_required(login_url='{}'.format(default_login_url))
def profile(request):	
	return render(request, 'researcher/profile.html')

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def add_to_team(request, pk):
	study = Approved_Proposal.objects.get(pk=pk)
	data = {"success" : "200"}	
	user = User.objects.get(pk=request.POST["id"])
	new_participant = Has_Researcher.objects.create(
		study=study,
		researcher=user
	)

	study_activity = Research_Proposal_Activity.objects.create(study=study,
		action_type=2,
		title="Member added to the group",
		message="{} {}".format(user.employee.name, user.employee.surname),
		employee=request.user.employee,
		activity_date=datetime.now(),
	)
	return JsonResponse(data)

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def delete_from_team(request, pk):	
	study = Approved_Proposal.objects.get(pk=pk)	
	user = User.objects.get(pk=request.POST["id"])	
	participant_instance = Has_Researcher.objects.all().filter(researcher=user, study=study)	
	participant_instance[0].delete()
	data = {"success" : "200"}

	study_activity = Research_Proposal_Activity.objects.create(study=study,
		action_type=3,
		title="Member removed from the group",
		message="{} {}".format(user.employee.name, user.employee.surname),
		employee=request.user.employee,
		activity_date=datetime.now(),
	)
	return JsonResponse(data)

@login_required(login_url='{}'.format(default_login_url))
def recent_activities(request):
	return render(request, 'researcher/home.html')

@login_required(login_url='{}'.format(default_login_url))
def data_download(request):
    patient = get_object_or_404(Patient_Profile, pk=request.user.patient_profile.pk)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="research_dataset.csv"'

    writer = csv.writer(response, delimiter=',')
    writer.writerow(['Condition Name', 'Condition Status', 'Patient Age'])

    writer.writerow(["teste", "teste", "teste"])

    return response

@login_required(login_url='{}'.format(default_login_url))
def logout(request):

	return render(request, 'researcher/home.html')