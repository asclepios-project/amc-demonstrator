from django.urls import path
from . import views

urlpatterns = [
    #path('', views.login, name='login'),
    path('home/', views.home, name='home'),
    path('home/profile/', views.profile, name='profile'),
    path('home/functional-encryption/', views.functional_encryption, name='functional_encryption'),
    path('home/research-studies/', views.research_studies, name='research_studies'),
    path('home/research-studies/<int:pk>/', views.research_study, name='research_study'),
    path('home/data-sources/', views.data_sources, name='data_sources'),
    path('home/data-sources/<int:pk>/', views.data_source, name='data_source'),
    path('home/research-studies/<int:pk>/', views.research_study, name='research_study'),
    path('home/manage-research-studies/', views.manage_research_studies, name='manage_research_studies'),
    path('home/manage-research-studies/<int:pk>/', views.manage_research_study, name='manage_research_study'),
    path('home/manage-research-studies/<int:pk>/add_to_team', views.add_to_team, name='add_to_team'),
    path('home/manage-research-studies/<int:pk>/delete_from_team', views.delete_from_team, name='delete_from_team'),
    path('home/profile/', views.profile, name='profile'),
    path('home/recent-activities/', views.recent_activities, name='recent_activities'),
    path('home/functional-encryption/', views.functional_encryption, name='functional_encryption'),
    path('home/functional-encryption/create-operation', views.create_operation, name='create_operation'),
]