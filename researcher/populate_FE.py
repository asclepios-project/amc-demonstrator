from researcher.models import *

#if __name__ == "__main__":
print("Populating available fields for FE...")
Functional_Encryption.objects.all().delete()
FE_Operation.objects.all().delete()

available_fields = [("Ambulance joined session","start_datetime"), ("Ambulance started care", "time_of_arrival"), ("Ambulance delivered at hospital","end_datetime")]

for field in available_fields:
	Functional_Encryption.objects.create(
		field_name=field[0],
		translation=field[1]
)