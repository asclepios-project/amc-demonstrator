function add_to_team(btn){
    //console.log(btn.id);
    var answer = window.confirm("Do you really want to add to this research proposal?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'add_to_team',
        data: { id: btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function() {
            location.reload(true);
        }
      });
    }
}

function delete_from_team(btn){
    //console.log(btn.id);
    var answer = window.confirm("Do you really want to remove from this research proposal?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'delete_from_team',
        data: { id: btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function() {
            location.reload(true);
        }
      });
    }
}