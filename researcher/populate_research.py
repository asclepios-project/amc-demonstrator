from researcher.models import *
from login.models import User, Employee, Research_Proposal_Activity
from datetime import datetime, timedelta
from keycloak import KeycloakOpenID
from faker import Faker
from faker.providers import internet
from tqdm import tqdm
fake = Faker('nl_NL')
fake.add_provider(internet)
import requests, string, names, random, time, warnings, secrets, json
from asclepiosapi.settings import internal_ip

class Populate_Researcher_Use_Case():
	## params
	number_of_pis = 3
	number_of_researchers = 5
	number_of_studies = 5	

	studies = ["Stroke Care", "Sleep Medicine"]
	researchers = []
	pis = []
	features = []

	keys = {}

	principal_investigators = {
		"silvia-pi" : ["Silvia", "Olabarriaga"],
		"diogo-pi" : ["Diogo", "Ferrazani"],
		"henk-pi" : ["Henk", "Marquering"]
	}

	principal_investigators_keys = principal_investigators.keys()

	researchs = {
		"marcela-r" : ["Marcela", "Tuler"],
		"lucio-r" : ["Lucio", "Reis"],
		"tom-r" : ["Tom", "Doe"],
		"lucas-r" : ["Lucas", "Silva"],
		"matilde-r" : ["Matilde", "Castro"],
		"yago-r" : ["Yago", "Rezende"]
	}

	researchs_keys = researchs.keys()

	complete_features = {
		"patient_age" : "Patient age",
		"patient_gender" : "Patient gender",
		"patient_blood_type" : "Patient blood type",
		"time_of_arrival" : "Time of arrival",
		"heart_rate" : "Heart rate (bpm)",
		"oxygen_saturation" : "Oxygen saturation (%)",
		"glucose" : "Glucose (mmol/L)",
		"blood_pressure_systolic" : "Blood pressure systolic (mmHg)",
		"blood_pressure_diastolic" : "Blood pressure diastolic (mmHg)",
		"breathing_frequency" : "Breathing Frequency (breathes per minute)",
		"emv_score_eyes" : "EMV score eyes",
		"emv_score_motor" : "EMV score motors",
		"emv_score_verbal" : "EMV score verbal",
		"time_of_patient_deliver" : "Time of patient deliver at hospital",
	}

	complete_features_description = {
		"patient_age" : "Age of the patient.",
		"patient_gender" : "Gender of the patient.",
		"patient_blood_type" : "Blood type of the patient.",
		"time_of_arrival" : "Time of ambulance arrival at patient's location.",
		"heart_rate" : "Heart rate (bpm) measured by the ambulance.",
		"oxygen_saturation" : "Oxygen saturation (%) measured by the ambulance.",
		"glucose" : "Glucose (mmol/L) measured by the ambulance.",
		"breathing_frequency" : "Breathing frequency (breathes per minute) measured by the ambulance.",
		"blood_pressure_systolic" : "Blood pressure systolic (mmHg) measured by the ambulance.",
		"blood_pressure_diastolic" : "Blood pressure diastolic (mmHg) measured by the ambulance.",
		"emv_score_eyes" : "EMV score eyes measured by the ambulance.",
		"emv_score_motor" : "EMV score motors measured by the ambulance.",
		"emv_score_verbal" : "EMV score verbal measured by the ambulance.",
		"time_of_patient_deliver" : "Time of the patient deliver at the hospital by the ambulance.",
	}

	def __init__(self):
		print("Initiating class")
		self.init_and_configure_keycloak()		
		return

	# Generates a new SALT with random and string
	def generate_salt(self, N=6):
		return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))
        

	def init_and_configure_keycloak(self):	
		# Configure client
		keycloak_openid = KeycloakOpenID(server_url="http://{}:8181/auth/".format(internal_ip),
		    client_id="amc-client",
		    realm_name="master")
		# keycloak_openid = KeycloakOpenID(server_url="http://{}/auth/".format(internal_ip),
		#     client_id="amc-client",
		#     realm_name="master")

		# Get Token
		token = keycloak_openid.token("amcadmin", "amcadmin")
		userinfo = keycloak_openid.userinfo(token['access_token'])

		# defining the api-endpoint 
		self.registration_authority_endpoint = "http://{}:8083/api/v1/auth/".format(internal_ip)
		self.cpabe_endpoint = "http://{}:8084/api/v1/put".format(internal_ip)
		self.cpabe_policy_endpoint = "http://{}:8084/api/v1/setpolicy".format(internal_ip)
		# self.registration_authority_endpoint = "http://{}/ra/api/v1/auth/".format(internal_ip)
		# self.cpabe_endpoint = "http://{}/cpabe/api/v1/put".format(internal_ip)
		# self.cpabe_policy_endpoint = "http://{}/cpabe/api/v1/setpolicy".format(internal_ip)
		self.headers = {
		    'content-type': 'application/json',
		    'Authorization' : 'Bearer {}'.format(token["access_token"])
		}
		return

	def create_principal_investigator(self):		
		for i in tqdm(range(self.number_of_pis)):
			username = random.choice(list(self.principal_investigators_keys))
			first_name = self.principal_investigators[username][0]
			last_name = self.principal_investigators[username][1]
			# salt = self.generate_salt()
			# username = first_name+last_name+"-"+salt
			email = first_name+last_name+"@researcher-pi.com"
			del self.principal_investigators[username]
			# data example.
			data = {
				"firstName":first_name,
				"lastName":last_name,
				"email":email,
				"username":username,		
				"enabled":"true",
				"keys":["role"],
				"values":["researcher-pi"],
				"realmRoles":[],
				#"credentials":[{"type":"password","value":"pwd123","temporary":"false"}]
			}

			self.create_user_using_registration_authority(self.headers, data)
			self.create_user_django(first_name, last_name, username, email, pi=True)
			self.pis.append(username)
		return

	def create_researchers(self):
		for i in tqdm(range(self.number_of_pis)):
			username = random.choice(list(self.researchs_keys))
			first_name = self.researchs[username][0]
			last_name = self.researchs[username][1]
			# salt = self.generate_salt()
			# username = first_name+last_name+"-"+salt
			email = first_name+last_name+"@researcher.com"
			del self.researchs[username]
			# data example.
			data = {
				"firstName":first_name,
				"lastName":last_name,
				"email":email,
				"username":username,		
				"enabled":"true",
				"keys":["role"],
				"values":["researcher"],
				"realmRoles":[],
				#"credentials":[{"type":"password","value":"pwd123","temporary":"false"}]
			}

			self.create_user_using_registration_authority(self.headers, data)
			self.create_user_django(first_name, last_name, username, email, pi=False)
			self.researchers.append(username)
		return	

	def create_study(self):
		for i in tqdm(range(len(self.studies))):
			user = User.objects.get(username=random.choice(self.pis))
			study = Approved_Proposal.objects.create(
				name=self.studies[i],			
				principal_investigator=user,
				start_datetime=datetime.now(),
				end_datetime=datetime.now(),
				status="Active",
			)

			self.create_activity_log(1, study, "Research study created", "", user.employee)

			Has_Researcher.objects.create(
				researcher=user,
				study=study
			)			

			self.add_approved_features(study, 6)
			self.add_researchers_to_study(study, 2)
		return

	def add_approved_features(self, study, n):
		for i in range(n):
			Has_Approved_Feature.objects.create(
				approved_proposal=study,
				feature=self.features[i]
			)

		# for feature in self.features:
		# 	Has_Approved_Feature.objects.create(
		# 		approved_proposal=study,
		# 		feature=feature
		# 	)
		return

	def add_researchers_to_study(self, study, n):
		for i in range(n):
			user = User.objects.get(username=self.researchers[i])
			Has_Researcher.objects.create(
				researcher=user,
				study=study
			)

			self.create_activity_log(1, study, "Member added to the group", "{} {}".format(user.employee.name, user.employee.surname), None)
		# for r in self.researchers:
		# 	user = User.objects.get(username=r)
		# 	Has_Researcher.objects.create(
		# 		researcher=user,
		# 		study=study
		# 	)
		return

	def create_user_using_registration_authority(self, headers, data):
		r = requests.post(url=(self.registration_authority_endpoint+"add-user"), headers=headers, json=data)
		#print(headers, data)
		return

	def create_user_django(self, first_name, last_name, username, email, pi):		
		if pi:
			user = User.objects.create_user(username=username,
				email=email,
				password="!asclepios!",
				user_type=7
			)
			self.create_employee_profile(user, first_name, last_name, email, "Principal Investigator")
		else:
			user = User.objects.create_user(username=username,
				email=email,
				password="!asclepios!",
				user_type=6
			)
			self.create_employee_profile(user, first_name, last_name, email, "Researcher")		
		return

	def create_employee_profile(self, user, first_name, last_name, email, role):
		employee_profile = Employee.objects.create(
			user=user,			
			name=first_name,
			surname=last_name,
			email=email,
			role=role,
			department="Research"
		)
		return

	def create_trial(self):
		complete_data = Complete_Dataset.objects.create(			
			trial_ID=1,
			start_datetime=datetime.now() - timedelta(days=800),
			end_datetime=datetime.now(),
			dataset_name="Acute Stroke Care"
		)

		for feature in self.features:
			Has_Feature.objects.create(
				complete_dataset=complete_data,
				feature=feature
			)

		return

	def create_features(self):

		raw_policy = 'role:researcher role:researcher-pi 1of2'
		r = requests.post(url=(self.cpabe_policy_endpoint), headers=self.headers, data=raw_policy)

		for i in tqdm(range(len(self.complete_features))):
			feature = random.choice(list(self.complete_features.keys()))			
			new_feature = Feature.objects.create(				
				feature_name=feature,
				name=self.complete_features[feature],
				description=self.complete_features_description[feature]
			)
			self.features.append(new_feature)

			verKey = secrets.token_hex(16)
			encKey = secrets.token_hex(16)		
			data = {
				"verKey": verKey,
				"encKey": encKey
				}		
			r = requests.post(url=(self.cpabe_endpoint), headers=self.headers, json=data)		
			keyid = r.content.decode("utf-8")
						
			new_feature.key_ID=keyid
			new_feature.save()

			self.keys[feature] = {"keyid" : keyid,
								"verKey" : verKey,
								"encKey" : encKey}

			del self.complete_features[feature]
		return

	def create_activity_log(self, action_type, study, title, message, employee):
		study_activity = Research_Proposal_Activity.objects.create(			
			study=study,
			action_type=2,
			title=title,
			message=message,
			employee=employee,
			activity_date=datetime.now(),
		)
		return

	def download_features_and_keys_ids(self):
		with open("features_keys.json", "w") as outfile:
			json.dump(self.keys, outfile)
		return		

	def populate_FE(self):		
		Functional_Encryption.objects.all().delete()
		FE_Operation.objects.all().delete()

		available_fields = [("Ambulance joined session","start_datetime"), ("Ambulance started care", "time_of_arrival"), ("Ambulance delivered at hospital","end_datetime")]

		for field in available_fields:
			Functional_Encryption.objects.create(
				field_name=field[0],
				translation=field[1]
		)
		return

def populate_all():
	print("Starting script to populate research and functional encryption!")
	Approved_Proposal.objects.all().delete()
	Has_Researcher.objects.all().delete()
	Feature.objects.all().delete()
	Complete_Dataset.objects.all().delete()
	User.objects.all().delete()	
	populate_research = Populate_Researcher_Use_Case()

	print("\n Creating complete features...")
	populate_research.create_features()

	print("\n Creating trial dataset with all features...")
	populate_research.create_trial()

	print("\n Creating principal investigators..")
	populate_research.create_principal_investigator()

	print("\n Creating researchers...")
	populate_research.create_researchers()

	print("\n Creating study...") 
	populate_research.create_study()

	print("\n Download feature keys...")
	populate_research.download_features_and_keys_ids()

	#if __name__ == "__main__":
	print("\n Populating available fields for Functional Encryption...")
	populate_research.populate_FE()
	
	print("\nScript terminated\n")	

	return
if __name__ == "__main__":
	populate_all()