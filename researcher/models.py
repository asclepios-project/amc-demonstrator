from django.db import models
from django.conf import settings
User = settings.AUTH_USER_MODEL

# AC-ABAC models, This model will just serve the getinfo endpoint of this application

#class Research_Group(models.Model):
#	identifier = models.IntegerField(unique=True)
#	principal_investigator = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

class Approved_Proposal(models.Model):
	identifier = models.IntegerField(null=True, unique=True)
	name = models.CharField(null=True, max_length=100)
	principal_investigator = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	#research_group = models.ForeignKey(Research_Group, on_delete=models.CASCADE, null=True)
	start_datetime = models.DateField(null=True)
	end_datetime = models.DateField(null=True)
	status = models.CharField(null=True, max_length=100)

class Has_Researcher(models.Model):
	identifier = models.IntegerField(null=True, unique=True)
	researcher = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	study = models.ForeignKey(Approved_Proposal, on_delete=models.CASCADE, null=True)

class Feature(models.Model):
	identifier = models.IntegerField(null=True, unique=True)
	feature_name = models.CharField(null=True, max_length=100)
	name = models.CharField(null=True, max_length=100)
	description = models.CharField(null=True, max_length=200)
	key_ID =models.CharField(null=True, max_length=100)
	
class Has_Approved_Feature(models.Model):
	identifier = models.IntegerField(null=True, unique=True)
	approved_proposal = models.ForeignKey(Approved_Proposal, on_delete=models.CASCADE, null=True)
	feature = models.ForeignKey(Feature, on_delete=models.CASCADE, null=True)

#class Has_Approved_Proposal(models.Model):
#	identifier = models.IntegerField(unique=True)
#	researcher = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
#	approved_proposal = models.ForeignKey(Approved_Proposal, on_delete=models.CASCADE, null=True)
#

class Complete_Dataset(models.Model):
	identifier = models.IntegerField(unique=True, null=True)
	trial_ID = models.IntegerField(unique=True, null=True)
	dataset_name = models.CharField(null=True, max_length=100)
	start_datetime = models.DateField(null=True)
	end_datetime = models.DateField(null=True)

class Has_Feature(models.Model):
	identifier = models.IntegerField(unique=True, null=True)
	complete_dataset = models.ForeignKey(Complete_Dataset, on_delete=models.CASCADE, null=True)
	feature = models.ForeignKey(Feature, on_delete=models.CASCADE, null=True)

class Functional_Encryption(models.Model):
    field_name = models.CharField(max_length=200, null=True)
    translation = models.CharField(max_length=200, null=True)

class FE_Operation(models.Model):
	operation = models.CharField(max_length=200, null=True)
	operation_datetime = models.DateField(null=True)
	equation_left_side = models.CharField(max_length=200, null=True)
	equation_right_side = models.CharField(max_length=200, null=True)