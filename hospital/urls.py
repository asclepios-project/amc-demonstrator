from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_login, name='index_login'),
    path('home/', views.home, name='home'),
    path('home/check_new_eoc', views.check_new_eoc, name='check_new_eoc'),
    path('home/logout', views.logout, name='logout'),
    path('home/Profile/', views.profile, name='profile'),
    path('home/Emergency-Session/', views.emergency_sessions, name='emergency_sessions'),
    path('home/Emergency-Session/<int:id>/', views.emergency_session, name='emergency_session'),
    path('home/Emergency-Session/<int:id>/update_episode_of_care_info', views.update_episode_of_care_info, name='update_episode_of_care_info'),
    path('home/Emergency-Session/<int:id>/checkin_patient', views.checkin_patient, name='checkin_patient'),
    path('home/Emergency-Session/<int:id>/check_permission', views.es_check_permission, name='es_check_permission'),
    path('home/Emergency-Session/<int:id>/Summary/<int:enc>/', views.old_eps_view, name='old_eps_view'),
    path('home/Emergency-Session/<int:id>/get_eps', views.get_eps, name='get_eps'),
    path('home/Emergency-Session/<int:id>/end_session', views.end_session, name='end_session'),
    path('home/Emergency-Session/<int:id>/resq_transference', views.resq_transference, name='resq_transference'),
    path('home/Team/', views.teams, name='teams'),
    path('home/Team/<int:id>/', views.team_instance, name='team_instance'),
    path('home/Team/<int:id>/add_to_team', views.add_to_team, name='add_to_team'),
]