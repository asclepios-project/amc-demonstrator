from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth import logout as django_logout
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION, ContentType
from django.http import HttpResponseForbidden
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from login.models import *
from login.forms import *
from django.http import JsonResponse
from django.http import HttpResponse
from datetime import datetime
from django.core import serializers
import json
from django.core.files.storage import default_storage
import pydicom
import io, base64, urllib
import matplotlib.pyplot as plt
from django.utils.http import urlencode
from asclepiosapi.settings import internal_ip, default_login_url
from asclepiosapi import settings
import pytz

local_tz = 'Europe/Amsterdam'
# pytz.timezone(local_tz)
#############################################################################
################################## Login ####################################
@login_required(login_url='{}'.format(default_login_url))
def index_login(request):
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(username=username, password=password)

        if user is not None:
            print(user.user_type)
            if user.user_type is not 4:
                return HttpResponseForbidden()
            auth_login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect("/hospital/home")
        else:
            return render(request, 'hospital/login.html')
    else:
        return render(request, 'hospital/login.html')
    return render(request, 'hospital/login.html')

##############################################################################
################################## Home ######################################
@login_required(login_url='{}'.format(default_login_url))
def home(request):
    # usr = User.objects.get(pk=request.user.pk)
    # usr.user_type = 4
    # usr.save()
    if request.user.user_type is not 4:
        raise PermissionDenied
    return render(request, 'hospital/home.html')

###############################################################################
################################## Profile ####################################
@login_required(login_url='{}'.format(default_login_url))
def profile(request):
    if request.user.user_type is not 4:
        raise PermissionDenied
    return render(request, 'hospital/profile.html')

###############################################################################
################################## Emergency Session ##########################
@login_required(login_url='{}'.format(default_login_url))
def emergency_sessions(request):
    if request.user.user_type is not 4:
        raise PermissionDenied
    
    teams = Care_Team.objects.filter(is_active=True)
    teams_part_of = Care_Team_Participants.objects.filter(participant_id=request.user.employee, identifier__in=teams)

    teams_po = []

    for team in teams:
        for t in teams_part_of:
            if team == t.identifier:
                teams_po.append(team)
                break
    
    ongoing_sessions = Episode_Of_Care.objects.filter(care_team__in=teams_po)

    for os in ongoing_sessions:
        os.enc_id = os.encounter.pk
        if os.encounter.end_datetime is not None:
            os.status = "Access revoked"
            os.status_color = "red"
            os.action = "No action allowed"
            os.bttn_color = "grey"
            os.ac = False

        elif os.end_datetime != None:
            os.status = "Access revoked"
            os.status_color = "red"
            os.action = "No action allowed"
            os.bttn_color = "grey"
            os.ac = False

        else:
            os.status = "Active"
            os.status_color = "green"
            os.action = "Enter session"
            os.bttn_color = "#4679bd"
            os.ac = True
        #os.pat_name = os.episode_of_care_encounter.encounter_patient.patient_name + " " + os.episode_of_care_encounter.encounter_patient.patient_surname

    
    return render(request, 'hospital/emergency_sessions.html',{
        "sessions" : ongoing_sessions
    })

@login_required(login_url='{}'.format(default_login_url))
def emergency_session(request, id=None):
    if request.user.user_type is not 4:
        raise PermissionDenied
    
    encounter = get_object_or_404(Encounter, pk=id)
    if encounter.end_datetime is not None:
        raise PermissionDenied

    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

    # Old Encounters
    encounters = Encounter.objects.filter(~Q(pk=encounter.pk), patient=encounter.patient).order_by('start_datetime', )
    for obj in encounters:
        obj.episodes = Episode_Of_Care.objects.filter(encounter=obj)
        #obj.episodes = reminder = serializers.serialize("json", Encounter.objects.all())
        obj.date = str(obj.start_datetime)[0:10]
        obj.eps = len(obj.episodes)

    #reminder = serializers.serialize("json", Encounter.objects.all())



    # Olds Episode of care
    episodes_of_care = Episode_Of_Care.objects.filter(encounter=encounter).order_by('start_datetime')

    if len(episodes_of_care) == 1:
        call_centre_eoc = None
        ambulance_eoc = None
    else:
        call_centre_eoc = episodes_of_care[0]
        ambulance_eoc = episodes_of_care[1]
    episodes_of_care = episodes_of_care[0:(len(episodes_of_care)-1)]

    # This Episode of Care
    try:
        ep = get_object_or_404(Episode_Of_Care, encounter=encounter, care_team__in=teams)
    except:
        eps = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
        ep = eps[len(eps)-1]
    ep.visualized = True
    ep.save()
    
    imuri = None
    dicom_dict = {}
    if ep.radiological_imaging != None:
        dicom = default_storage.open(ep.radiological_imaging)
        dataset = pydicom.dcmread(dicom)
        fill_dict(dataset, dicom_dict)
        buffer = io.BytesIO()
        fig = plt.gcf()
        fig.set_facecolor('black')
        plt.imshow(dataset.pixel_array, cmap='gray')
        plt.axis('off')
        plt.savefig(buffer, format='png', dpi=200, facecolor=fig.get_facecolor(), transparent=True)
        plt.colorbar()
        plt.close()

        imsrc = base64.b64encode(buffer.getvalue())
        imuri = 'data:image/png;base64,{}'.format(urllib.parse.quote(imsrc))
        

    if request.method == "GET":
        episode_of_care_form = Episode_Of_Care_Hospital_Form(instance=ep)
    if request.method == "POST":
        dicom = request.FILES.get('myfile', False)

        episode_of_care_form = Episode_Of_Care_Hospital_Form(request.POST, instance=ep)
        if episode_of_care_form.is_valid():
            x = episode_of_care_form.save()
            if ep.radiological_imaging is None:
                if dicom is not False:
                    x.radiological_imaging = dicom.name
                    x.save()
                    file_name = default_storage.save(dicom.name, dicom)

    # Patient history
    allergies = Allergy_Intolerance.objects.filter(subject=encounter.patient)
    conditions = Condition.objects.filter(subject=encounter.patient)
    fmh = Family_Member_History.objects.filter(subject=encounter.patient)

    # Organizations
    ambulance_orgs = Organization.objects.filter(~Q(type=3),~Q(type=4),~Q(pk=request.user.employee.org.pk))
    hospital_orgs = Organization.objects.filter(~Q(type=3),~Q(type=2),~Q(pk=request.user.employee.org.pk))

    #req = Request.objects.filter(encounter=encounter)
    encounter.episodes_of_care = Episode_Of_Care.objects.filter(encounter=encounter)
    encounter.date = str(encounter.start_datetime)[0:10]
    
    discharge_request = Request.objects.filter(request_type=3, encounter=encounter, org_requesting=request.user.employee.org)
    if not discharge_request:
        discharge = False
    else:
        discharge = True

    transference_request = Request.objects.filter(request_type=2, encounter=encounter, org_requesting=request.user.employee.org)
    if not transference_request:
        transference = False
    else:
        transference = True    

    return render(request, 'hospital/emergency_session.html',{
        "encounter" : encounter,
        "encounters" : encounters,
        "episode_of_care_form" : episode_of_care_form,
        "allergies" : allergies,
        "conditions" : conditions,
        "fmh": fmh,
        "hospital_orgs" : hospital_orgs,
        "ambulance_orgs" : ambulance_orgs,
        "discharge_request" : discharge,
        "transference_request" : transference,
        "episodes" : episodes_of_care,
        #"call_centre_remarks" : call_centre_eoc,
        #"ambulance_remarks" : ambulance_eoc,
        "ep": ep,
#        "reminder" : reminder,
        # "imuri" : imuri,
        # "dicom_dict" : dicom_dict,
        "keyid" : encounter.patient.key_id,
        "patient" : encounter.patient.pk,
        "updated" : ep.updated,
        "json_id" : ep.json_id,
        "internal_ip" : internal_ip,
        "organisation_name" : "{} ({})".format(ep.managing_organization.name, ep.managing_organization.alias),
        "organisation_address" : "{}, {}, {}".format(ep.managing_organization.address, ep.managing_organization.zipcode, ep.managing_organization.country),
        "organisation_phone" : ep.managing_organization.telecom,
        "ep_start_datetime" : str(ep.start_datetime),
        "ep_accepted_datetime" : str(ep.accepted_date),
        "ep_end_datetime" : str(ep.end_datetime),
    })
   
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def update_episode_of_care_info(request, id=None):    
    encounter = get_object_or_404(Encounter, pk=id)

    if request.method == "POST":
        print("updating ep info")
        encounter = get_object_or_404(Encounter, pk=id)
        json_id = request.POST["json_id"]
        
        care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
        teams = []
        for team in care_teams_part:
            teams.append(team.identifier)

        try:
            ep = get_object_or_404(Episode_Of_Care, encounter=encounter, care_team__in=teams)
        except:
            eps = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
            ep = eps[len(eps)-1]

        ep.json_id = json_id
        ep.updated = True
        ep.save()

        return JsonResponse({})

def fill_dict(dataset, dicom_dict):
    indent=0
    dont_print = ['Pixel Data', 'File Meta Information Version']

    indent_string = "   " * indent
    next_indent_string = "   " * (indent + 1)

    for data_element in dataset:
        if data_element.name in dont_print:
            print("""<item not printed -- in the "don't print" list>""")
        else:
            repr_value = repr(data_element.value)
            if len(repr_value) > 50:
                repr_value = repr_value[:50] + "..."
            print("{0:s} {1:s} = {2:s}".format(indent_string,
                                                data_element.name,
                                                repr_value))
            dicom_dict[data_element.name] = repr_value

############################################################################################
################################# Check prmission ############################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def es_check_permission(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)
    permit = True

    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

     # This Episode of Care
    try:
        ep = get_object_or_404(Episode_Of_Care, encounter=encounter, care_team__in=teams)
    except:
        eps = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
        ep = eps[len(eps)-1]

    if ep.end_datetime != None:
        permit = False

    response = {
        "success" : 200,
        "permit" : permit
    }
    return JsonResponse(response)

##############################################################################################
#########################  Check new episodes  ###############################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def check_new_eoc(request):
    new = False
    care_teams_part =Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

    eps = Episode_Of_Care.objects.filter(care_team__in=teams)
    for ep in eps:
        print(ep.visualized)
        if ep.visualized == False:
            new = True

    response = {
        "sucess" : 200,
        "new" : new
    }
    return JsonResponse(response)

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def end_session(request, id=None):
    if request.user.user_type is not 4:
        raise PermissionDenied
    
    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=id)
        request = Request.objects.create(
            encounter=encounter,
            request_type=3,
            org_requesting=request.user.employee.org,
            org_requested=request.user.employee.org,
            answered=False,
            request_date=datetime.now(pytz.timezone(local_tz))
        )
        return JsonResponse({})

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def add_to_team(request, id=None):
    if request.user.user_type is not 4:
        raise PermissionDenied
    
    if request.method == "POST":
        team = get_object_or_404(Care_Team, pk=id)
        employee = get_object_or_404(Employee, pk=request.POST["employee_id"])
        ctp = Care_Team_Participants.objects.create(
            care_team_id=team,
            participant_id=employee,
            participant_role=employee.role,
            added_date=datetime.now(pytz.timezone(local_tz))
        )

        team_activities = Team_Activities.objects.create(
            care_team=team,
            action_type=2,
            title="Member added to the team",
            message="{} {}".format(employee.name, employee.surname),
            activity_date=datetime.now(pytz.timezone(local_tz))
        )
        return JsonResponse({})

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def resq_transference(request, id=None):
    if request.user.user_type is not 4:
        raise PermissionDenied
    

    if request.method == "POST":
        org = get_object_or_404(Organization, pk=request.POST["org_id"])
        encounter = get_object_or_404(Encounter, pk=id)
        request = Request.objects.create(
            encounter=encounter,
            request_type=2,
            org_requesting=request.user.employee.org,
            org_requested=request.user.employee.org,
            transfer_to=org,
            answered=False,
            request_date=datetime.now(pytz.timezone(local_tz))
        )
        return JsonResponse({})

###############################################################################
################################## Teams ######################################
@login_required(login_url='{}'.format(default_login_url))
def teams(request):
    if request.user.user_type is not 4:
        raise PermissionDenied

    care_team_response = []

    filters = {
        "identifier" :request.GET.get('identifier'),
    }
    
    if filters["identifier"] != None and len(filters["identifier"]) > 3:
        try:
            care_team_response = Care_Team.objects.filter(identifier=filters["identifier"])
        except:
            pass
    
    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    

    for team in care_teams_part:
        if team.identifier.is_active:
            team.status = "Active"
            team.color = "green"
        else:
            team.status = "Inactive"
            team.color = "red"
        team.created_date = str(team.identifier.start_datetime)[0:10]
    
    for care_team in care_team_response:
        care_team.created_date = str(care_team.start_datetime)[0:10]
        if care_team.is_active:
            care_team.status = "Active"
            care_team.color = "green"
        else:
            care_team.status = "Inactive"
            care_team.color = "red"

    return render(request, 'hospital/teams.html', {"filters" : filters,
        "care_teams" : care_teams_part,
        "care_team_response" : care_team_response
    })

@login_required(login_url='{}'.format(default_login_url))
def team_instance(request, id=None):
    if request.user.user_type is not 4:
        raise PermissionDenied

    employees = Employee.objects.filter(Q(org=request.user.employee.org),~Q(pk=request.user.employee.pk))
    
    care_team = get_object_or_404(Care_Team, pk=id)
    participants = Care_Team_Participants.objects.filter(identifier=care_team)


    activities = Team_Activities.objects.filter(care_team=care_team)

    i = 1
    for act in activities:
        ###
        if i % 2 == 0:
            act.side = ""
        else:
            act.side = "timeline-inverted"
        i += 1
        ###  

    return render(request, 'hospital/team_instance.html', {
        "care_team" : care_team,
        "participants" : participants,
        "employees": employees,
        "activities" : activities
    })

###############################################################################
#########################  Logout  ############################################
@login_required(login_url='{}'.format(default_login_url))
def logout(request):
    logout_url = settings.OIDC_OP_LOGOUT_ENDPOINT
    return_to_url = request.build_absolute_uri(settings.LOGOUT_REDIRECT_URL)
    django_logout(request)
    return redirect(logout_url + '?' + urlencode({'redirect_uri': return_to_url, 'client_id': settings.OIDC_RP_CLIENT_ID}))







##########################################################################
#########################  Hospital Team  ################################
@login_required(login_url='{}'.format(default_login_url))
def hospital_team(request):
    print("Hospital Team")
    return render(request, 'main/hospital_team/hospital_team.html')

@login_required(login_url='{}'.format(default_login_url))
def hosp_emergency_sessions(request):
    print("Hospital Team Emergency Session")

    care_teams = []
    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user)

    for team in care_teams_part:
        if team.care_team_id.care_team_is_active:
            care_teams.append(team.care_team_id)
    
    non_accepted_eoc_resp = []
    accepted_eoc_resp = []

    for teams in care_teams:
        non_accepted_eoc = Episode_Of_Care.objects.filter(episode_of_care_team=teams, episode_of_care_accepted=False)
        accepted_eoc = Episode_Of_Care.objects.filter(episode_of_care_team=teams, episode_of_care_accepted=True)

        if len(non_accepted_eoc) > 0:
            for eoc in non_accepted_eoc:
                non_accepted_eoc_resp.append(eoc)

        if len(accepted_eoc) > 0:
            for eoc in accepted_eoc:
                accepted_eoc_resp.append(eoc)
    
    return render(request, 'main/hospital_team/emergency_sessions.html', {
        "non_accepted" : non_accepted_eoc_resp,
        "accepted" : accepted_eoc_resp,
    })

##############################################################################################
#########################  GET Episodes  #####################################################
def old_eps_view(request, id=None, enc=None):
    encounter = get_object_or_404(Encounter, pk=enc)
    # eps = Episode_Of_Care.objects.filter(episode_of_care_encounter=encounter)    
    return render(request, 'ambulance/episodes_summary.html', 
        {"internal_ip" : internal_ip, "keyid" : encounter.patient.key_id, "encounter" : encounter})


@login_required(login_url='{}'.format(default_login_url))
def hosp_emergency_session(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)
    print(encounter.encounter_is_active)
    if request.method == "GET":
        eoc_form = Episode_Of_Care_Call_Centre_Form()
        if request.method == "GET":
            if encounter.encounter_patient == None:
                filters = {
                    "name" :request.GET.get('name'),
                    "surname": request.GET.get('surname'),
                    "BSN" : request.GET.get('BSN'),
                    "ZipCode" : request.GET.get('Zip Code')
                }

                patients_filtered = Patient_Profile.objects.filter(patient_name=request.GET.get('name'))
                pf_json = serializers.serialize('json', patients_filtered)

                return render(request, 'main/Hospital_Team/emergency_session.html', {"patient" : False,
                "filters" : filters,
                "patients" : json.loads(pf_json),
                "encounter": encounter.pk })
            
            else:
                encounter = Encounter.objects.get(pk=id)
                teams_involved = Episode_Of_Care.objects.filter(Q(episode_of_care_encounter=encounter), ~Q(episode_of_care_tag = "Hospital Team"))
                patient_profile = encounter.encounter_patient
                episode_of_care = Episode_Of_Care.objects.get(episode_of_care_encounter=encounter, episode_of_care_tag="Hospital Team")
                ep = get_object_or_404(Episode_Of_Care, pk=episode_of_care.pk)
                episode_of_care_form = Episode_Of_Care_Hospital_Form(instance=ep)
                #available_teams = Care_Team.objects.filter(care_team_is_active=True)

                
                pat_fmh, pat_ai, pat_med, pat_enc = patient_history_resumed(patient_profile.user)

                print(teams_involved)

                return render(request, 'main/Hospital_Team/emergency_session.html', {"patient" : True,
                "encounter" : encounter,
                "patient_profile" : patient_profile,
                "episode_of_care" : episode_of_care,
                "episode_of_care_form" : episode_of_care_form,
                "teams_involved" : teams_involved,
                "medications" : pat_med,
                "family_member_history" : pat_fmh,
                "allergy_intolerance" : pat_ai,
                "encounters": pat_enc})
    
    if request.method == "POST":
        episode_of_care = Episode_Of_Care.objects.get(episode_of_care_encounter=encounter, episode_of_care_tag="Hospital Team")
        episode_of_care.neurological_evaluation = request.POST["neurological_evaluation"]
        episode_of_care.radiological_imaging = request.POST["radiological_imaging"]
        episode_of_care.image_thrombus_location = request.POST["image_thrombus_location"]
        episode_of_care.image_aspects = request.POST["image_aspects"]
        episode_of_care.image_collateral_score = request.POST["image_collateral_score"]
        episode_of_care.infart_core_size = request.POST["infart_core_size"]
        episode_of_care.penumbra_size = request.POST["penumbra_size"]
        episode_of_care.medication = request.POST["medication"]
        episode_of_care.procedure = request.POST["procedure"]
        episode_of_care.save()

        return redirect("/main/Hospital-Team/Emergency-Session/{}".format(encounter.pk))




###############################################################################################
#########################  Encounter  ###################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def hosp_create_encounter(request):
    if request.method == "POST":
        starter = request.user
        tag = "Emergency Session"
        is_active = True
        start_time = datetime.now(pytz.timezone(local_tz))
        new_encounter = Encounter.objects.create(encounter_starter=starter,
        encounter_tag=tag,
        encounter_is_active=is_active,
        encounter_start_datetime=start_time)
        data = {"success" : "200",
        "encounter_pk" : new_encounter.pk}
        return JsonResponse(data)

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def end_emergency(request, id=None, pk=None):
    if request.method == "POST":
        encounter = Encounter.objects.get(pk=pk)
        encounter.encounter_is_active = False
        encounter.encounter_end_datetime = datetime.now(pytz.timezone(local_tz))
        encounter.save()
        data = {"success" : "200"}
        return JsonResponse(data)

######################################################################
############## Choose Patient and Start Episode of Care ##############
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def hosp_attr_patient(request, id=None, pk=None):

    patient_prof = Patient_Profile.objects.get(pk=pk)

    encounter = get_object_or_404(Encounter, pk=id)
    encounter.encounter_patient=patient_prof
    encounter.save()

    care_team = Care_Team.objects.get(care_team_identifier=8097022)

    episode_of_care = Episode_Of_Care.objects.create(
    episode_of_care_team=care_team,
    episode_of_care_encounter=encounter,
    episode_of_care_tag="Hospital Team",
    episode_of_care_start_datetime=datetime.now(pytz.timezone(local_tz)),
    episode_of_care_accepted=True)

    data = {"success" : "200"}
    return JsonResponse(data)

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def checkin_patient(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)

    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

    # End call centre or hospital session
    eps = Episode_Of_Care.objects.filter(encounter=encounter)
    for i in range(len(eps)-1):
        if eps[i].end_datetime is None:
            eps[i].end_datetime = datetime.now(pytz.timezone(local_tz))
            eps[i].save()

            if eps[i].tag != "Call Centre":
                team_activities = Team_Activities.objects.create(
                    care_team=eps[i].care_team,
                    action_type=5,
                    title="Access Revoked",
                    message="Emergency ID {}".format(eps[i].encounter.pk),
                    encounter=eps[i].encounter,
                    activity_date=datetime.now(pytz.timezone(local_tz))
                )

    # This Episode of Care
    ep = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
    if len(ep) > 1:
        ep[1].patient_accepted = True
        ep[1].accepted_date = datetime.now(pytz.timezone(local_tz))
        ep[1].save()
    else:
        ep[0].patient_accepted = True 
        ep[0].accepted_date = datetime.now(pytz.timezone(local_tz))
        ep[0].save()
    return JsonResponse({})

##############################################################################################
#########################  Request Service  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def get_eps(request, id=None):
    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=request.POST["id"])
        eps = Episode_Of_Care.objects.filter(episode_of_care_encounter=encounter)
        serdata = serializers.serialize("json", eps)
        #print(serdata)
        return JsonResponse(json.loads(serdata), safe=False)