from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from login.models import *
from login.forms import *
from django.http import JsonResponse
from django.http import HttpResponse
from django.core import serializers
import json
from django.contrib.auth import logout
from datetime import datetime

def generate_json(dict_param):
    resp_json = json.dumps(dict_param)
    return resp_json