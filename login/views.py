from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .models import *
from .forms import *
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION, ContentType
import sys
from .help_json import *
from django.contrib.auth.decorators import login_required
from keycloak import KeycloakOpenID

@login_required
def index_login(request):    
    print("login page")
    print(request.user.user_type)
    if request.user.is_authenticated:        
        if request.user.user_type == 1:
            return redirect("/patient/home")
        if request.user.user_type == 2:
            return redirect("/ambulance/home")
        if request.user.user_type == 3:
            return redirect("/call_centre/home")
        if request.user.user_type == 4:
            return redirect("/hospital/home")
        if request.user.user_type == 5:
            return redirect("/org/home")
        if request.user.user_type == 6 or request.user.user_type == 7:
            return redirect("/researcher/home")        


    if request.method == "POST":
        print("Post method detected")
        username = request.POST.get("username")
        password = request.POST.get("password")
        print(username, password)

        return render(request, 'login/login.html')
        #user = authenticate(username=username, password=password)
        #print(user)        

        """
        if user is not None:
            login(request, user)
            LogEntry.objects.log_action(
                        user_id=user.pk,
                        content_type_id=ContentType.objects.get_for_model(User).pk,
                        object_id=user.pk,
                        object_repr="Account Login",
                        action_flag=ADDITION,
            )
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect("/patient/home")
        else:
            return render(request, 'login/login.html')
        """

    else:
        return render(request, 'login/login.html')

def index_signup(request):
    if request.method == "POST":
        standard_form = Standard_Form(request.POST)
        if(standard_form.is_valid()):
            username = request.POST["username"]
            password1 = request.POST["password"]
            password2 = request.POST["password2"]
            email = request.POST["email"]

            tmp_user = User.objects.create_user(username, email, password1)
            tmp_user.first_name = standard_form.cleaned_data['name']
            tmp_user.last_name = standard_form.cleaned_data['surname']
            tmp_user.save()
            print("User Created!")

            ########################## TESTING SAVE ################
            resp_dict = {}
            resp_dict['patient_id'] = tmp_user.pk
            resp_dict['name'] = standard_form.cleaned_data['name']
            resp_dict['surname'] = standard_form.cleaned_data['surname']
            resp_dict['gender'] = standard_form.cleaned_data['gender']
            resp_dict['telecom'] = standard_form.cleaned_data['telecom']
            resp_dict['bsn'] = standard_form.cleaned_data['bsn']
            #resp_dict['birthDate'] = standard_form.cleaned_data['birthDate']
            resp_dict['address'] = standard_form.cleaned_data['address']
            resp_dict['postalCode'] = standard_form.cleaned_data['postalCode']
            resp_dict['country'] = standard_form.cleaned_data['country']
            new_patient_json = generate_json(resp_dict)
            ########################################################

            ## delete this after
            first_name = request.POST["name"]
            last_name = request.POST["surname"]
            gender = request.POST["gender"]
            phone = request.POST["telecom"]
            bsn = request.POST["bsn"]
            birthdate = request.POST["birthDate"]
            address = request.POST["address"]
            postal_code = request.POST["postalCode"]
            country = request.POST["country"]
            tmp_patient = Patient_Profile.objects.create(user=tmp_user)
            tmp_patient.patient_name = first_name
            tmp_patient.patient_surname = last_name
            #tmp_patient.patient_telecom = phone
            tmp_patient.patient_BSN = bsn
            tmp_patient.patient_birthDate = birthdate
            tmp_patient.patient_gender = gender
            tmp_patient.patient_address = address
            tmp_patient.patient_zipcode = postal_code
            tmp_patient.save()
            print("Patient Created!")
            ##

            user = authenticate(username=username, password=password1)
            if user is not None:
                login(request, user)
                next = request.GET.get('next')
                LogEntry.objects.log_action(
                            user_id=user.pk,
                            content_type_id=ContentType.objects.get_for_model(User).pk,
                            object_id=user.pk,
                            object_repr="Account Created!",
                            action_flag=ADDITION,
                )

                if next:
                    return redirect(next)
                return redirect("/patient/home")
            else:   
                return render(request, 'login/signup.html', {'standard_form': standard_form})
        else:
                return render(request, 'login/signup.html', {'standard_form': standard_form})

    else:
        standard_form = Standard_Form()
        return render(request, 'login/signup.html', {'standard_form': standard_form})