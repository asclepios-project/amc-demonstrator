from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField
import uuid
from django.contrib.auth.models import AbstractUser
from .codes import ALLERG_INT_CODES, CONDITION_CODE, CONDITION_BODY_SITE
from researcher.models import Approved_Proposal


class User(AbstractUser):
    USER_TYPE_CHOICES = (
      (1, 'patient'),
      (2, 'ambulance'),
      (3, 'call_centre'),
      (4, 'hospital'),
      (5, 'org_admin'),
      (6, 'researcher'),
      (7, 'principal_investigator'),
    )

    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, null=True, default=USER_TYPE_CHOICES[0][0])

##############################################################################
############# Patient Profile following FHIR
class Patient_Profile(models.Model):
    PAT_GENDER = (
        ('unknown', 'Unknown'),
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other'),
    )

    PAT_MARITAL = (
        ('UNK', 'Unknown'),
        ('M', 'Married'),
        ('S', 'Never Married'),
        ('D', 'Divorced'),
        ('A', 'Annulled'),
        ('W', 'Widowed'),
        ('U', 'Unmarried'),
        ('T', 'Domestic partner'),
        ('P', 'Polygamous'),
        ('I', 'Interlocutory'),
        ('L', 'Legally Separated')
    )

    BLOOD_TYPE = (
        ('Unknown', 'Unknown'),
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('O+', 'O+'),
        ('0-', '0-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),    
    )

    PAT_RELATIONSHIP = (
        ('U', 'Unknown'),
        ('C', 'Emergency Contact'),
        ('E', 'Employer'),
        ('F', 'Federal Agency'),
        ('I', 'Insurance Company'),
        ('N', 'Next-of-Kin'),
        ('S', 'State Agency'),
    )

    PAT_LANGUAGE = (
        ('en', 'English'),
        ('ar', 'Arabic'),
        ('bn', 'Bengali'),
        ('cs', 'Czech'),
        ('da', 'Danish'),
        ('de','German'),
        ('de-AT', 'German (Austria)'),
        ('de-CH','German (Switzerland'),
        ('de-DE','German (Germany'),
        ('el', 'Greek'),
        ('en-AU', 'English (Australia)'),
        ('en-CA', 'English (Canada)'),
        ('en-GB', 'English (Great Britain)'),	
        ('en-IN', 'English (India)'),
        ('en-NZ', 'English (New Zeland)'),
        ('en-SG', 'English (Singapore)'),
        ('en-US', 'English (United States)'),
        ('es', 'Spanish'),
        ('es-AR', 'Spanish (Argentina)'),
        ('es-ES', 'Spanish (Spain)'),
        ('es-UY', 'Spanish (Uruguay)'),
        ('fi', 'Finnish'),
        ('fr', 'French'),
        ('fr-BE', 'French (Belgium)'),
        ('fr-CH', 'French (Switzerland)'),
        ('fr-FR', 'French (France)'),
        ('fy', 'Frysian'),
        ('fy-NL', 'Frysian (Netherlands)'),
        ('hi', 'Hindi'),
        ('hr', 'Croatian'),	
        ('it', 'Italian'),
        ('it-CH', 'Italian (Switzerland)'),
        ('it-IT', 'Italian (Italy)'),
        ('ja', 'Japanese'),
        ('ko', 'Korean'),
        ('nl', 'Dutch'),
        ('nl-BE', 'Dutch (Belgium)'),
        ('nl-NL', 'Dutch (Netherlands)'),
        ('no', 'Norwegian'),
        ('no-NO', 'Norwegian (Norway)'),
        ('pa', 'Punjabi'),
        ('pl', 'Polish'),
        ('pt', 'Portuguese'),
        ('pt-BR', 'Portuguese (Brazil)'),
        ('ru', 'Russian'),
        ('ru-RU', 'Russian (Russia)'),
        ('sr', 'Serbian'),
        ('sr-RS', 'Serbian (Serbia)'),
        ('sv', 'Swedish'),
        ('sv-SE', 'Swedish (Sweden)'),
        ('te', 'Telegu'),
        ('zh', 'Chinese'),
        ('zh-CN', 'Chinese (China)'),
        ('zh-HK', 'Chinese (Hong Kong)'),
        ('zh-SG', 'Chinese (Singapore)'),
        ('zh-TW', 'Chinese (Taiwan)'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    identifier = models.IntegerField(null=True, unique=True)
    active = models.BooleanField("Active", null=True, default=False)
    name = models.CharField("Name", null=True, max_length=200, blank=True)
    surname = models.CharField("Surname", null=True, max_length=200, blank=True)
    BSN = models.IntegerField("BSN", null=True, blank=True)
    telecom =models.CharField("Phone Number", null=True, max_length=200, blank=True)
    gender = models.CharField("Gender", max_length=20, choices=PAT_GENDER, default=PAT_GENDER[0][0], null=True, blank=True)
    birthDate = models.DateField("Birth Date", null=True, blank=True)
    address = models.CharField("Address", null=True, max_length=100, blank=True)
    zipcode = models.CharField("Zip Code", null=True, max_length=50, blank=True)
    maritalStatus = models.CharField("Marital Status", max_length=10, choices=PAT_MARITAL, default=PAT_MARITAL[0][0], blank=True)
    photo = models.ImageField("Profile Picture", upload_to='patient_pic', blank=True, null=True)
    communication_language = models.CharField("Communication Language", max_length=10, choices=PAT_LANGUAGE, default=PAT_LANGUAGE[0][0], null=True, blank=True)
    blood_type = models.CharField("Blood Type", max_length=10, choices=BLOOD_TYPE, default=BLOOD_TYPE[0][0], null=True, blank=True)

    # Managing Organization
    managing_organization = models.CharField("Managing Organization", max_length=10)

    # Contact Informations
    contact_name = models.CharField("Name", null=True, max_length=200)
    contact_relationship = models.CharField("Relationship",max_length=20, choices=PAT_RELATIONSHIP, default=PAT_RELATIONSHIP[0][0], null=True)
    contact_telecom = models.CharField("Phone Number", null=True, max_length=200)
    contact_zipcode = models.CharField("Zip Code", null=True, max_length=50, blank=True)
    contact_address = models.CharField("Address", null=True, max_length=200)
    contact_gender = models.CharField("Gender", max_length=20, choices=PAT_GENDER, default=PAT_GENDER[0][0], null=True)

    last_time_updated = models.DateTimeField("Last Time Updated", null=True)

    key_id = models.CharField("KeyID", null=True, max_length=512, blank=True)

#################################################################
############# Allergies Following FHIR
class Allergy_Intolerance(models.Model):
    ALLERG_TYPE = (
        (None, 'Select an option'),
        ('allergy', 'Allergy'),
        ('intolerance', 'Intolerance'),
    )

    ALLERG_CLINICAL_STATUS = (
        (None, 'Select an option'),
        ('active', 'Active'),
        ('inactive', 'Inactive'),
        ('resolved', 'Resolved'),
    )

    ALLERG_VERIFICATION_STATUS = (
        (None, 'Select an option'),
        ('unconfirmed', 'Unconfirmed'),
        ('confirmed', 'Confirmed'),
        ('refuted', 'Refuted'),
        ('entered-in-error', 'Entered in Error')
    )

    ALLERG_CATEGORY = (
        (None, 'Select an option'),
        ('food', 'Food'),
        ('medication', 'Medication'),
        ('environment', 'Environment'),
        ('biologic', 'Biologic'),
    )

    ALLERG_CRITICALITY = (
        (None, 'Select an option'),
        ('low', 'Low Risk'),
        ('high', 'High'),
        ('unable-to-assess', 'Unable to Assess Risk')
    )
    
    identifier =  models.IntegerField(null=True, unique=True)
    subject = models.ForeignKey(Patient_Profile, on_delete=models.CASCADE, null=True)
    clinicalStatus = models.CharField("Clinical Status", max_length=20, null=True, choices=ALLERG_CLINICAL_STATUS, default=ALLERG_CLINICAL_STATUS[0][0])
    verificationStatus = models.CharField("Verification Status", max_length=20, null=True, choices=ALLERG_VERIFICATION_STATUS, default=ALLERG_VERIFICATION_STATUS[0][0])
    type = models.CharField("Type", null=True, max_length=20, choices=ALLERG_TYPE, default=ALLERG_TYPE[0][0])
    category = models.CharField("Category", null=True, max_length=20, choices=ALLERG_CATEGORY, default=ALLERG_CATEGORY[0][0])
    criticality = models.CharField("Criticality", null=True,  max_length=20, choices=ALLERG_CRITICALITY, default=ALLERG_CRITICALITY[0][0])
    note = models.CharField("Note", null=True, max_length=200, blank=True)
    recordedDate = models.DateField(null=True)

    # Need to finish
    code = models.IntegerField("Name", null=True, choices=ALLERG_INT_CODES, default=ALLERG_INT_CODES[0][0], blank=True)

class Condition(models.Model):
    COND_CLINICAL_STATUS = (
        (None, 'Select an option'),
        ('active', 'Active'),
        ('recurrence', 'Recurrence'),
        ('relapse', 'Relapse'),
        ('inactive', 'Inactive'),
        ('remission', 'Remission'),
        ('resolved', 'Resolved'),
    )

    COND_VERIFICATION_STATUS = (
        (None, 'Select an option'),
        ('unconfirmed', 'Unconfirmed'),
        ('provisional', 'Provisional'),
        ('differential', 'Differential'),
        ('confirmed', 'Confirmed'),
        ('refuted', 'Refuted'),
        ('entered-in-error', 'Entered in Error')
    )

    COND_CATEGORY = (
        (None, 'Select an option'),
        ('problem-list-item', 'Problem List Item'),
        ('encounter-diagnosis', 'Encounter Diagnosis')
    )

    COND_SEVERITY = (
        (None, 'Select an option'),
        (24484000, 'Severe'),
        (6736007, 'Moderate'),
        (255604002, 'Mild')
    )

    subject = models.ForeignKey(Patient_Profile, on_delete=models.CASCADE, null=True)
    identifier = models.IntegerField(null=True, unique=True)
    clinicalStatus = models.CharField("Clinical Status",max_length=20, null=True, choices=COND_CLINICAL_STATUS, default=COND_CLINICAL_STATUS[0][0])
    verificationStatus = models.CharField("Verification Status",max_length=20, null=True, choices=COND_VERIFICATION_STATUS, default=COND_VERIFICATION_STATUS[0][0])
    category = models.CharField("Category",max_length=20, null=True, choices=COND_CATEGORY, default=COND_CATEGORY[0][0])
    severity = models.IntegerField("Severity", null=True, choices=COND_SEVERITY, default=COND_SEVERITY[0][0])
    note  = models.CharField("Note", null=True, max_length=200, blank=True)
    recordedDate = models.DateField("Recorded Date", null=True)

    # Need to finish
    code = models.IntegerField("Codition", null=True, choices=CONDITION_CODE, default=CONDITION_CODE[0][0])
    bodySite = models.IntegerField("Body Site", null=True, choices=CONDITION_BODY_SITE, default=CONDITION_BODY_SITE[0][0])
    

class Family_Member_History(models.Model):
    FAM_GENDER = (
        (None, 'Select an option'),
        ('unknown', 'Unknown'),
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other'),
    )

    FAM_RELATIONSHIP = (
        (None, 'Select an option'),
        ('CHILD', 'Child'),
        ('AUNT', 'Aunt'),
        ('COUSIN', 'Cousin'),
        ('GRNDCHILD', 'Grandchild'),
        ('FTH', 'Father'),
        ('MTH', 'Mother'),
        ('BRO', 'Brother'),
        ('HUSB', 'Husband'),
        ('Wife', 'Wife')
    )

    subject = models.ForeignKey(Patient_Profile, on_delete=models.CASCADE, null=True)
    name = models.CharField("Name", null=True, max_length=200)
    relationship = models.CharField("Relationship", max_length=20, choices=FAM_RELATIONSHIP, default=FAM_RELATIONSHIP[0][0], null=True)
    sex = models.CharField("Sex", max_length=20, choices=FAM_GENDER, default=FAM_GENDER[0][0], null=True)
    age = models.IntegerField("Age", null=True, blank=True)
    estimatedAge = models.BooleanField("Estimated Age", null=True, default=False)
    deceased = models.BooleanField("Deceased", null=True, max_length=200)
    condition_code = models.OneToOneField(Condition, on_delete=models.CASCADE, null=True)
    note = models.CharField("Note", null=True, max_length=200, blank=True)
    recordedDate = models.DateField("Updated or Recorded Date", null=True)
    condition = models.IntegerField("Codition", null=True, choices=CONDITION_CODE, default=CONDITION_CODE[0][0])


########################################################################################################
########################################################################################################
class Organization(models.Model):
    ORG_TYPE_CHOICES = (
        (1, 'Unspecified'),
        (2, 'Ambulance Service'),
        (3, 'Call Centre Service'),
        (4, 'Hospital Service'),
    )
    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    identifier = models.IntegerField(null=True, unique=True)
    active = models.BooleanField(null=True)
    type = models.PositiveSmallIntegerField(choices=ORG_TYPE_CHOICES, null=True, default=ORG_TYPE_CHOICES[0][0])
    name = models.CharField(null=True, max_length=100)
    alias = models.CharField(null=True, max_length=100)
    telecom = models.CharField(null=True, max_length=100)
    zipcode = models.CharField(null=True, max_length=100)
    address = models.CharField(null=True, max_length=100)
    country = models.CharField(null=True, max_length=100)
    city = models.CharField(null=True, max_length=100)
    


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    identifier = models.IntegerField(unique=True, null=True)
    org = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True)
    type = models.PositiveSmallIntegerField(null=True)
    name = models.CharField(null=True, max_length=100)
    surname = models.CharField(null=True, max_length=100)
    BSN = models.CharField(null=True, max_length=100)
    email = models.EmailField(null=True, max_length=100)
    role = models.CharField(null=True, max_length=100)
    department = models.CharField(null=True, max_length=100)
    


class Care_Team(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True)
    identifier = models.IntegerField(null=True, unique=True)
    name = models.CharField(null=True, max_length=100)
    tag = models.CharField(null=True, max_length=100, default="")
    is_active = models.BooleanField(null=True)
    category = models.CharField(null=True, max_length=100)
    start_datetime = models.DateTimeField(null=True)
    end_datetime = models.DateTimeField(null=True)
    reason_code = models.IntegerField(null=True)
    telecom = models.IntegerField(null=True)
    note = models.CharField(null=True, max_length=200)
    current_location = models.CharField(null=True, max_length=200)
    time_to_revoke = models.IntegerField(null=True, default=30)


class Encounter(models.Model):
    ENC_STATUS = (
        ('planned', 'Planned'),
        ('arrived', 'Arrived'),
        ('triaged', 'Triaged'),
        ('in-progress', 'In Progress'),
        ('onleave', 'On Leave'),
        ('finish', 'Finish'),
        ('cancelled', 'Cancelled'),
        ('entered-in-error', 'Entered in Error'),
        ('unknown', 'Unknown')
    )

    ENC_CLASS = (
        ('AMB', 'Ambulatory'),
        ('EMER', 'Emergency'),
        ('FLD', 'Field'),
        ('HH', 'Home health'),
        ('IMP', 'Impatient encounter'),
        ('ACUTE', 'Impatient acute'),
        ('NONAC', 'Impatient non-acute'),
        ('OBSENC', 'Observation encounter'),
        ('PRENC', 'Pre-admission'),
        ('SS', 'Short stay'),
        ('VR', 'Virtual'),
    )

    identifier = models.IntegerField(null=True, unique=True)
    patient = models.ForeignKey(Patient_Profile, on_delete=models.CASCADE, null=True)
    starter = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='+', null=True)
    org_starter = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='+', null=True)
    org_finisher = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='++', null=True)    
    start_datetime = models.DateTimeField(null=True)
    end_datetime = models.DateTimeField(null=True)

# Episode of care
class Episode_Of_Care(models.Model):
    WHO_CALLED = (
        ('unknown', 'Unknown'),
        ('relative', 'Relative'),
        ('patient', 'Patient'),
        ('other', 'Other'),
    )

    SERVICE = (
      (1, 'call_centre'),
      (2, 'ambulance'),
      (3, 'hospital'),
    )

    ##### Created for better visualization of records
    organisation_name = models.CharField(null=True, max_length=200)
    organisation_address = models.CharField(null=True, max_length=200)
    organisation_phone = models.CharField(null=True, max_length=200)
    #####

    visualized = models.BooleanField(null=True, default=False)
    updated = models.BooleanField(null=True, default=False)
    last_time_updated = models.DateTimeField(null=True)

    encounter = models.ForeignKey(Encounter, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient_Profile, on_delete=models.CASCADE, null=True)
    service = models.PositiveSmallIntegerField(choices=SERVICE, null=True, default=SERVICE[0][0])
    care_team = models.ForeignKey(Care_Team, null=True, on_delete=models.CASCADE)
    identifier = models.IntegerField(null=True, unique=True)
    json_id = models.CharField(null=True, max_length=100)
    tag = models.CharField(null=True, max_length=100)
    type = models.IntegerField(null=True)
    care_manager = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True)
    managing_organization = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True)

    start_datetime = models.DateTimeField(null=True)
    end_datetime = models.DateTimeField(null=True)

    accepted = models.BooleanField(null=True)
    accepted_date = models.DateTimeField(null=True)
    patient_accepted = models.BooleanField(null=True)

    ################################# For Call Centre #################################################
    call_centre_remarks = models.CharField("Remarks", null=True, max_length=200, blank=True)
    last_time_seen_well = models.TimeField("Last time seen well", null=True, blank=True)
    time_of_the_onset = models.TimeField("Time of the onset", null=True, blank=True)
    local_of_the_onset = models.CharField("Local of the onset", null=True, max_length=200, blank=True)
    local_patient = models.CharField("Where patient is now", null=True, max_length=200, blank=True)
    who_called = models.CharField("Who called", max_length=20, choices=WHO_CALLED, default=WHO_CALLED[0][0], null=True)

    #################################  Ambulance ######################################################

    time_of_arrival = models.TimeField(null=True, blank=True)
    heart_rate = models.IntegerField(null=True, blank=True)
    blood_pressure_systolic = models.IntegerField(null=True, blank=True)
    blood_pressure_diastolic = models.IntegerField(null=True, blank=True)
    oxygen_saturation = models.IntegerField(null=True, blank=True)
    breathing_frequency = models.IntegerField(null=True, blank=True)
    glucose = models.IntegerField(null=True, blank=True)
    usage_of_coagulant_medicine = models.BooleanField(null=True, blank=True)
    international_normalized_ratio = models.IntegerField(null=True, blank=True)
    emv_score_eyes = models.IntegerField(null=True, blank=True)
    emv_score_motor = models.IntegerField(null=True, blank=True)
    emv_score_verbal = models.IntegerField(null=True, blank=True)
    ABCDE_stability_airways = models.BooleanField(null=True, blank=True)
    ABCDE_stability_breathing = models.BooleanField(null=True, blank=True)
    ABCDE_stability_circulation = models.BooleanField(null=True, blank=True)
    ABCDE_stability_disability = models.BooleanField(null=True, blank=True)
    ABCDE_stability_exposure = models.BooleanField(null=True, blank=True)
    ambulance_remarks = models.CharField(null=True, max_length=200, blank=True)
    physical_evaluation = models.CharField(null=True, max_length=200, blank=True)
    applied_medication =  models.CharField(null=True, max_length=200, blank=True)

    ################################## Care Center #########################################################

    neurological_evaluation = models.CharField(null=True, max_length=50, blank=True)
    radiological_imaging = models.CharField(null=True, max_length=50, blank=True)
    image_thrombus_location = models.CharField(max_length=20, blank=True, null=True)
    image_aspects = models.IntegerField(null=True, blank=True)
    image_collateral_score = models.IntegerField(null=True, blank=True)
    infart_core_size = models.IntegerField(null=True, blank=True)
    penumbra_size = models.IntegerField(null=True, blank=True)
    medication = models.CharField(null=True, max_length=50, blank=True)
    procedure = models.CharField(null=True, max_length=200, blank=True)

# Relationship who is participating in each care team
class Care_Team_Participants(models.Model):
    identifier = models.ForeignKey(Care_Team, on_delete=models.CASCADE, related_name="care")
    participant_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    participant_role = models.CharField(null=True, max_length=100)
    responsible = models.BooleanField(null=True)
    participating = models.BooleanField(null=True)
    added_date = models.DateTimeField(null=True)

# Requests for service
class Request(models.Model):
    REQUEST_TYPE_CHOICES = (
      (1, 'request_service'),
      (2, 'request_transference'),
      (3, 'request_patient_discharge'),
    )

    encounter = models.ForeignKey(Encounter, on_delete=models.CASCADE, null=True)
    request_type = models.PositiveSmallIntegerField(choices=REQUEST_TYPE_CHOICES, null=True, default=REQUEST_TYPE_CHOICES[0][0])
    team_requesting = models.ForeignKey(Care_Team, on_delete=models.CASCADE, null=True)
    org_requesting = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True)
    org_requested = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True, related_name='+')
    transfer_to = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True, related_name='++')
    answered = models.BooleanField(null=True)
    request_date = models.DateTimeField(null=True)
    request_ref = models.ForeignKey("self", on_delete=models.CASCADE, null=True)
    
class Team_Activities(models.Model):
    ACTION_TYPE_CHOICES = (
      (1, 'created'),
      (2, 'member_added'),
      (3, 'member_removed'),
      (4, 'team_deactivated'),
      (5, 'new_session'),
      (5, 'access_revoked'),
    )

    care_team = models.ForeignKey(Care_Team, on_delete=models.CASCADE, null=True)
    action_type = models.PositiveSmallIntegerField(choices=ACTION_TYPE_CHOICES, null=True, default=ACTION_TYPE_CHOICES[0][0])
    title = models.CharField(max_length=200, null=True)
    message = models.CharField(max_length=200, null=True)
    encounter = models.ForeignKey(Encounter, on_delete=models.CASCADE, null=True)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True)
    activity_date = models.DateTimeField(null=True)
    

class Research_Proposal_Activity(models.Model):
    ACTION_TYPE_CHOICES = (
      (0, 'Unknown'),
      (1, 'created'),
      (2, 'member_added'),
      (3, 'member_removed'),
      (4, 'team_deactivated'),
      (5, 'dataset_download'),
      (5, 'access_revoked'),
    )

    study = models.ForeignKey(Approved_Proposal, on_delete=models.CASCADE, null=True)
    action_type = models.PositiveSmallIntegerField(choices=ACTION_TYPE_CHOICES, null=True, default=ACTION_TYPE_CHOICES[0][0])
    title = models.CharField(max_length=200, null=True)
    message = models.CharField(max_length=200, null=True)    
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, null=True)
    activity_date = models.DateTimeField(null=True)