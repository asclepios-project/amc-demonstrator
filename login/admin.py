from django.contrib import admin
from .models import Organization, Care_Team, Care_Team_Participants, Encounter, Episode_Of_Care, Patient_Profile, Family_Member_History, Condition, Allergy_Intolerance

@admin.register(Patient_Profile)
class Patient_Profile_Admin(admin.ModelAdmin):

    fields = ['user', 'active', 'name', 'surname', 'telecom', 'gender', 'birthDate', 'address', 'maritalStatus',
    'photo', 'contact_name', 'contact_relationship', 'contact_telecom', 'contact_address', 'contact_gender',
    'communication_language', 'key_id']

    ##### list_filter = ('active', 'contact_communication_language')

    ordering = []
    search_fields = ['name', 'surname', 'birthDate', 'key_id']

@admin.register(Family_Member_History)
class Family_Member_History_Admin(admin.ModelAdmin):
    fields = ['name', 'relationship', 'sex', 'age', 'estimatedAge', 'family_mh_deceased', 'code', 'note',
    'recordedDate', 'condition']

@admin.register(Condition)
class Condition_Admin(admin.ModelAdmin):
    fields = ['subject', 'clinicalStatus', 'verificationStatus', 'category', 'severity',
    'note', 'recordedDate', 'code', 'bodySite']

@admin.register(Allergy_Intolerance)
class Allergy_Intolerance_Admin(admin.ModelAdmin):
    fields = ['subject', 'clinicalStatus', 'verificationStatus', 'type', 'category', 'criticality', 'note',
    'recordedDate', 'code']

@admin.register(Organization)
class Organization_Admin(admin.ModelAdmin):
    fields = ['subject', 'active', 'type', 'name', 'alias', 'telecom', 'zipcode', 'address', 'country',
    'city']

@admin.register(Care_Team)
class Care_Team_Admin(admin.ModelAdmin):
    pass

@admin.register(Care_Team_Participants)
class Care_Team_Participants_Admin(admin.ModelAdmin):
    pass

@admin.register(Encounter)
class Encounter_Admin(admin.ModelAdmin):
    pass

@admin.register(Episode_Of_Care)
class Episode_Of_Care_Admin(admin.ModelAdmin):

    fields = ['call_centre_remarks', 'last_time_seen_well', 'time_of_the_onset', 'local_of_the_onset',
    'local_patient', 'who_called', 'time_of_arrival', 'heart_rate', 'blood_pressure_systolic']    