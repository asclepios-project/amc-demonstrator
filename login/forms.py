from django import forms
from django.forms import ChoiceField
from .models import *
from phonenumber_field.formfields import PhoneNumberField
from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput, DateTimePickerInput, MonthPickerInput, YearPickerInput

################### Standard SIGN UP FORM #####################
class Standard_Form(forms.Form):

    GENDER = (
        ('unknown', 'Unknown'),
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other'),
    )

    # Account Setup
    username = forms.CharField(widget=forms.TextInput,max_length=100, label="Username")
    password = forms.CharField(widget=forms.PasswordInput,max_length=100, label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput, max_length=100, label="Retype Password")
    email = forms.EmailField(widget=forms.EmailInput,label="Email", required=False)

    # Overall Profile
    gender = forms.ChoiceField(widget=forms.Select,choices=GENDER, required=False)
    name = forms.CharField(widget=forms.TextInput, label="Name")
    surname = forms.CharField(widget=forms.TextInput, label="Surname")
    birthDate = forms.DateField(widget=DatePickerInput(format='%Y-%m-%d'))
    bsn = forms.CharField(widget=forms.TextInput, label="BSN")

    # Personal Details
    telecom = PhoneNumberField(widget=forms.TextInput, label=("Phone Number"), required=False)
    address = forms.CharField(widget=forms.TextInput, label="Address", required=False)
    postalCode = forms.CharField(widget=forms.TextInput, label="Zip Code")
    country = forms.CharField(widget=forms.TextInput, label="Country", required=False)

##################################################################
################### Roles Profile Forms ##########################
class Patient_Profile_Form(forms.ModelForm):

    class Meta:
        model = Patient_Profile
        fields = ('name',
            'surname',
            'BSN',
            'birthDate', 
            'telecom',
            'gender',
            'zipcode',
            'address',
            'maritalStatus',
            'photo',
            'communication_language',
            'blood_type')
        
        widgets = {
            'birthDate': DatePickerInput(format='%Y-%m-%d'),
            'gender': forms.Select(attrs={'class' :'chosen', 'style' : 'width:200px;'}),
            'blood_type': forms.Select(attrs={'class' :'chosen', 'style' : 'width:200px;'}),
            'communication_language': forms.Select(attrs={'class' :'chosen', 'style' : 'width:200px;'}),
            'maritalStatus': forms.Select(attrs={'class' :'chosen', 'style' : 'width:200px;'}), 
        }

        telecom = PhoneNumberField(widget=forms.TextInput(attrs={'placeholder': ('Phone')}), label=("Phone Number"), required=False)
        contact_telecom = PhoneNumberField(widget=forms.TextInput(attrs={'placeholder': ('Phone')}), label=("Phone Number"), required=False)
    
class Organization_Form(forms.ModelForm):

    class Meta:
        model = Organization
        fields = ('name',
            'alias',
            'telecom',
            'zipcode', 
            'address',
            'country',
            'city',
            )
        
        widgets = {
        }
    
class Emergency_Contact_Form(forms.ModelForm):

    class Meta:
        model = Patient_Profile
        fields = ('contact_name',
        'contact_relationship',
        'contact_telecom',
        'contact_zipcode',
        'contact_address',
        'contact_gender',
        )
        widgets = {
            'contact_gender': forms.Select(attrs={'class' :'chosen', 'style' : 'width:200px;'}),
            'contact_relationship': forms.Select(attrs={'class' :'chosen', 'style' : 'width:200px;'}),
        }
        contact_telecom = PhoneNumberField(widget=forms.TextInput(attrs={'placeholder': ('')}), label=("Phone Number"), required=False)


class Encounter_Form(forms.ModelForm):

    class Meta:
        model = Encounter
        fields = ('patient',
        'starter',
        )

class Episode_Of_Care_Call_Centre_Form(forms.ModelForm):
    class Meta:
        model = Episode_Of_Care
        fields = [
            'call_centre_remarks',
            'last_time_seen_well',
            'time_of_the_onset',
            'local_of_the_onset',
            'local_patient',
            'who_called'
            ]

        widgets = {
                'call_centre_remarks': forms.Textarea(attrs={'class' :'input100', 'placeholder':'Enter remarks...'}),
                'last_time_seen_well': forms.TextInput(attrs={'class' :'input100', 'placeholder':'Time Format "hh:mm"'}),
                'time_of_the_onset': forms.TextInput(attrs={'class' :'input100', 'placeholder':'Time Format "hh:mm"'}),
                'local_of_the_onset': forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter location...'}),
                'local_patient': forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter address, number, zipcode...'}),
            }

class Episode_Of_Care_Ambulance_Form(forms.ModelForm):
    class Meta:
        model = Episode_Of_Care
        fields = [
            'time_of_arrival',
            'heart_rate',
            'blood_pressure_systolic',
            'blood_pressure_diastolic',
            'oxygen_saturation',
            'breathing_frequency',
            'glucose',
            'usage_of_coagulant_medicine',
            'international_normalized_ratio',
            'emv_score_eyes',
            'emv_score_motor',
            'emv_score_verbal',
            'ABCDE_stability_airways',
            'ABCDE_stability_breathing',
            'ABCDE_stability_circulation',
            'ABCDE_stability_disability',
            'ABCDE_stability_exposure',
            'ambulance_remarks',
            'physical_evaluation',
            'applied_medication'
        ]

        widgets = {
                'heart_rate': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'blood_pressure_systolic': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'blood_pressure_diastolic': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'ambulance_remarks': forms.Textarea(attrs={'class' :'input100', 'placeholder':'...'}),
            }

class Episode_Of_Care_Hospital_Form(forms.ModelForm):
    class Meta:
        model = Episode_Of_Care
        fields = [
            'time_of_arrival',
            'heart_rate',
            'blood_pressure_systolic',
            'blood_pressure_diastolic',
            'oxygen_saturation',
            'breathing_frequency',
            'glucose',
            'usage_of_coagulant_medicine',
            'international_normalized_ratio',
            'emv_score_eyes',
            'emv_score_motor',
            'emv_score_verbal',
            'ABCDE_stability_airways',
            'ABCDE_stability_breathing',
            'ABCDE_stability_circulation',
            'ABCDE_stability_disability',
            'ABCDE_stability_exposure',
            'physical_evaluation',
            'neurological_evaluation',
            'radiological_imaging',
            'image_thrombus_location',
            'image_aspects',
            'image_collateral_score',
            'infart_core_size',
            'penumbra_size',
            'medication',
            'procedure'
        ]

        widgets = {
                'heart_rate': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'blood_pressure_systolic': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'blood_pressure_diastolic': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'neurological_evaluation': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'radiological_imaging': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'image_thrombus_location': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'image_aspects': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'image_collateral_score': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'infart_core_size': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'penumbra_size': forms.TextInput(attrs={'class' :'input100', 'placeholder':'...'}),
                'medication': forms.Textarea(attrs={'class' :'input100', 'placeholder':'...'}),
                'procedure': forms.Textarea(attrs={'class' :'input100', 'placeholder':'...'})
            }

###################################################################
###################### Other Forms ################################
class Allergy_Intolerance_Form(forms.ModelForm):
    
    class Meta:
        model = Allergy_Intolerance
        
        fields = ('clinicalStatus',
            'verificationStatus',
            'type',
            'category',            
            'criticality',
            'code',
            'note')
        
        empty_labels = (
            'clinicalStatus'
        )
        
        widgets = {
            'clinicalStatus' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'verificationStatus' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'type' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'category' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'criticality': forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'code': forms.Select(attrs={'class' :'chosen', 'style': 'width:300px;'}),
            'note' : forms.TextInput(attrs={'style': 'width:300px;'})
        }

class Family_Member_History_Form(forms.ModelForm):
    

    class Meta:
        
        model = Family_Member_History

        EXPERTISE_CHOICES = (
            (None, 'Select an option'),
            (True, True),
            (False, False),
        )

        fields = (
            'relationship',
            'sex',
            'estimatedAge',
            'age',
            'deceased',
            'note',
            'condition')

        widgets = {
            'age' : forms.NumberInput(attrs={'style': 'width:150px;'}),
            'relationship' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'sex' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'condition' : forms.Select(attrs={'class' :'chosen', 'style': 'width:300px;'}),
            'deceased' : forms.Select(choices=EXPERTISE_CHOICES, attrs={'class' :'chosen', 'style': 'width:150px;'}),
        }

class Condition_Form(forms.ModelForm):
    class Meta:
        model = Condition
        fields = ('clinicalStatus',
            'verificationStatus',
            'severity',
            'code',
            'bodySite',
            'note',)

        widgets = {
            'clinicalStatus' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'verificationStatus' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'category' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'severity' : forms.Select(attrs={'class' :'chosen', 'style': 'width:150px;'}),
            'code': forms.Select(attrs={'class' :'chosen', 'style': 'width:300px;'}),
            'bodySite': forms.Select(attrs={'class' :'chosen', 'style': 'width: 300px;'}),
            'note' : forms.TextInput(attrs={'style': 'width:300px;'})     
        }

class Care_Team_Form(forms.ModelForm):
    class Meta:
        model = Care_Team
        fields = ('name',
        'tag',
        'note',
        'category',
        'is_active')
    
        widgets = {
                'name': forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter team name...'}),
                'tag': forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter team tag...'}),
                'category': forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter team category...'}),
                'note': forms.Textarea(attrs={'class' :'input100', 'placeholder':'Remarks...'})
            }


###################################################################################################
###################################################################################################
###################################################################################################
###################################################################################################

class Hospital_Employee_Form(forms.Form):
    ROLES = (
        ('neurologist', 'Neurologist'),
        ('cardiologist', 'Cardiologist'),
        ('nurse', 'Nurse'),
        ('therapist', 'Therapist' ),
        ('medical assistant', 'Medical Assistant'),
        ('pharmacist', 'Pharmacist'),
        ('radiologist', 'Radiologist'),
        ('anesthesiogist', 'Anesthesiologist'),
        ('other', 'Other')
    )

    DEPARTMENTS = (
        ('pediatrics', 'Pediatrics' ),
        ('pharmacy', 'Pharmacy' ),
        ('lab', 'Laboratory' ),
        ('radiology', 'Radiology/Imaging' ),
        ('cardiac care', 'Cardiac Care' ),
        ('icu', 'Intense Care Unity' ),
        ('surgey', 'Surgery' ),
        ('therapist', 'Therapist' ),
    )

    name = forms.CharField(label='Name', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter name...'}))
    surname = forms.CharField(label='Surname', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter surname...'}))
    username = forms.CharField(label='Username', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'username'}))
    password = forms.CharField(label='Password', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'password'}))
    email = forms.EmailField(label='Email', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'email@example.com'}))
    role = forms.ChoiceField(label="Role", widget=forms.Select, choices=ROLES, required=True)
    department = forms.ChoiceField(label="Department", widget=forms.Select, choices=DEPARTMENTS, required=True)
    notes = forms.CharField(label='Notes', max_length=100, required=False, widget=forms.Textarea(attrs={'class' :'input100', 'placeholder':'Enter remarks...'}))

class Ambulance_Employee_Form(forms.Form):
    ROLES = (
        ('driver', 'Driver'),
        ('paramedic', 'Paramedic'),
        ('senior paramedic', 'Senior Paramedic'),
        ('emergency care assistant', 'Emergency Care Assistant'),
        ('emergency medical dispatcher', 'Emergency Medical Dispatcher'),
        ('nurse', 'Nurse'),
        ('other', 'Other'),
    )

    DEPARTMENTS = (
        ('emergency', 'Emergency'),
    )

    name = forms.CharField(label='Name', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter name...'}))
    surname = forms.CharField(label='Surname', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter surname...'}))
    username = forms.CharField(label='Username', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'username'}))
    password = forms.CharField(label='Password', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'password'}))
    email = forms.EmailField(label='Email', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'email@example.com'}))
    role = forms.ChoiceField(label="Role", widget=forms.Select,choices=ROLES, required=True)
    department = forms.ChoiceField(label="Department", widget=forms.Select,choices=DEPARTMENTS, required=True)
    notes = forms.CharField(label='Notes', max_length=100, required=False, widget=forms.Textarea(attrs={'class' :'input100', 'placeholder':'Enter remarks...'}))

class CC_Employee_Form(forms.Form):
    ROLES = (
        ('attendant', 'Attendant'),
        ('supervisor', 'Supervisor'),
    )

    DEPARTMENTS = (
        ('emergencies', 'Emergency Center' ),
        ('help', 'Help Center' ),
    )

    name = forms.CharField(label='Name', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter name...'}))
    surname = forms.CharField(label='Surname', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'Enter surname...'}))
    username = forms.CharField(label='Username', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'username'}))
    password = forms.CharField(label='Password', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'password'}))
    email = forms.EmailField(label='Email', max_length=100, widget=forms.TextInput(attrs={'class' :'input100', 'placeholder':'email@example.com'}))
    role = forms.ChoiceField(label="Role", widget=forms.Select,choices=ROLES, required=True)
    department = forms.ChoiceField(label="Department", widget=forms.Select,choices=DEPARTMENTS, required=True)
    notes = forms.CharField(label='Notes', max_length=100, required=False, widget=forms.Textarea(attrs={'class' :'input100', 'placeholder':'Enter remarks...'}))