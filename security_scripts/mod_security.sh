######################################### MOD SECURITY for Apache
sudo apt-get update

sudo apt-get install libapache2-mod-security2

sudo service apache2 restart

# This command has as output -> security2_module (shared)
sudo apachectl -M | grep security

sudo cp /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf

#Change the value of SecRuleEngine from DetectionOnly to on
sudo vim /etc/modsecurity/modsecurity.conf

sudo systemctl restart apache2

sudo mv /usr/share/modsecurity-crs /usr/share/modsecurity-crs.bk

sudo git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git /usr/share/modsecurity-crs

sudo cp /usr/share/modsecurity-crs/crs-setup.conf.example /usr/share/modsecurity-crs/crs-setup.conf

sudo nano /etc/apache2/mods-enabled/security2.conf

######## Add the following lines at the end ########
#### IncludeOptional /usr/share/modsecurity-crs/*.conf
#### IncludeOptional "/usr/share/modsecurity-crs/rules/*.conf

sudo systemctl restart apache2


#################### TEST WITH
# http://127.0.0.1/index.html?exec=/bin/bash