# Install fail2ban
sudo apt install fail2ban

# check content of jail.conf
# sudo cat /etc/fail2ban/jail.conf

# copy the default ,conf file to .local
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

# Install iptables for fail2ban
sudo apt install iptables-persistent

# Apply iptables rules 
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 22 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 80 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -j ACCEPT
sudo iptables -A INPUT -j DROP

# Restart fail2ban
sudo service fail2ban restart

# Config the local file instead of the conf file
sudo vim /etc/fail2ban/jail.local

# Config the file adding the following for apache, apache-fakegooglebot, apache-shellshock, apache-botsearch, apache-overflows, apache-noscript
# [apache]
# enabled  = true
# port     = http,https
# filter   = apache-shellshock
# logpath  = /var/log/apache*/*error.log
# maxretry = 2

# copy the local file tto the conf file again, and reload the fail2ban service
sudo cp /etc/fail2ban/jail.local /etc/fail2ban/jail.conf
sudo service fail2ban reload

# check new rules with
sudo iptables -L

# check fail2ban client status and the jails active
sudo fail2ban-client status

# check status, ip banned etc on apache witth
sudo fail2ban-client status apache