from django.core import serializers
from login import models
import json

with open("synthetic_data.json", "w") as outfile:
	output_json = {}	
	data = serializers.serialize("json", models.Allergy_Intolerance.objects.all())
	output_json["allergy_intolerance"] = data
	outfile.write(data)
	outfile.write("\n")

	data = serializers.serialize("json", models.Condition.objects.all())
	output_json["condition"] = data
	outfile.write(data)
	outfile.write("\n")
	
	data = serializers.serialize("json", models.Family_Member_History.objects.all())
	output_json["family_member_history"] = data
	outfile.write(data)
	outfile.write("\n")

	data = serializers.serialize("json", models.Encounter.objects.all())
	output_json["encounter"] = data
	outfile.write(data)
	outfile.write("\n")

	data = serializers.serialize("json", models.Episode_Of_Care.objects.all())
	output_json["episode_of_care"] = data
	outfile.write(data)
	outfile.write("\n")

with open("teste.json", "w") as outfile2:
	json.dump(output_json, outfile2)