from researcher.models import *
from login.models import User, Employee
from datetime import datetime
from keycloak import KeycloakOpenID
from faker import Faker
from faker.providers import internet
from tqdm import tqdm
fake = Faker('nl_NL')
fake.add_provider(internet)
import requests, string, names, random, time, warnings, secrets

class Populate_Researcher_Use_Case():
	## params
	number_of_pis = 2
	number_of_researchers = 2
	number_of_studies = 2

	studies = ["Acute Stroke Care"]
	researchers = []
	pis = []

	def __init__(self):
		print("Initiating class")
		self.init_and_configure_keycloak()		
		return

	# Generates a new SALT with random and string
	def generate_salt(self, N=6):
		return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))
        

	def init_and_configure_keycloak(self):	
		# Configure client
		keycloak_openid = KeycloakOpenID(server_url="http://192.168.0.104:8181/auth/",
		    client_id="amc-client",
		    realm_name="master")

		# Get Token
		token = keycloak_openid.token("amcadmin", "amcadmin")
		userinfo = keycloak_openid.userinfo(token['access_token'])

		# defining the api-endpoint 
		self.registration_authority_endpoint = "http://192.168.0.104:8083/api/v1/auth/"
		self.headers = {
		    'content-type': 'application/json',
		    'Authorization' : 'Bearer {}'.format(token["access_token"])
		}
		return

	def create_principal_investigator(self):		
		for i in tqdm(range(self.number_of_pis)):
			first_name = names.get_first_name()
			last_name = names.get_last_name()
			salt = self.generate_salt()
			username = first_name+last_name+"-"+salt
			email = first_name+last_name+"@researcher-pi.com"
			# data example.
			data = {
				"firstName":first_name,
				"lastName":last_name,
				"email":email,
				"username":username,		
				"enabled":"true",
				"keys":["role"],
				"values":["researcher-pi"],
				"realmRoles":[],
				#"credentials":[{"type":"password","value":"pwd123","temporary":"false"}]
			}

			self.create_user_using_registration_authority(self.headers, data)
			self.create_user_django(first_name, last_name, username, email, pi=True)
			self.pis.append(username)
		return

	def create_researchers(self):
		for i in tqdm(range(self.number_of_pis)):
			first_name = names.get_first_name()
			last_name = names.get_last_name()
			salt = self.generate_salt()
			username = first_name+last_name+"-"+salt
			email = first_name+last_name+"@researcher.com"
			# data example.
			data = {
				"firstName":first_name,
				"lastName":last_name,
				"email":email,
				"username":username,		
				"enabled":"true",
				"keys":["role"],
				"values":["researcher"],
				"realmRoles":[],
				#"credentials":[{"type":"password","value":"pwd123","temporary":"false"}]
			}

			self.create_user_using_registration_authority(self.headers, data)
			self.create_user_django(first_name, last_name, username, email, pi=False)
			self.researchers.append(username)
		return	

	def create_study(self):
		for i in tqdm(range(len(self.studies))):
			user = User.objects.get(username=random.choice(self.pis))
			study = Approved_Proposal.objects.create(
				name=self.studies[i],			
				principal_investigator=user,
				start_datetime=datetime.now(),
				end_datetime=datetime.now(),
				status="Active",
			)
		return

	def create_user_using_registration_authority(self, headers, data):
		r = requests.post(url=(self.registration_authority_endpoint+"add-user"), headers=headers, json=data)
		#print(headers, data)
		return

	def create_user_django(self, first_name, last_name, username, email, pi):
		user = User.objects.create_user(username=username,
			email=email,
			password="!asclepios!"
		)
		if pi:
			user.user_type=7
			self.create_employee_profile(user, first_name, last_name, email, "Principal Investigator")
		else:
			user.user_type=6
			self.create_employee_profile(user, first_name, last_name, email, "Researcher")
		user.save()
		return

	def create_employee_profile(self, user, first_name, last_name, email, role):
		employee_profile = Employee.objects.create(
			user=user,			
			employee_name=first_name,
			employee_surname=last_name,
			employee_email=email,
			employee_role=role,
			employee_department="Research"
		)
		return

def populate():
	Approved_Proposal.objects.all().delete()
	Has_Researcher.objects.all().delete()
	Feature.objects.all().delete()
	Complete_Dataset.objects.all().delete()
	User.objects.all().delete()

	populate_research = Populate_Researcher_Use_Case()

	print("\nCreating principal investigators")
	populate_research.create_principal_investigator()

	print("\nCreating researchers")
	populate_research.create_researchers()

	print("\nCreating study")
	populate_research.create_study()

	print()

	return
if __name__ == "__main__":
	populate()


"""
print("\nCreating features and adding them to a trial...\n")
####### Create features
features = []
feature_age = Feature.objects.create(
	name="age",
	key_ID="2c699c9c-2e67-4db4-a69c-a14800fd9783"
)

feature_gender = Feature.objects.create(
	name="gender",
	key_ID="2c699c9c-2e67-4db4-a69c-a14800fd9783"
)

feature_cc = Feature.objects.create(
	name="condition_code",
	key_ID="2c699c9c-2e67-4db4-a69c-a14800fd9783"
)
features=[feature_age, feature_gender, feature_cc]


### Create Trial and add all features
trial = Complete_Dataset.objects.create(
	identifier=1,
	trial_ID=1,
	principal_investigator=user,
	start_datetime=datetime.now(),
	end_datetime=datetime.now(),
)


###### Create study, add reseachers and features
print("Creating study, adding researcher and approved features...\n")
study = Approved_Proposal.objects.create(
	name="Acute Stroke Care",
	status="Active"

)
Has_Researcher.objects.create(
	researcher=user,
	study=study
)

for feature in features:
	Has_Approved_Feature.objects.create(
		approved_proposal=study,
		feature=feature,
	)
"""