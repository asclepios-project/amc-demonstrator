from login.models import *
from datetime import datetime
from keycloak import KeycloakOpenID
from faker import Faker
from faker.providers import internet
from tqdm import tqdm
fake = Faker('nl_NL')
fake.add_provider(internet)
import requests, string, names, random, time, warnings, secrets, json
import sys

### imports for dump
from django.core import serializers
from login import models
import json

class Populate_Acute_Stroke():
	##### Patients that has an account inside our application without any record
	N_PATIENTS_WITHOUT_RECORDS = 5 #80000
	N_PATIENTS_WITH_RECORDS = 5 #20000
	# Percent of the patients that has records had an stroke
	PERCENT_PATIENTS_HAD_STROKE = 80
	# Percent of the patients that had stroke needed a transferred between hospitals
	PERCENT_PATIENTS_NEEDED_TRANSFER = 60

	##### Organizations, call centre, ambulance & hospitals
	# Number of each organisation, if you want to change this please change the list initialized in the init function
	N_CALL_CENTRE_ORG = 1 
	N_AMBULANCE_ORG = 2
	N_HOSPITAL_ORG = 2 #7

	# Number of employees and teams inside each organisation
	N_ORGANIZATION_TEAMS = 2 #10
	N_EMPLOYEES_PER_ORG = 2 #100

	hospital_orgs = None
	ambulance_orgs = None
	call_centre_orgs = None

	org_starter = None
	org_finisher = None
	
	###### Episode of care variables
	#
	heart_rate = [0,150]
	blood_pressure_systolic = [40, 300]
	blood_pressure_diastolic = [10, 150]
	oxygen_saturation = [40, 100]
	breathing_frequency = [0, 100]
	glucose = [1, 60]
	international_normalized_ratio = [1, 10]
	emv_score_eyes = [0, 4]
	emv_score_motor = [0, 4]
	emv_score_verbal = [0, 4]

	def __init__(self):
		self.gender = ['male', 'female']
		self.hospitals = [('VU medisch centrum','VUMC'), ('Academisch Medisch Centrum', 'AMC'), ('Universitair Medisch Centrum Groningen', 'UMCG'), ('Universitair Medisch Centrum Utrecht', 'UMCU'), ('Leids Universitair Medisch Centrum', 'LUMC'),('Maastricht Universitair Medisch Centrum', 'MUMC+'), ('Erasmus MC', 'EMC'), ('Sint Lucas Andreas Ziekenhuis', '')]
		self.ambulances = [('Ambulancezorg Nederland', 'AN'), ('UMCG Ambulancezorg', 'UMCGA'),]
		self.call_centre = [('Emergency Call Centre', 'ECC')]

		# Configure client
		keycloak_openid = KeycloakOpenID(server_url="http://192.168.0.104:8181/auth/",
			client_id="amc-client",
			realm_name="master")

		# Get Token
		token = keycloak_openid.token("amcadmin", "amcadmin")
		userinfo = keycloak_openid.userinfo(token['access_token'])	

		# defining the api-endpoint 
		self.registration_authority_endpoint = "http://192.168.0.104:8083/api/v1/auth/"
		self.cpabe_endpoint = "http://192.168.0.104:8084/api/v1/put"
		self.cpabe_policy_endpoint = "http://192.168.0.104:8084/api/v1/setpolicy"
		self.headers = {
			'content-type': 'application/json',
			'Authorization' : 'Bearer {}'.format(token["access_token"])
		}

		self.keys = {}
		return

	# Functions

	# Generates a new SALT with random and string
	def generate_salt(self, N=6):
		return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))

	# Generates a new BSN with randint
	def generate_bsn(self):
		import random
		return random.randint(100000000,999999999)

	# Generates a new Age following the age parameters
	def generate_age(self, age=0):
		import random
		year = 2020 - age
		month = str(random.randint(1,12))
		day = str(random.randint(1,28))
		if len(month) == 1:
			month = "0{}".format(month)
		if len(day) == 1:
			day = "0{}".format(day)

		response = "{}-{}-{}".format(year, month, day)
		return response

	##### Generate encounter and episodes of care for the acute stroke care
	# Generate the encounter
	def generate_encounter(self, patient):
		self.org_starter = random.choice(self.call_centre_orgs)
		self.org_finisher = random.choice(self.hospital_orgs)
		employee_starter = random.choice(Employee.objects.all().filter(org=self.org_starter))

		encounter = Encounter.objects.create(
			patient=patient,
			starter=employee_starter,
			org_starter=self.org_starter,
			org_finisher=self.org_finisher,
			start_datetime=datetime.now(),
			end_datetime=datetime.now()
		)		
		return encounter
		
	# Generate the episodes of care
	def generate_episode_of_care(self, encounter=None, type_of_org=None, starter=False, finisher=False):
		if (starter):
			managing_organization=self.org_starter
		elif (finisher):
			managing_organization=self.org_finisher
		else:
			if type_of_org == "call centre":
				managing_organization=random.choice(self.call_centre_orgs)
			if type_of_org == "ambulance":
				managing_organization=random.choice(self.ambulance_orgs)
			if type_of_org == "hospital":
				managing_organization=random.choice(self.hospital_orgs)
		team=random.choice(Care_Team.objects.all().filter(organization=managing_organization))

		episode_of_care = Episode_Of_Care.objects.create(
			encounter=encounter,
			patient=encounter.patient,
			care_team=team,
			tag=type_of_org,
			#episode_of_care_type=,
			managing_organization=managing_organization,
			start_datetime=datetime.now(),
			end_datetime=datetime.now(),
			accepted=True,
			accepted_date=datetime.now(),
			patient_accepted=True)

		if type_of_org == "call centre":
			episode_of_care.call_centre_remarks=fake.sentence(nb_words=10)
			episode_of_care.last_time_seen_well="00:00h"
			episode_of_care.time_of_the_onset="00:00h"
			episode_of_care.local_of_the_onset=fake.sentence(nb_words=3)
			episode_of_care.local_patient=fake.address().split("\n")[0] + " " + fake.address().split("\n")[1]
			episode_of_care.who_called='relative'

		elif type_of_org == "ambulance":
			episode_of_care.time_of_arrival="00:00h"
			episode_of_care.heart_rate=random.randint(self.heart_rate[0], self.heart_rate[1])
			episode_of_care.blood_pressure_systolic=random.randint(self.blood_pressure_systolic[0], self.blood_pressure_systolic[1])
			episode_of_care.blood_pressure_diastolic=random.randint(self.blood_pressure_diastolic[0], self.blood_pressure_diastolic[1])
			episode_of_care.oxygen_saturation=random.randint(self.oxygen_saturation[0], self.oxygen_saturation[1])
			episode_of_care.breathing_frequency=random.randint(self.breathing_frequency[0], self.breathing_frequency[1])
			episode_of_care.glucose=random.randint(self.glucose[0], self.glucose[1])
			episode_of_care.usage_of_coagulant_medicine=random.choice([True, False])
			episode_of_care.international_normalized_ratio=random.randint(self.international_normalized_ratio[0], self.international_normalized_ratio[1])
			episode_of_care.emv_score_eyes=random.randint(self.emv_score_eyes[0], self.emv_score_eyes[1])
			episode_of_care.emv_score_motor=random.randint(self.emv_score_motor[0], self.emv_score_motor[1])
			episode_of_care.emv_score_verbal=random.randint(self.emv_score_verbal[0], self.emv_score_verbal[1])
			episode_of_care.ABCDE_stability_airways=random.choice([True, False])
			episode_of_care.ABCDE_stability_breathing=random.choice([True, False])
			episode_of_care.ABCDE_stability_circulation=random.choice([True, False])
			episode_of_care.ABCDE_stability_disability=random.choice([True, False])
			episode_of_care.ABCDE_stability_exposure=random.choice([True, False])
			episode_of_care.ambulance_remarks=fake.sentence(nb_words=10)
			episode_of_care.physical_evaluation=fake.sentence(nb_words=10)
			episode_of_care.applied_medication=fake.sentence(nb_words=10)

			if type_of_org == "hospital":
				episode_of_care.neurological_evaluation=fake.sentence(nb_words=10)
				episode_of_care.radiological_imaging=fake.sentence(nb_words=10)
				episode_of_care.image_thrombus_location="ICA"
				episode_of_care.image_aspects=2
				episode_of_care.image_collateral_score=5
				episode_of_care.infart_core_size=5
				episode_of_care.penumbra_size=10
				episode_of_care.medication=fake.sentence(nb_words=3)
				episode_of_care.procedure=fake.sentence(nb_words=10)

		episode_of_care.save()
		return

	##### Generate a direct treatment for the acute stroke or a nono direct treatment ( with transfer between hospitals )
	def generate_direct_stroke_treatment(self, patient):

		encounter = self.generate_encounter(patient)
		call_centre = self.generate_episode_of_care(encounter, "call centre", starter=True)
		ambulance_eoc = self.generate_episode_of_care(encounter, "ambulance")
		hospital = self.generate_episode_of_care(encounter, "hospital", finisher=True)		
		return

	def generate_non_direct_stroke_treatment(self, patient):		
		encounter = self.generate_encounter(patient)
		call_centre = self.generate_episode_of_care(encounter, "call centre", starter=True)
		ambulance_eoc = self.generate_episode_of_care(encounter, "ambulance")
		hospital_eoc = self.generate_episode_of_care(encounter, "hospital")
		ambulance_eoc = self.generate_episode_of_care(encounter, "ambulance")
		hospital_eoc = self.generate_episode_of_care(encounter, "hospital", finisher=True)		
		return

	##### Generate medical records
	# Generate allergy and intolerance records
	def generate_allergy_int_record(self, profile=None, ain=1):		
		for i in range(ain):
			new_allergy_int = Allergy_Intolerance.objects.create(subject=profile, code=3829006, clinicalStatus='active',
			verificationStatus='unconfirmed', type='intolerance', category='medication',
			criticality='low', note=fake.sentence(nb_words=5), recordedDate=datetime.now())		
		return

	# Generate allergy and intolerance records
	def generate_condition_record(self, profile=None, cn=1):		
		for i in range(cn):
			new_condition = Condition.objects.create(subject=profile, clinicalStatus='active', 
				verificationStatus='confirmed', category='problem-list-item', severity=24484000,
				note=fake.sentence(nb_words=5), recordedDate=datetime.now(), code=223003, bodySite=7376007)		
		return

	# Generate allergy and intolerance records
	def generate_family_member_history_record(self, profile=None, fmhn=1):
		for i in range(fmhn):
			new_family_member_history = Family_Member_History.objects.create(subject=profile, name=names.get_first_name(), relationship='COUSIN',
				sex='male', age=random.randint(20,40), estimatedAge=True, deceased=False, condition=368009,
				note=fake.sentence(nb_words=5), recordedDate=datetime.now())		
		return

	def create_patient_profile(self, user, data):
		profile = Patient_Profile.objects.create(
			user=user,
			active=True,
			name=data["firstName"],
			surname=data["lastName"],
			BSN=self.generate_bsn(),
			telecom=fake.phone_number(),
			gender='male',
			birthDate=self.generate_age(),
			address=fake.address().split("\n")[0],
			zipcode=fake.address().split("\n")[1],
			maritalStatus="S",
			communication_language="en-GB",
			managing_organization="",
			contact_name=(names.get_first_name() + " " + names.get_last_name()),
			contact_relationship='U',
			contact_telecom=fake.phone_number(),
			contact_zipcode=fake.address().split("\n")[1],
			contact_address=fake.address().split("\n")[0],
			contact_gender='male',
			last_time_updated=datetime.now()
		)
		return profile

	def generate_patients(self, patients_n=0, records=False, allergies_n=1, conditions_n=1, fmh_n=1):
		#from db_model.models import Patient_Profile, User           

		if records:
			print("     - patients with records")
		else:
			print("     - patients without records")
		
		for i in tqdm(range(patients_n)):
			first_name = names.get_first_name()
			last_name = names.get_last_name()
			salt = self.generate_salt()
			username=first_name+last_name+"-"+salt
			email=first_name+last_name+"@patient.com"

			data = {
				"firstName":first_name,
				"lastName":last_name,
				"email":email,
				"username":username,
				"enabled":"true",
				"keys":["role"],
				"values":["patient"],
				"realmRoles":[]
				#"credentials":[{"type":"password","value":"pwd123","temporary":"false"}]
			}

			userid = self.create_user_using_registration_authority(self.headers, data)
			data["keys"].append("userid")
			data["values"].append(userid)
			self.edit_user_using_registration_authority(self.headers, data)
			user = self.create_user_django(first_name, last_name, username, email, 3)
			profile = self.create_patient_profile(user, data)
			self.create_and_save_SSE_keys(profile, userid)

			if records:
				self.generate_allergy_int_record(profile, allergies_n)
				self.generate_condition_record(profile, conditions_n)
				self.generate_family_member_history_record(profile, fmh_n)
				
				had_stroke_percent = random.randint(0,100)			
				if had_stroke_percent <= self.PERCENT_PATIENTS_HAD_STROKE:
					direct_treatment = random.randint(0,100)					
					if direct_treatment >= self.PERCENT_PATIENTS_NEEDED_TRANSFER:
						self.generate_direct_stroke_treatment(profile)
					else:
						self.generate_non_direct_stroke_treatment(profile)


	# Generates organisations based no the init lists parameters
	# Then, generates teams for each organisation
	def generate_organizations(self, pn, organization_type, teams_number, employees_number):    	
		for i in tqdm(range(pn)):
			if organization_type == "Hospital":
				#print("\nHospital organisations")
				org = self.hospitals.pop()
				org_name = org[0]
				org_alias = org[1]
				org_type = 4
			elif organization_type == "Ambulance":
				#print("\nAmbulance organisations")
				org = self.ambulances.pop()
				org_name = org[0]
				org_alias = org[1]
				org_type = 2
			elif organization_type == "Call Centre":
				#print("\nCall centre organisations")
				org = self.call_centre.pop()
				org_name = org[0]
				org_alias = org[1]
				org_type = 3
			else:
				return print("Organization type must be Hospital, Ambulance or Call Centre")
				
			username = org_name.replace(" ",'').lower()
			user_org = User.objects.create_user(username, username+"@organisation.com", "!asclepios!")
			user_org.first_name = org_name
			user_org.last_name = ""
			user_org.user_type = 5
			user_org.save()

			org = Organization.objects.create(user=user_org, active=True, type=org_type, name=org_name, alias=org_alias, zipcode=fake.address().split("\n")[1],
			address=fake.address().split("\n")[0], country="The Netherlands", telecom=fake.phone_number())        

			#print("     --- creating teams for organization")
			for i in range(teams_number):	           
				Care_Team.objects.create(organization=org, name="Emergency Team {}".format(i),
					tag="ET-{}".format(i), is_active=True, category="Emergency",
					start_datetime=datetime.now(), note=fake.sentence(nb_words=5))

			#print("     --- creating employees for organization")
			for i in range(employees_number):
				first_name = names.get_first_name()
				last_name = names.get_last_name()
				salt = self.generate_salt()
				username=first_name+last_name+"-"+salt
				email=first_name+last_name+"@employee.com"

				data = {
					"firstName":first_name,
					"lastName":last_name,
					"email":email,
					"username":username,		
					"enabled":"true",
					"keys":["role"],
					"values":["health"],
					"realmRoles":[],
					#"credentials":[{"type":"password","value":"pwd123","temporary":"false"}]
				}				

				userid = self.create_user_using_registration_authority(self.headers, data)
				data["keys"].append("userid")
				data["values"].append(userid)
				self.edit_user_using_registration_authority(self.headers, data)
				user = self.create_user_django(first_name, last_name, username, email, 3)

				emp = Employee.objects.create(user=user, org=org, name=first_name, surname=last_name,
					BSN=self.generate_bsn(), email=email, role="", department="")
				
				care_team = Care_Team.objects.filter(organization=org)[0]
				participant_team = Care_Team_Participants.objects.create(identifier=care_team, participant_id=emp, added_date=datetime.now())

	def create_user_using_registration_authority(self, headers, data):
		r = requests.post(url=(self.registration_authority_endpoint+"add-user"), headers=headers, json=data)		
		response = str(r.content.decode('utf-8'))		
		return response.split(" ")[3][2:]

	def edit_user_using_registration_authority(self, headers, data):		
		r = requests.post(url=(self.registration_authority_endpoint+"edit-user/"+data["username"]), headers=headers, json=data)			
		return 		

	def create_user_django(self, first_name, last_name, username, email, user_type):
		user = User.objects.create_user(username=username,
			email=email,
			password="!asclepios!"
		)		
		user.user_type=user_type
		user.save()		
		return user

	def create_and_save_SSE_keys(self, profile, userid):
		raw_policy = 'role:health userid:{} 1of2'.format(userid)
		r = requests.post(url=(self.cpabe_policy_endpoint), headers=self.headers, data=raw_policy)

		verKey = secrets.token_hex(16)
		encKey = secrets.token_hex(16)		
		data = {
			"verKey": verKey,
			"encKey": encKey
			}		
		r = requests.post(url=(self.cpabe_endpoint), headers=self.headers, json=data)		
		keyid = r.content.decode("utf-8")
					
		profile.key_id = keyid
		profile.save()

		self.keys[profile.pk] = {"keyid" : keyid,
								"verKey" : verKey,
								"encKey" : encKey}
		return

	def download_sse_keys_json(self):
		with open("keys.json", "w") as outfile:
			json.dump(self.keys, outfile)
		return

	def dump_synthetic_data_json(self):
		output_json = {}	

		data = serializers.serialize("json", models.Allergy_Intolerance.objects.all())
		output_json["allergy_intolerance"] = data			

		data = serializers.serialize("json", models.Condition.objects.all())
		output_json["condition"] = data			
		
		data = serializers.serialize("json", models.Family_Member_History.objects.all())
		output_json["family_member_history"] = data			

		data = serializers.serialize("json", models.Encounter.objects.all())
		output_json["encounter"] = data			

		data = serializers.serialize("json", models.Episode_Of_Care.objects.all())
		output_json["episode_of_care"] = data			

		with open("synthetic_data.json", "w") as outfile2:
			json.dump(output_json, outfile2)

	def populate(self):		
		warnings.filterwarnings("ignore")
		Organization.objects.all().delete()
		Employee.objects.all().delete()
		User.objects.all().delete()
		Condition.objects.all().delete()
		Allergy_Intolerance.objects.all().delete()
		Family_Member_History.objects.all().delete()
		Encounter.objects.all().delete()
		Episode_Of_Care.objects.all().delete()
		Care_Team.objects.all().delete()
		Care_Team_Participants.objects.all().delete()

		print("\nStarting populate acute stroke script...")
		print("\nCreating organisations...")
		self.generate_organizations(pn=self.N_HOSPITAL_ORG, organization_type="Hospital", teams_number=self.N_ORGANIZATION_TEAMS, employees_number=self.N_EMPLOYEES_PER_ORG)
		self.generate_organizations(pn=self.N_AMBULANCE_ORG, organization_type="Ambulance", teams_number=self.N_ORGANIZATION_TEAMS, employees_number=self.N_EMPLOYEES_PER_ORG)
		self.generate_organizations(pn=self.N_CALL_CENTRE_ORG, organization_type="Call Centre", teams_number=self.N_ORGANIZATION_TEAMS, employees_number=self.N_EMPLOYEES_PER_ORG)

		print("\nCreating patients...")
		self.hospital_orgs = Organization.objects.all().filter(type=4)
		self.ambulance_orgs = Organization.objects.all().filter(type=2)
		self.call_centre_orgs = Organization.objects.all().filter(type=3)
		self.generate_patients(patients_n=self.N_PATIENTS_WITH_RECORDS, records=True)
		self.generate_patients(patients_n=self.N_PATIENTS_WITHOUT_RECORDS, records=False)
		self.download_sse_keys_json()
		self.dump_synthetic_data_json()
		return	


## Start
if __name__ == "__main__":
	populate()