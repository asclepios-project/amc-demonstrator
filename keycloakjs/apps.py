from django.apps import AppConfig


class KeycloakjsConfig(AppConfig):
    name = 'keycloakjs'
