var keycloak = Keycloak({
    url: 'http://192.168.0.104:8181/auth',
    realm: 'master',
    clientId: 'amc-client',                    
});

var username;
var ver_key;
var enc_key;

function initKeycloak() {                
    keycloak.init({ onLoad: 'login-required' }).then(function(authenticated) {
        //alert(authenticated ? 'Authenticated' : 'Not authenticated');                    
        keycloak.loadUserProfile().then(function(profile) {
            username = profile["username"];
            retrieve_sse_keys(key_id)
        }).catch(function() {
            //alert('Failed to load user profile');
        });
    }).catch(function() {
        //alert('Failed to initialize');
    });                                
}

function refresh_token(){
    keycloak.updateToken(30).then(function() {
        //console.log("Token refreshed succesfuly!");
        downloadFeatures();
    }).catch(function() {
        //alert('Failed to refresh token');
    });
    return
}

function retrieve_sse_keys(uuid) {
    console.log("Retrieving SSE keys...");
    console.log(uuid)
    console.log(keycloak.token)
    console.log(username)
    sse_keys = getSSEkeys(uuid , username, keycloak.token);
    var result = JSON.parse(sse_keys);
    enc_key = result["encKey"];
    ver_key = result["verKey"];
    //retrieve_data({"keyword":["user_related|211", "model|login.allergy_intolerance"],"condition":"(1*2)"})
    retrieve_data({"keyword":"user_related|242"})
    return
}

function retrieve_data(search_keyword){
    console.log ("Retrieving data...");
    result = search(search_keyword, ver_key, enc_key, key_id, iskey=true, isfe=false, keycloak.token);
    console.log(result.objects)
    innHtml = "";
    for (const obj in result.objects){
        console.log(result.objects[obj]);
        if(result.objects[obj]["model"] == "login.allergy_intolerance"){
            innHtml += '<a id="div-"'+ result.objects[obj]["json_id"] +' class="list-group-item list-group-item-action flex-column align-items-start">';
            innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px;">';
            innHtml += '<h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["allergy_int_type"] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["allergy_int_category"] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["allergy_int_criticality"] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["allergy_int_code"] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">' +result.objects[obj]["allergy_int_note"] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><div id='+ result.objects[obj]["json_id"] +' onclick="remove_ai(this)" class="options mr-auto ml-auto" style="padding: 2px; background-color: #4679bd; border-radius: 32px; width: 96px;"><h6 style="text-align: center; font-size: 12px; font-weight: bold; color: white; margin: 0px; padding: 2px;">Remove </h6></div></div>'

            innHtml += '</div></a>'
        }        
    }
    document.getElementById("loader").style.display = 'none'
    document.getElementById("allergyIntTable").style.display = 'block';
    var element = document.getElementById("allerg_int_objects");
        element.innerHTML = innHtml;
    return
}

function upload_data(json, fileid){
    uploadData(json, fileid.toString(), ver_key, enc_key, key_id, iskey=true, keycloak.token);
}

function delete_data(json_id){
    console.log("deleting")
    document.getElementById("allergyIntTable").style.display = 'none';
    document.getElementById("loader").style.display = 'block'
    deleteData(json_id, ver_key, enc_key, key_id, iskey=true, keycloak.token)
    document.getElementById("div-"+json_id).style.display = 'none';
    document.getElementById("allergyIntTable").style.display = 'block';
}
