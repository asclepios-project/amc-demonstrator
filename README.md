# Acute Stroke Demonstrator
## Developed for ASCLEPIOS project

This repository contains the baseline code for the acute stroke demonstrator (data sharing for treatment and research in acute care for stroke). The demonstrator is a django web application that uses security components from the ASCLEPIOS framework.

## Requirements
First, deploy the [AMC ASCLEPIOS framework docker-compose](https://gitlab.com/asclepios-project/amc-asclepios-local-deployment) and set up the [asclepios-sse-client-node](https://gitlab.com/indie-sleep-demo/asclepios-sse-client-node).

After cloning the repository, change the environment variables:
Change, if necessary, the keycloak environment variables from `asclepiosapi/settings.py` to:
* ***auth_ri*** = "http://`INTERNAL_IP`:8181/auth/realms/master".
* ***OIDC_RP_CLIENT_ID*** = "amc-client"

Change the environment variables from the populate database scripts at `synthetic_data/populate_acute_stroke.py` and `synthetic_data/populate_researcher.py`
* ***server_url*** = "http://`INTERNAL_IP`:8181/auth/"
* ***self.registration_authority_endpoint*** = "http://`INTERNAL_IP`:8083/api/v1/auth"
* ***self.cpabe_endpoint*** = "http://`INTERNAL_IP`:8084/api/v1/put" (on * populate_acute_stroke.py script only)

Change the SSE environment variablesa  at
`sse/static/sse/js/sse.js`
* ***base_url_cp_abe*** = "http://`INTERNAL_IP`:8084/api/v1/put"
* ***base_url_ta*** = "http://`INTERNAL_IP`:8000/api/v1/put"
* ***base_url_sse_Server*** = "http://`INTERNAL_IP`:8080/api/v1/put"

Once done, follow the usage steps

## Usage

To use the application, first needs to populate using the scripts inside `synthetic_data/` folder.

### setup
``` sh
git clone <THIS-REPO-URL.git>
cd ASCLEPIOS-demo/
### Install the requirements
python3 -m pip install -r requirements.txt
### Populate application for acute stroke use case
python3 manage.py shell < synthetic_data/populate_acute_stroke.py
### Populate application for research use case
python3 manage.py shell < synthetic_data/populate_researcher.py
### Use node.js to populate SSE for acute stroke use case
node populate_acute_stroke.js
### Use node.js to populate SSE for research use case
node populate_researcher.js
### Start application
python3 manage.py runserver 8888
```
### Interfaces
To access the application as:
* ***Patient***: go to`localhost:8888/patient/home/` and login with a patient-type user
* ***Call centre employee***: go to`localhost:8888/call_centre/home/` and login with a call-centre-type user
* ***Ambulance employee***: go to`localhost:8888/ambulance/home/` and login with a ambulance-type user
* ***Hospital employee***: go to`localhost:8888/hospital/home/` and login with a hospital-type user
* ***PI/Researcher***: go to`localhost:8888/research/home/` and login with a research/pi-type user


## License
TODO