from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION, ContentType
from django.http import HttpResponseForbidden
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout as django_logout
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from login.forms import *
from datetime import datetime
from django.http import JsonResponse
import random
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION, ContentType
from asclepiosapi.settings import internal_ip, default_login_url
from asclepiosapi import settings
from django.utils.http import urlencode
import pytz

local_tz = 'Europe/Amsterdam'

def generate_id(MODEL, start, end):
    import random
    id = random.randint(start, end)
    while True:
        if not MODEL.objects.filter(identifier=id):
            return id
        else:
            id = random.randint(start, end)

@login_required(login_url='{}'.format(default_login_url))
def login(request):
    if request.user.is_authenticated:
        return redirect("/org/home")

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(username=username, password=password)
        #print(user)
        if user is not None:
            #print(user.user_type)
            if user.user_type is not 5:
                return HttpResponseForbidden()
            auth_login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect("/org/home")
        else:
            return render(request, 'org/login.html')
    else:
        return render(request, 'org/login.html')
    return render(request, 'org/login.html')


##############################################################################################
#########################  Home  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def home(request):
    if request.user.user_type is not 5:
        raise PermissionDenied

    return render(request, 'org/home.html')
    
    """if request.user.organization.organization_type != 3:
        return render(request, 'org/home.html')
    else:
        return redirect('/org/home/Employees/')
        """


##############################################################################################
#########################  Employees  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def employees(request):
    if request.user.user_type is not 5:
            raise PermissionDenied
    
    if request.method == "GET":
        return render(request, 'org/employees.html')

@login_required(login_url='{}'.format(default_login_url))
def create_employee(request):
    if request.user.user_type is not 5:
            raise PermissionDenied

    if request.user.organization.type == 4:
        new_employee_form = Hospital_Employee_Form()

    elif request.user.organization.type == 3:
        new_employee_form = CC_Employee_Form()

    elif request.user.organization.type == 2:
        new_employee_form = Ambulance_Employee_Form()
    
    if request.method == "GET":
        return render(request, 'org/create_employee.html',{
            'new_employee_form' : new_employee_form
        })
    
    if request.method == "POST":
        if request.user.organization.type == 4:
            new_emp = Hospital_Employee_Form(request.POST)

        if request.user.organization.type == 3:
            new_emp = CC_Employee_Form(request.POST)

        if request.user.organization.type == 2:
            new_emp = Ambulance_Employee_Form(request.POST)

        if new_emp.is_valid():
            username = request.POST["employee_username"]
            password = request.POST["employee_password"]
            email = request.POST["employee_email"]

            tmp_user = User.objects.create_user(username, email, password)
            tmp_user.first_name = request.POST['employee_name']
            tmp_user.last_name = request.POST['employee_surname']
            tmp_user.user_type = request.user.organization.type
            tmp_user.save()
                
            new_emp = Employee.objects.create(
                user=tmp_user,
                employee_org=request.user.organization,
                employee_type=request.user.organization.type,
                employee_name=request.POST['employee_name'],
                employee_surname=request.POST['employee_surname'],
                employee_role=request.POST['employee_role'],
                employee_department=request.POST['employee_department']
            )
            new_emp.identifier = generate_id(Employee, 80000000, 89999999)
            new_emp.save()

            messages.success(request, 'Employee successful created! ID {}'.format(new_emp.identifier))

        return render(request, 'org/create_employee.html',{
            'new_employee_form' : new_employee_form
        })

@login_required(login_url='{}'.format(default_login_url))
def manage_employees(request):
    if request.user.user_type is not 5:
            raise PermissionDenied

    if request.method == "GET":
        employee_filtered = []

        filters = {
            "identifier" :request.GET.get('identifier'),            
        }

        if filters["identifier"] != None and len(filters["identifier"]) > 3:
            try:
                employee_filtered = Employee.objects.filter(identifier=filters["identifier"])
            except:
                pass

        employees = Employee.objects.filter(org=request.user.organization)

        return render(request, 'org/manage_employees.html', {"filters" : filters,
        "employees" : employees,
        "employee_response" : employee_filtered})
    

@login_required(login_url='{}'.format(default_login_url))
def manage_employee(request, pk=None):
    employee = Employee.objects.get(pk=pk)
    care_teams = Care_Team.objects.filter(organization=request.user.organization)
    care_teams_belong = Care_Team_Participants.objects.filter(participant_id=employee)

    for care_team in care_teams_belong:
        care_team.added = str(care_team.added_date)[0:16]
    return render(request, 'org/manage_employee.html',{
        "employee" : employee,
        "care_teams" : care_teams,
        "care_teams_belong" : care_teams_belong
    })

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def add_to_team(request, pk=None):
    care_team = Care_Team.objects.get(pk=request.POST["id"])
    employee = Employee.objects.get(pk=pk)
    care_team_part = Care_Team_Participants.objects.create(
        identifier=care_team,
        participant_id=employee,
        added_date=datetime.now(pytz.timezone(local_tz)),
    )

    message = "Member "+employee.name+" "+employee.surname+" added to the team!"
    team_activity = Team_Activities.objects.create(
        care_team=care_team,
        action_type=2,
        title="Member added to the team",
        message="{} {}".format(employee.name, employee.surname),
        activity_date=datetime.now(pytz.timezone(local_tz)),
        employee=employee
    )

    return JsonResponse({"success" : 200})

##############################################################################################
#########################  Teams  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def add_to_team_organization(request, pk=None):
    care_team = Care_Team.objects.get(pk=pk)
    employee = Employee.objects.get(pk=request.POST["id"])
    care_team_part = Care_Team_Participants.objects.create(
        identifier=care_team,
        participant_id=employee,
        added_date=datetime.now(pytz.timezone(local_tz)),
    )

    message = "Member "+employee.name+" "+employee.surname+" added to the team!"
    team_activity = Team_Activities.objects.create(
        care_team=care_team,
        action_type=2,
        title="Member added to the team",
        message="{} {}".format(employee.name, employee.surname),
        activity_date=datetime.now(pytz.timezone(local_tz)),
        employee=employee
    )

    return JsonResponse({"success" : 200})

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def delete_team(request, pk=None):
    care_team = Care_Team.objects.get(pk=pk)
    care_team.delete()
    return JsonResponse({"success" : 200})

##############################################################################################
#########################  Teams  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def teams(request):
    return render(request, 'org/teams.html')

@login_required(login_url='{}'.format(default_login_url))
def create_team(request):
    care_team_form = Care_Team_Form()
    if request.method == "POST":
        care_team_form = Care_Team_Form(request.POST)
        print(care_team_form.is_valid())
        if care_team_form.is_valid():
            new_care_team = care_team_form.save()
            new_care_team.organization = request.user.organization
            new_care_team.start_datetime = datetime.now(pytz.timezone(local_tz))
            new_care_team.identifier = generate_id(Care_Team, 100000000, 199999999)
            new_care_team.save()
            #print("Care team added")

            team_activity = Team_Activities.objects.create(
                care_team=new_care_team,
                action_type=1,
                message="Team created!",
                activity_date=datetime.now(pytz.timezone(local_tz))
            )

            messages.success(request, 'Care Team successful created! ID {}'.format(new_care_team.identifier))

    return render(request, 'org/create_team.html', {
        "new_care_team" : care_team_form
    })
    
    

@login_required(login_url='{}'.format(default_login_url))
def manage_teams(request):
    if request.method == "GET":
        care_team_response = []

        filters = {
            "identifier" :request.GET.get('identifier'),
        }

        if filters["identifier"] != None and len(filters["identifier"]) > 3:
            try:
                care_team_response = Care_Team.objects.filter(identifier=filters["identifier"])
            except:
                pass
        
        care_teams = Care_Team.objects.filter(organization=request.user.organization)

        for care_team in care_teams:
            if care_team.is_active:
                care_team.status = "Active"
            else:
                care_team.status = "Inactive"
            
        for care_team in care_team_response:
            if care_team.is_active:
                care_team.status = "Active"
            else:
                care_team.status = "Inactive"
        
        return render(request, 'org/manage_teams.html', {"filters" : filters,
        "care_teams" : care_teams,
        "care_team_response" : care_team_response})

@login_required(login_url='{}'.format(default_login_url))
def manage_team(request, pk):
    care_team = Care_Team.objects.get(pk=pk, organization=request.user.organization)
    employees = Employee.objects.filter(org=request.user.organization)
    participants = Care_Team_Participants.objects.filter(identifier=care_team)
    activities = Team_Activities.objects.filter(care_team=care_team)

    i = 1
    for act in activities:
        ###
        if i % 2 == 0:
            act.side = ""
        else:
            act.side = "timeline-inverted"
        i += 1
        ###

    return render(request, 'org/manage_team.html',{
        "care_team" : care_team,
        "participants" : participants,
        "activities" : activities,
        "employees" : employees
    })

##############################################################################################
def gen_user():
    rand_id = random.randint(10000,99999)
    resp = "tp-"+str(rand_id)
    return resp
##############################################################################################
#########################  EMERGENCY REQUESTS || START EMERGENCY SESSION  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def start_emergency_session(request, id=None):
    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=id)

        new_pat = Patient_Profile_Form(request.POST)
        if new_pat.is_valid():
            new_pat.save()
            username = gen_user()
            user_pat = User.objects.create_user(username, "emptyemail@example.com", "pwd123!@")
            user_pat.first_name = request.POST["patient_name"]
            user_pat.last_name = request.POST["patient_surname"]
            user_pat.user_type = 1
            user_pat.save()

            new_pat.user = user_pat
            patient_profile = new_pat.save()
            patient_profile.user = user_pat
            patient_profile.save()

            LogEntry.objects.log_action(
                user_id=patient_profile.user.pk,
                content_type_id=ContentType.objects.get_for_model(Encounter).pk,
                object_id=encounter.pk,
                object_repr="Emergency Session Started",
                action_flag=ADDITION,
            )

            encounter.patient = patient_profile
            encounter.save()

            request = Request.objects.create(
                encounter=encounter,
                request_type=1,
                org_requesting=request.user.organization,
                org_requested=request.user.organization,
                answered=False,
                request_date=datetime.now(pytz.timezone(local_tz))
            )

            return redirect("/org/home/Emergency-Request/")

    if request.method == "GET":

        pats = []
        filters = {
            "name" :request.GET.get('name'),
            "surname": request.GET.get('surname'),
            "bsn" : request.GET.get('bsn'),
            "zipcode" : request.GET.get('zipcode'),
        }

        if filters["zipcode"] != None and len(filters["zipcode"]) > 3:
            try:
                pats = Patient_Profile.objects.filter(zipcode=filters["identifier"])
            except:
                pass

        elif filters["name"] != None and len(filters["name"]) >= 3:
            pats = Patient_Profile.objects.filter(name__icontains=filters["name"])
        
        elif filters["surname"] != None and len(filters["surname"]) >= 3:
            pats = Patient_Profile.objects.filter(surname__icontains=filters["surname"])
        
        elif filters["bsn"] != None and len(filters["bsn"]) >= 3:
            pats = Patient_Profile.objects.filter(BSN=filters["BSN"])
        
        new_patient_form = Patient_Profile_Form()

        return render(request, 'org/start_emergency_session.html',{
            "filters" : filters,
            "patients" : pats,
            "new_patient_form" : new_patient_form
        })

##############################################################################################
#########################  Start Emergency Session| CREATE ENCOUNTER  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def create_encounter(request):
    #check_call_center_permission(request.user.user_type)
    new_encounter = Encounter.objects.create(
        org_starter=request.user.organization,
        start_datetime=datetime.now(pytz.timezone(local_tz)),
    )
    return JsonResponse({"id":new_encounter.pk})

##############################################################################################
#########################  END ENCOUNTER  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def end_encounter(request):
    #check_call_center_permission(request.user.user_type)
    print("Ending encounter")
    print(request.POST["req_id"])
    req = get_object_or_404(Request, pk=request.POST["req_id"])
    req.answered = True
    req.save()

    encounter = get_object_or_404(Encounter, pk=req.encounter.pk)
    encounter.org_finisher = request.user.organization
    encounter.end_datetime = datetime.now(pytz.timezone(local_tz))
    encounter.save()    

    eps = Episode_Of_Care.objects.filter(encounter=encounter)
    for i in range(len(eps)):
        if eps[i].end_datetime is None:
            eps[i].end_datetime = datetime.now(pytz.timezone(local_tz))
            eps[i].save()

            if eps[i].tag != "Call Centre":
                team_activities = Team_Activities.objects.create(
                    care_team=eps[i].care_team,
                    action_type=5,
                    title="Access Revoked",
                    message="Emergency ID {}".format(eps[i].encounter.pk),
                    encounter=eps[i].encounter,
                    activity_date=datetime.now(pytz.timezone(local_tz))
                )

    LogEntry.objects.log_action(
        user_id=encounter.patient.user.pk,
        content_type_id=ContentType.objects.get_for_model(Encounter).pk,
        object_id=encounter.pk,
        object_repr="Emergency Session Ended",
        action_flag=ADDITION,
    )
    
    return JsonResponse({})

##############################################################################################
#########################  SEND REQUEST TRANSFERENCE TO ORG  #################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def send_request_to_org(request):
    #check_call_center_permission(request.user.user_type)
    print("Sending request to org")
    #print(request.POST["req_id"])
    req = get_object_or_404(Request, pk=request.POST["req_id"])
    new_req = Request.objects.create(
        encounter=req.encounter,
        request_type=2,
        org_requesting=request.user.organization,
        org_requested=req.transfer_to,
        request_date=datetime.now(pytz.timezone(local_tz)),
        request_ref=req,
        answered=False
    )
    
    return JsonResponse({})

##############################################################################################
#########################  ACCEPT TRANSFERENCE REQUEST FROM ORG   ############################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def accept_request_from_org(request):
    #check_call_center_permission(request.user.user_type)
    print("Accepting request from org")
    req = get_object_or_404(Request, pk=request.POST["req_id"])
    req.answered = True
    req.save()
    
    return JsonResponse({})

##############################################################################################
#########################  ACCEPT TRANSFERENCE REQUEST FROM ORG   ############################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def attr_patient_request_team(request, id=None):
    #check_call_center_permission(request.user.user_type)
    encounter = get_object_or_404(Encounter, pk=id)
    pat = get_object_or_404(Patient_Profile, pk=request.POST["pat_pk"])
    encounter.patient=pat
    encounter.save()

    LogEntry.objects.log_action(
        user_id=pat.user.pk,
        content_type_id=ContentType.objects.get_for_model(Encounter).pk,
        object_id=encounter.pk,
        object_repr="Emergency Session Started",
        action_flag=ADDITION,
    )

    request = Request.objects.create(
        encounter=encounter,
        request_type=1,
        org_requesting=request.user.organization,
        org_requested=request.user.organization,
        answered=False,
        request_date=datetime.now(pytz.timezone(local_tz))
    )
    
    return JsonResponse({})

##############################################################################################
#########################  EMERGENCY REQUESTS  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def emergency_request(request):
    if request.method == "GET":
        #print(request.user.organization.organization_type)
        req_service = Request.objects.filter(org_requested=request.user.organization, request_type=1)
        req_transfer = Request.objects.filter(org_requested=request.user.organization, request_type=2)
        req_discharge = Request.objects.filter(org_requested=request.user.organization, answered=False, request_type=3)

        for req in req_transfer:
            if request.user.organization.organization_type == 2:
                if not req.answered:
                    req.status = "Waiting"
                    req.color = "goldenrod"
                    req.func_ref = "nta"
                    req.action = "Accept"

                elif req.team_requesting is None:
                    req.status = "Assign team"
                    req.color = "goldenrod"
                    req.func_ref = "att"
                    req.action = "Assign"
                else:
                    req.status = "Accepted"
                    req.color = "green"
                    req.func_ref = None
                    req.action = "Nothing to do"


            elif req.transfer_to is not None:
                ref = Request.objects.filter(request_ref=req)
                if len(ref) == 0:
                    req.status = "Send request"
                    req.color = "goldenrod"
                    req.func_ref = "srto"
                    req.action = "Send"
                
                #print(len(ref))
                if len(ref) == 1:
                    if ref[0].answered is False:
                        req.status = "Waiting"
                        req.color = "goldenrod"
                        req.func_ref = None
                        req.action = "Nothing to do"
                    else:
                        req.status = "Request ambulance"
                        req.color = "goldenrod"
                        req.func_ref = "srto_ambulance"
                        req.action = "Request"

                if len(ref) == 2:
                    if ref[1].answered is False:
                        req.status = "Waiting"
                        req.color = "goldenrod"
                        req.func_ref = None
                        req.action = "Nothing to do"
                    else:
                        req.status = "Transfer done"
                        req.color = "green"
                        req.func_ref  = None
                        req.action = "Nothing to do"

            
            else:
                if req.answered is False:
                    req.status = "Waiting"
                    req.color = "goldenrod"
                    req.func_ref = "arfo"
                    req.action = "Accept"

                elif req.team_requesting is None:
                    req.status = "Waiting team"
                    req.color = "goldenrod"
                    req.func_ref = "atte"
                    req.action = "Assign team"
                else:
                    req.status = "Care team assigned"
                    req.color = "green"
                    req.func_ref = None
                    
        for r in req_service:
            if not r.answered:
                r.status = "Waiting"
                r.status_color = "goldenrod"
                r.action = "Assign team"
                r.btn_color = "#4679bd"

            else:
                if r.encounter.end_datetime is not None:
                    r.status = "Finalized"
                    r.status_color = "darkred"
                    r.action = "Nothing to do"
                    r.btn_color = "grey"

                else:
                    r.status = "Ongoing"
                    r.status_color = "green"
                    r.action = "Nothing to do"
                    r.btn_color = "grey"
 
        
        for r in req_discharge:
            r.action = "Discharge"

        teams = Care_Team.objects.filter(organization=request.user.organization)

        ambulance_orgs = Organization.objects.filter(type=2)

        for team in teams:
            if team.is_active:
                team.status = "Active"
                team.color = "green"
            else:
                team.status = "Inactive"
                team.color = "red"

        return render(request, 'org/emergency_request.html',{
            "service_requests" : req_service,
            "transfer_requests": req_transfer,
            "discharge_requests" : req_discharge,
            "teams" : teams,
            "ambulance_orgs" : ambulance_orgs
        })

##############################################################################################
#########################  Attr Team ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def attr_team(request):
    req = get_object_or_404(Request, pk=request.POST["req_id"]) 
    care_team = get_object_or_404(Care_Team, pk=request.POST["team_id"])

    req.answered = True
    req.team_requesting = care_team
    req.save()

    episode_of_care = Episode_Of_Care.objects.create(
        encounter=req.encounter,
        managing_organization=request.user.organization,
        care_team=care_team,
        start_datetime=datetime.now(pytz.timezone(local_tz)),
    )

    if request.user.organization.type == 2:
        episode_of_care.tag = "Ambulance"

    if request.user.organization.type == 4:
        episode_of_care.tag = "Hospital"
    episode_of_care.save()

    team_activities = Team_Activities.objects.create(
        care_team=care_team,
        encounter=req.encounter,
        action_type=5,
        title="New Emergency",
        message="Emergency ID {}".format(req.encounter.pk),
        activity_date=datetime.now(pytz.timezone(local_tz))
    )

    return JsonResponse({"success" : 200})

##############################################################################################
#########################  Choose ambulance org ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def req_amb_org(request, pk=None):
    req = get_object_or_404(Request, pk=request.POST["req_id"]) 
    org = get_object_or_404(Organization, pk=request.POST["org_id"])

    new_req = Request.objects.create(
        encounter=req.encounter,
        request_type=2,
        org_requesting=request.user.organization,
        org_requested=org,
        transfer_to=req.transfer_to,
        request_date=datetime.now(pytz.timezone(local_tz)),
        request_ref=req,
        answered=False
    )
    return JsonResponse({"success" : 200})

##############################################################################################
#########################  Profile  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def org_profile(request):
    return render(request, 'org/org_profile.html')

@login_required(login_url='{}'.format(default_login_url))
def edit_profile(request):
    if request.method == "POST":
        org_profile = Organization_Form(request.POST, instance=request.user.organization)
        if org_profile.is_valid():
            org_profile.save()
            return redirect('/org/home/Organization/')
    else:
        organization_form = Organization_Form(instance=request.user.organization)
        return render(request, 'org/org_profile.html', {"org_form" : organization_form})

##############################################################################################
#########################  Logout  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def logout(request):
    logout_url = settings.OIDC_OP_LOGOUT_ENDPOINT
    return_to_url = request.build_absolute_uri(settings.LOGOUT_REDIRECT_URL)
    django_logout(request)
    return redirect(logout_url + '?' + urlencode({'redirect_uri': return_to_url, 'client_id': settings.OIDC_RP_CLIENT_ID}))
