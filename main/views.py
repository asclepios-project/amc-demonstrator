from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from login.models import *
from login.forms import *
from django.http import JsonResponse
from django.http import HttpResponse
from django.core import serializers
from django.contrib.auth import logout as django_logout
import json, csv
from django.core.files.storage import default_storage
import pydicom
import io, base64, urllib
import matplotlib.pyplot as plt
from datetime import datetime
from asclepiosapi.settings import internal_ip
from django.utils.http import urlencode
from django.utils.safestring import mark_safe
from asclepiosapi.settings import internal_ip, default_login_url
from asclepiosapi import settings



###############################################################################################
############################ Logs #############################################################
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION, ContentType
def save_log(user, action, content_type, object_id, object_repr, change_message):
    LogEntry.objects.log_action(
                user_id=user,
                content_type_id=content_type,
                object_id=object_id,
                object_repr=object_repr,
                action_flag=action,
            )
    return

###############################################################################################
#########################  My Data  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def my_data(request):
    return render(request, 'main/patient/mydata.html')

def get_org_info(request, id):
    response = {"status_code" : 200}
    org_pk = request.GET["org_pk"]
    org = get_object_or_404(Organization, pk=org_pk)
    response["name"] = org.name
    response["phone"] = org.telecom
    response["address"] = org.address
    return JsonResponse(response)

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def data_download(request):
    patient = get_object_or_404(Patient_Profile, pk=request.user.patient_profile.pk)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="patient_record.csv"'

    writer = csv.writer(response, delimiter=',')
    writer.writerow(['Name', 'Surname', 'BSN', 'telecom', 'gender', 'birth date', 'address', 'zipcode', 'marital status', 'communication language'])

    writer.writerow([patient.name, patient.surname, patient.BSN, patient.telecom, patient.gender,
    patient.birthDate, patient.address, patient.zipcode, patient.maritalStatus, patient.communication_language])

    return response



###############################################################################################
#########################  Episodes Of Care  ##################################################
@login_required(login_url='{}'.format(default_login_url))
def episodes_of_care(request):
    encounters = Encounter.objects.filter(patient=request.user.patient_profile)

    for obj in encounters:
        #print(obj.finisher)
        obj.start_date = str(obj.start_datetime)[0:10]
        if obj.end_datetime is not None:
            obj.end_date = str(obj.end_datetime)[0:10]

        

    return render(request,'main/patient/episodes_of_care.html',
    {"encounters" : encounters})

###############################################################################################
#########################  Episodes Of Care  ############################################
@login_required(login_url='{}'.format(default_login_url))
def episode_of_care(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)
    eps = Episode_Of_Care.objects.filter(encounter=encounter)    

    #encounter.start_date = str(encounter.encounter_start_datetime)[0:10]
    for i in range(len(eps)):
        eps[i].ind = i
        eps[i].accepted_date = str(eps[i].accepted_date)[10:19]
        eps[i].start_datetime = str(eps[i].start_datetime)[10:19]
        if eps[i].tag == "Ambulance":
            eps[i].end_datetime = str(eps[i].end_datetime)[10:19] + " +"+str(eps[i].care_team.time_to_revoke)+"min"
        else:
            eps[i].end_datetime = str(eps[i].end_datetime)[10:19]
        
        imuri = None
        dicom_dict = {}
        #print(eps[i].radiological_imaging)
        # if eps[i].radiological_imaging != None:
        #     dicom = default_storage.open(eps[i].radiological_imaging)
        #     dataset = pydicom.dcmread(dicom)
        #     fill_dict(dataset, dicom_dict)
        #     buffer = io.BytesIO()
        #     fig = plt.gcf()
        #     fig.set_facecolor('black')
        #     plt.imshow(dataset.pixel_array, cmap='gray')
        #     plt.axis('off')
        #     plt.savefig(buffer, format='png', dpi=200, facecolor=fig.get_facecolor(), transparent=True)
        #     plt.colorbar()
        #     plt.close()

        #     imsrc = base64.b64encode(buffer.getvalue())
        #     imuri = 'data:image/png;base64,{}'.format(urllib.parse.quote(imsrc))
    

    return render(request,'main/patient/episode_of_care.html', {
        "eps" : eps,
        "encounter" : encounter,
        "imuri" : imuri,
        "dicom_dict" : dicom_dict,
        "keyid": request.user.patient_profile.key_id,
        "user_related": request.user.patient_profile.pk,
        "internal_ip" : internal_ip
    })

def fill_dict(dataset, dicom_dict):
    indent=0
    dont_print = ['Pixel Data', 'File Meta Information Version']

    indent_string = "   " * indent
    next_indent_string = "   " * (indent + 1)

    for data_element in dataset:
        if data_element.name in dont_print:
            print("""<item not printed -- in the "don't print" list>""")
        else:
            repr_value = repr(data_element.value)
            if len(repr_value) > 50:
                repr_value = repr_value[:50] + "..."
            print("{0:s} {1:s} = {2:s}".format(indent_string,
                                                data_element.name,
                                                repr_value))
            dicom_dict[data_element.name] = repr_value

###############################################################################################
#########################  Recent Activities  ############################################
@login_required(login_url='{}'.format(default_login_url))
def recent_activities(request):
    logs = LogEntry.objects.filter(user_id=request.user.id).order_by('action_time')
    logs_json = serializers.serialize('json', logs)
    return render(request,'main/patient/recent_activities.html', {"activities" : json.loads(logs_json)})

    
###############################################################################################
########################## Family Medical Records #############################################
@login_required(login_url='{}'.format(default_login_url))
def family_mh(request):
    family_medical_history = Family_Member_History.objects.filter(subject=request.user.patient_profile)
    amount = len(family_medical_history)
    family_mh_form = Family_Member_History_Form()
    return render(request, 'main/patient/patient_family_medical_history.html', {"data" : family_medical_history,
        "family_mh_form" : family_mh_form,
        "amount": amount,
        "keyid": request.user.patient_profile.key_id,
        "user_related": request.user.patient_profile.pk,
        "internal_ip" : internal_ip})

@login_required(login_url='{}'.format(default_login_url))
def detailed_fmh(request, fmh_id=None):
    family_mh = get_object_or_404(Family_Member_History, pk=fmh_id)
    return render(request, 'main/patient/patient_family_medical_history_detailed.html', {"family_mh" : family_mh})

@login_required(login_url='{}'.format(default_login_url))
def save_fmh(request):
    response_data = {} 
    if request.method == "POST":
        family_mh_form = Family_Member_History_Form(request.POST)
        if (family_mh_form.is_valid()):
            new_fmh = family_mh_form.save()
            new_fmh.user_related = request.user.patient_profile
            new_fmh.save()

            response_data = { "success" : 200,
                "PK" : new_fmh.pk }
            
            object_repr = "Family History Record Saved"
            save_log(request.user.pk, ADDITION, ContentType.objects.get_for_model(Family_Member_History).pk, new_fmh.pk, object_repr,"")

            return JsonResponse(response_data)
        else:
            return JsonResponse(response_data)
    
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def remove_fmh(request, fmh_id=None):
    query = get_object_or_404(Family_Member_History, pk=fmh_id)
    query.delete()
    object_repr = "Family History Record Deleted"
    save_log(request.user.pk, DELETION, ContentType.objects.get_for_model(Family_Member_History).pk, fmh_id, object_repr,"")
    data = {"message" : "ok"}
    return JsonResponse(data)
############################################################################################
############################################################################################


############################################################################################
#########################  Allergy Intolerances  ###########################################
@login_required(login_url='{}'.format(default_login_url))
def save_ai(request):
    resp_dict = {}
    response_data = {} 
    if request.method == "POST":
        allergy_int_form = Allergy_Intolerance_Form(request.POST)
        if (allergy_int_form.is_valid()):

            new_ai = allergy_int_form.save()
            new_ai.user_related = request.user.patient_profile
            ai = new_ai.save()
            response_data = {"success" : 200,
                "PK": new_ai.pk }

            object_repr = "Allergy Intolerance Record Saved"
            save_log(request.user.pk, ADDITION, ContentType.objects.get_for_model(Allergy_Intolerance).pk, new_ai.pk, object_repr,"")
            return JsonResponse(response_data)
        else:
            return JsonResponse(response_data)

@login_required(login_url='{}'.format(default_login_url))
def allergy_intolerance_main(request):    
    allergy_int = Allergy_Intolerance.objects.filter(subject=request.user.patient_profile)
    amount = len(allergy_int)
    ai_form = Allergy_Intolerance_Form()
    return render(request, 'main/patient/allergy_intolerance.html', {"data" : allergy_int,
        "ai_form" : ai_form,
        'amount': amount,
        "keyid": request.user.patient_profile.key_id,
        "user_related": request.user.patient_profile.pk,
        "internal_ip" : internal_ip})

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def remove_ai(request, ai_id=None):
    query = get_object_or_404(Allergy_Intolerance, pk=ai_id)
    query.delete()
    object_repr = "Allergy Intolerance Record Deleted"
    save_log(request.user.pk, DELETION, ContentType.objects.get_for_model(Allergy_Intolerance).pk, ai_id, object_repr,"")
    data = {"message" : "ok"}
    return JsonResponse(data)
############################################################################################
############################################################################################


##########################################################################################
#########################  Conditions  ##################################################
@login_required(login_url='{}'.format(default_login_url))
def conditions(request):
    condition = Condition.objects.filter(subject=request.user.patient_profile)
    amount = len(condition)
    cond_form = Condition_Form()
    return render(request, 'main/patient/conditions.html', {"data" : condition,
        "cond_form" : cond_form,
        'amount': amount,
        "keyid": request.user.patient_profile.key_id,
        "user_related": request.user.patient_profile.pk,
        "internal_ip" : internal_ip})

def save_condition(request):
    response_data = {} 
    if request.method == "POST":
        condition_form = Condition_Form(request.POST)
        print(condition_form.errors)
        if (condition_form.is_valid()):
            new_cond = condition_form.save()
            new_cond.condition_subject = request.user.patient_profile
            new_cond.save()

            response_data = {"success" : 200}

            object_repr = "Condition Record Saved"
            save_log(request.user.pk, ADDITION, ContentType.objects.get_for_model(Medication).pk, new_cond.pk, object_repr,"")
            return JsonResponse(response_data)
        else:
            return JsonResponse(response_data)

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def remove_condition(request, cond_id=None):
    query = get_object_or_404(Condition, pk=cond_id)
    query.delete()
    object_repr = "Condition Record Deleted"
    save_log(request.user.pk, DELETION, ContentType.objects.get_for_model(Condition).pk, cond_id, object_repr,"")
    data = {"message" : "ok"}
    return JsonResponse(data)
############################################################################################
############################################################################################


####################################################################################################
#########################  Main Patient, Patient Profile  #####################################
@login_required(login_url='{}'.format(default_login_url))
def patient_profile(request):    
    return render(request, 'main/patient/patient_profile.html')

@login_required(login_url='{}'.format(default_login_url))
def index_main_patient(request):
    # if Patient_Profile.objects.filter(user=request.user).exists():
    #     #print("Patient profile exists!")
    #     pass
    # else:
    #     Patient_Profile.objects.create(user=request.user,
    #         last_time_updated=datetime.now())
    #     #print("Patient profile created!")
        
    return render(request, 'main/patient/main_patient.html')
############################################################################################
############################################################################################
    
###############################################################################################
#########################  Patient edit profile and emergency contact  ###########################################
@login_required(login_url='{}'.format(default_login_url))
def edit_profile(request):
    if request.method == "POST":
        patient_profile_form = Patient_Profile_Form(request.POST, instance=request.user.patient_profile)
        if patient_profile_form.is_valid():
            patient_profile_form.save()
            object_repr = "Profile updated."
            save_log(request.user.pk, CHANGE, ContentType.objects.get_for_model(Patient_Profile).pk, request.user.patient_profile.pk, object_repr,"")
            return redirect('/patient/home/Profile')
    else:
        patient_profile_form = Patient_Profile_Form(instance=request.user.patient_profile)
        return render(request, 'main/patient/patient_profile.html', {"patient_form" : patient_profile_form})

@login_required(login_url='{}'.format(default_login_url))
def edit_ec(request):
    if request.method == "POST":
        emergency_contact_form = Emergency_Contact_Form(request.POST, instance=request.user.patient_profile)
        if emergency_contact_form.is_valid():
            emergency_contact_form.save()
            object_repr = "Emergency Contact updated."
            save_log(request.user.pk, CHANGE, ContentType.objects.get_for_model(Patient_Profile).pk, request.user.patient_profile.pk, object_repr,"")
            return render(request, 'main/patient/patient_profile.html')
    else:
        emergency_contact_form = Emergency_Contact_Form(instance=request.user.patient_profile)
        return render(request, 'main/patient/patient_profile.html', {"ec_form" : emergency_contact_form})
############################################################################################
############################################################################################


##############################################################################################
#########################  Logout  ###########################################################
@login_required #redirect when user is not logged in
def index_logout(request):
    logout_url = settings.OIDC_OP_LOGOUT_ENDPOINT
    return_to_url = request.build_absolute_uri(settings.LOGOUT_REDIRECT_URL)
    django_logout(request)
    return redirect(logout_url + '?' + urlencode({'redirect_uri': return_to_url, 'client_id': settings.OIDC_RP_CLIENT_ID}))
############################################################################################
############################################################################################