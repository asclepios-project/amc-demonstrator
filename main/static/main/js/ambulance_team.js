$('#createCareTeam').submit(function(e){
    $.post('create_care_team', $(this).serialize(), function(data){
      if (Object.keys(data).length >= 1) {

        window.alert("Your care team has been created.");
        
        
        htmlRender = "<tr>";
        htmlRender += "<td>" + data.care_team_name + "</td>";
        htmlRender += "<td>" + data.care_team_is_active + "</td>";
        htmlRender += "<td>" + data.care_team_start_datetime + "</td>";
        htmlRender += "<td>";
        htmlRender += "<button id='"+data.PK+"' onclick='' class='btn'><i class='fa fa-edit'></i></button>";
        htmlRender += "<button id='"+data.PK+"' onclick='' class='btn'><i class='fa fa-trash-alt'></i></button>";
        htmlRender += "</td>";
        htmlRender += "</tr>";
        
        $('#care_team_form').collapse('hide');
        $("#care_team_table").delay(600).append(htmlRender).hide().fadeIn(500);
      
      }
      else {

        window.alert("Wops, Something went wrong.");

      }
    });
    e.preventDefault();
  });

  function accept_emergency(btn){
    console.log(btn.id);
    var answer = window.confirm("Are you sure you want to accept this invite?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'accept_emergency',
        data: { pk: btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function(response) {
            if(response["success"] == 200){
              alert('You have joined to this emergency!');
              location.reload(true);
            }
        }
      });
    }
  }

  function call_hospital(){
    console.log();
      $.ajax({
        type: 'POST',
        url: 'call_hospital',
        data: {  },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function(data) {
            if (data["success"] == 200 ){
              alert("Hospital called!");
              location.reload(true);
            }
        }
      });
  }