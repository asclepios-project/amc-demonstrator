function post(path, params, method='post') {

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    const form = document.createElement('form');
    form.method = method;
    form.action = path;
    crsf = "{% csrf_token %}";
    form.innerHTML += crsf;
  
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const hiddenField = document.createElement('input');
        hiddenField.type = 'hidden';
        hiddenField.name = key;
        hiddenField.value = params[key];
  
        form.appendChild(hiddenField);
      }
    }
    
    ps = {
      "encounter_tag" : "Emergency Session"}          
    post(path="/main/Emergency-Call-Centre-Prof/create_encounter/", params={}, method="POST")

    document.body.appendChild(form);
    form.submit();
}

function start_emergency_session(){
    console.log();
    var answer = window.confirm("Do you really want to start an emergency session?");
    if (answer){
        $.ajax({
        type: 'POST',
        url: '/main/Emergency-Call-Centre-Prof/create_encounter/',
        data: {},
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function(response) {
            if (response["success"] == 200){
              window.location.href = "Emergency-Session/"+response["encounter_pk"];
            }
          }
      });
    }
}

function choose_patient(btn){
  console.log(btn.id);
  var answer = window.confirm("Are you sure that you selected the correct patient?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'attr_patient/'+btn.id+'/',
      data: { pk: btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(response) {
        if(response["success"] == 200){
          alert('Patient selected !');
          window.location = window.location.href.split("?")[0];
        }
      }
    });
  }
}

function call_ambulance(){
  console.log();
    $.ajax({
      type: 'POST',
      url: 'call_ambulance',
      data: {  },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(data) {
          if (data["success"] == 200 ){
            alert("Ambulance called!");
            location.reload(true);
          }
      }
    });
}