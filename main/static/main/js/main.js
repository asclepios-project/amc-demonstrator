$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

});

function changing_image(id){

  $('#sect > div').map(function() {
    this.style.display = "none";
  });

  var y = document.getElementById((id+"_var"));
  y.style.display = "block";
  
}


function changing_episode(id){
  var x = document.getElementById(id);

  $('#sections > div').map(function() {
    this.style.backgroundColor = "#4679bd";
  });

  var y = document.getElementById(("section_"+id));
  y.style.backgroundColor = "#CED9E5";
  

  $('#episodes > div').map(function() {
    this.style.display = "none  ";
  });

  x.style.display = "block";
  
  //if (x.style.display === "none") {
  // x.style.display = "block";
  //} else {
  //  x.style.display = "none";
  //}
}
function download_data(){
  $.ajax({
      type: 'GET',
      url: 'download_data',
      data: { },
      csrfmiddlewaretoken: '{{ csrf_token }}',
  });
}

function remove_fmh(btn){
    console.log(btn.id);
    $.ajax({
        type: 'DELETE',
        url: 'remove_fmh/'+btn.id+'/',
        data: { pk: btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function() {
            alert('Object deleted!')
        }
    });
}

$('#addFamilyMH').submit(function(e){
    $.post('../save_fmh/', $(this).serialize(), function(data){
      if (Object.keys(data).length > 1) {

        htmlRender = "<tr>";
        htmlRender += "<td>" + data.Relationship + "</td>";
        htmlRender += "<td>" + data.Gender + "</td>";
        htmlRender += "<td>" + data.Born + "</td>";
        htmlRender += "<td>" + data.EstimatedAge + "</td>";
        htmlRender += "<td>" + data.Deceased + "</td>";
        htmlRender += "<td>";
        htmlRender += "<button class='btn'><i class='fa fa-angle-double-right'></i></button>";
        htmlRender += "<button class='btn'><i class='fa fa-edit'></i></button>";
        htmlRender += "<button class='btn'><i class='fa fa-trash'></i></button>";
        htmlRender += "</td>";
        htmlRender += "</tr>";

        window.alert("Family History has been saved !");
        $('#nofmh_image').fadeOut(1000);
        $('#fmh_form').collapse('hide');
        $("#fmh_table").append(htmlRender).hide().fadeIn(1000);

      }
      else {

        window.alert("Family History Failed");

      }
    });
    e.preventDefault();
  });