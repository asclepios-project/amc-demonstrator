var keycloak = Keycloak({
    // url: `http://${internal_ip}:8181/auth`,
    url: `http://161.74.31.93/auth`,
    realm: 'master',
    clientId: 'amc-client',                    
});

var username;
var ver_key;
var enc_key;

function initKeycloak() {                
    keycloak.init({ onLoad: 'check-sso' }).then(function(authenticated) {
        //alert(authenticated ? 'Authenticated' : 'Not authenticated');                    
        keycloak.loadUserProfile().then(function(profile) {
            username = profile["username"];
            console.log(key_id)
            retrieve_sse_keys(key_id)
        }).catch(function() {
            //alert('Failed to load user profile');
        });
    }).catch(function() {
        //alert('Failed to initialize');
    });                                
}

function refresh_token(){
    keycloak.updateToken(30).then(function() {
        //console.log("Token refreshed succesfuly!");
        downloadFeatures();
    }).catch(function() {
        //alert('Failed to refresh token');
    });
    return
}

function retrieve_sse_keys(uuid) {
    console.log("Retrieving SSE keys...");
    console.log(`Internal IP: ${internal_ip}`);
    console.log(uuid)
    console.log(keycloak.token)
    console.log(username)
    var amc_username = "amcadmin"
    sse_keys = getSSEkeys(uuid , username, keycloak.token);
    var result = JSON.parse(sse_keys);
    enc_key = result["encKey"];
    ver_key = result["verKey"];
    //retrieve_data({"keyword":["user_related|211", "model|login.allergy_intolerance"],"condition":"(1*2)"})
    keyword = "subject|"+user_related
    retrieve_data({"keyword":keyword})
    return
}

function retrieve_data(search_keyword){
    console.log ("Retrieving data...");
    result = search(search_keyword, ver_key, enc_key, key_id, iskey=true, isfe=false, keycloak.token);
    console.log(result.objects)
    if (result.count == 0){
      document.getElementById("loader").style.display = 'none'
      document.getElementById("no_records").style.display = 'block';
    }
    else{
      innHtml = "";
      for (const obj in result.objects){
        console.log(result.objects[obj]);
        if(result.objects[obj]["model"] == "login.allergy_intolerance"){            
            innHtml += '<a id="div-"'+ result.objects[obj]["json_id"] +' class="list-group-item list-group-item-action flex-column align-items-start">';
            innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px;">';
            innHtml += '<h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["type"]] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["category"]] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["criticality"]] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ CODES[result.objects[obj]["code"]] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">' +result.objects[obj]["note"] +'</h6></div>'

            innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><div id='+ result.objects[obj]["json_id"] +' onclick="window.alert("Under construction!")" class="options mr-auto ml-auto" style="padding: 2px; background-color: #4679bd; border-radius: 32px; width: 96px;"><h6 style="text-align: center; font-size: 12px; font-weight: bold; color: white; margin: 0px; padding: 2px;">Remove </h6></div></div>'

            innHtml += '</div></a>'
        }        
      }
      document.getElementById("loader").style.display = 'none'
      document.getElementById("allergyIntTable").style.display = 'block';
      var element = document.getElementById("allerg_int_objects");
      element.innerHTML = innHtml;
    }    
    return
}

function upload_data(json, fileid){
    uploadData(json, fileid.toString(), ver_key, enc_key, key_id, iskey=true, keycloak.token);
    location.reload();
}

/*
function delete_data(json_id){
    console.log("deleting")
    document.getElementById("allergyIntTable").style.display = 'none';
    document.getElementById("loader").style.display = 'block'
    deleteData(json_id, ver_key, enc_key, key_id, iskey=true, keycloak.token)
    document.getElementById("div-"+json_id).style.display = 'none';
    document.getElementById("allergyIntTable").style.display = 'block';
}
*/

function remove_ai(btn){
  var answer = window.confirm("Delete Allergy Intolerance?");
  if (answer) {
    //delete_data(btn.id);
  }
}

function download_my_data(){

}

form.addEventListener("submit", function(e) {
  e.preventDefault();
  var data = new FormData(form);
  var json = {}
  for (const [name,value] of data) {    
    json[name] = value    
  }
  fileid = makeid(16);
  json["json_id"] = fileid;
  json["model"] = "login.allergy_intolerance";
  json["subject"] = user_related;  
  $("#loadMe").modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
  });
  setInterval(function(){
    upload_data(json, fileid);
    $("#loadMe").modal("hide");
  }, 1000);
});

function makeid(length) {
    var result           = [];
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result.push(characters.charAt(Math.floor(Math.random() * 
 charactersLength)));
   }
   return result.join('');
}
