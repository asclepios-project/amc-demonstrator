var keycloak = Keycloak({
    // url: `http://${internal_ip}:8181/auth`,
    url: `http://161.74.31.93/auth`,
    realm: 'master',
    clientId: 'amc-client',                    
});

var username;
var ver_key;
var enc_key;

function initKeycloak() {                
    keycloak.init({ onLoad: 'login-required' }).then(function(authenticated) {
        //alert(authenticated ? 'Authenticated' : 'Not authenticated');                    
        keycloak.loadUserProfile().then(function(profile) {
            username = profile["username"];
            retrieve_sse_keys(key_id)
        }).catch(function() {
            //alert('Failed to load user profile');
        });
    }).catch(function() {
        //alert('Failed to initialize');
    });                                
}

function refresh_token(){
    keycloak.updateToken(30).then(function() {
        //console.log("Token refreshed succesfuly!");
        downloadFeatures();
    }).catch(function() {
        //alert('Failed to refresh token');
    });
    return
}

function retrieve_sse_keys(uuid) {
    console.log("Retrieving SSE keys...");
    console.log(uuid)
    console.log(keycloak.token)
    console.log(username)
    var amc_username = "amcadmin";
    sse_keys = getSSEkeys(uuid , amc_username, keycloak.token);
    var result = JSON.parse(sse_keys);
    enc_key = result["encKey"];
    ver_key = result["verKey"];
    //retrieve_data({"keyword":["user_related|211", "model|login.allergy_intolerance"],"condition":"(1*2)"})
    keyword = "subject|"+user_related
    retrieve_data({"keyword":keyword})
    return
}

function retrieve_data(search_keyword){
    console.log ("Retrieving data...");
    result = search(search_keyword, ver_key, enc_key, key_id, iskey=true, isfe=false, keycloak.token);
    console.log(result.objects)
    var i = 0;
    innHtml = "";
    for (const obj in result.objects){
      console.log(result.objects[obj]);
      if(result.objects[obj]["model"] == "login.condition"){
        i += 1;
        innHtml += '<a id="div-"'+ result.objects[obj]["json_id"] +' class="list-group-item list-group-item-action flex-column align-items-start">';
        innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'

        innHtml += '<div class="col-2 mt-auto mb-auto" style="padding: 0px;">';
        innHtml += '<h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["severity"]] +'</h6></div>'

        innHtml += '<div class="col-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["note"] +'</h6></div>'

        innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ CODES[result.objects[obj]["code"]] +'</h6></div>'

        innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ CODES[result.objects[obj]["bodySite"]] +'</h6></div>'            

        innHtml += '<div class="col-2 mt-auto mb-auto" style="padding: 0px"><div id='+ result.objects[obj]["json_id"] +' onclick="remove(this)" class="options mr-auto ml-auto" style="padding: 2px; background-color: #4679bd; border-radius: 32px; width: 96px;"><h6 style="text-align: center; font-size: 12px; font-weight: bold; color: white; margin: 0px; padding: 2px;">Remove </h6></div></div>'

        innHtml += '</div></a>'
      }        
    }
    if (i == 0){
      document.getElementById("loader").style.display = 'none'
      document.getElementById("no_records").style.display = 'block';
    }else{
      document.getElementById("loader").style.display = 'none'
      document.getElementById("conditiontable").style.display = 'block';
      var element = document.getElementById("condition_objects");
      element.innerHTML = innHtml;
    }
    return
}

function upload_data(json, fileid){
    uploadData(json, fileid.toString(), ver_key, enc_key, key_id, iskey=true, keycloak.token);
    /*
    innHtml += '<a id="div-"'+ json["json_id"] +' class="list-group-item list-group-item-action flex-column align-items-start">';
    innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'

    innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px;">';
    innHtml += '<h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ json["condition_severity"] +'</h6></div>'

    innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ json["condition_note"] +'</h6></div>'

    innHtml += '<div class="col-sm-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ json["condition_code"] +'</h6></div>'

    innHtml += '<div class="col-sm-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ json["condition_bodySite"] +'</h6></div>'            

    innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><div id='+ json["json_id"] +' onclick="remove_ai(this)" class="options mr-auto ml-auto" style="padding: 2px; background-color: #4679bd; border-radius: 32px; width: 96px;"><h6 style="text-align: center; font-size: 12px; font-weight: bold; color: white; margin: 0px; padding: 2px;">Remove </h6></div></div>'

    innHtml += '</div></a>'

    var element = document.getElementById("condition_objects");
    element.insertAdjacentHTML('beforeend', innHtml);

    $("#loadMe").modal("hide");
    */
    location.reload();

}


function remove(btn){
  var answer = window.confirm("Delete condition record? This action cannot be undone!");
  if (answer) {
     $("#loadDelete").modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
    });
    setInterval(function(){
      deleteData(btn.id.toString(), ver_key, enc_key, key_id, true, keycloak.token);
      $("#loadDelete").modal("hide");
      location.reload();
    }, 1000);    
  }
}

form.addEventListener("submit", function(e) {
  e.preventDefault();
  var data = new FormData(form);
  var json = {}
  for (const [name,value] of data) {    
    json[name] = value    
  }
  fileid = makeid(16);
  json["json_id"] = fileid;
  json["model"] = "login.condition";
  json["subject"] = user_related;  
  $("#loadMe").modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
  });
  setInterval(function(){
    upload_data(json, fileid);    
  }, 1000);    
});

function makeid(length) {
    var result           = [];
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result.push(characters.charAt(Math.floor(Math.random() * 
 charactersLength)));
   }
   return result.join('');
}



/*
$('#addCondition').submit(function(e){
    $.post('save_condition/', $(this).serialize(), function(data){
      if (data["success"] == 200) {
        location.reload(true);
        window.alert("Condition has been saved !");

      }
      else {

        window.alert("Condition failed");

      }
    });
    e.preventDefault();
  });

  function remove_condition(btn){
    console.log(btn.id);
    var answer = window.confirm("Do you really want to delete this Condition?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'remove_condition/'+btn.id+'/',
        data: { pk: btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function() {
            alert('Condition deleted!');
            location.reload(true);
        }
      });
    }
}
*/