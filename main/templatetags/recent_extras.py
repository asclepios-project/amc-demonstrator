from django.template import Library
from django.utils.safestring import mark_safe
from django.http import JsonResponse
from django.core import serializers
import datetime
import json

register = Library()

@register.filter(is_safe=True)
def left_or_right(arg): 
    if arg % 2 == 1:
        return mark_safe("timeline-inverted")
    else:
        return mark_safe("")

@register.filter(is_safe=True)
def format_time(arg): 
    return mark_safe((arg[0:10]+" "+arg[11:19]))

@register.filter(is_safe=True)
def set_color(arg):
    if arg == 1:
        return mark_safe("#003741")
    if arg == 2:
        return mark_safe("#003741")
    if arg == 3:
        return mark_safe("#003741")

@register.filter(is_safe=True)
def safe_js(arg):
    return json.dumps(arg)