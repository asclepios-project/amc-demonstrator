from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from login.models import *
from login.forms import *
from django.http import JsonResponse
from django.http import HttpResponse
from django.core import serializers
import json
from django.contrib.auth import logout
from datetime import datetime

def patient_history_resumed(patient_id_user):
    #print(patient_id.user)

    family_member_history = Family_Member_History.objects.filter(user_related=patient_id_user)
    allergy_intolerance = Allergy_Intolerance.objects.filter(user_related=patient_id_user)
    medications = Medication.objects.filter(user_related=patient_id_user)

    encounters = Encounter.objects.filter(encounter_patient=patient_id_user.patient_profile, encounter_tag="Emergency Session")
    response = {
        "fm_history" : family_member_history,
        "alleries" : allergy_intolerance,
        "medications" : medications,
        "encounters" : encounters
    }
    return family_member_history, allergy_intolerance, medications, encounters

def generate_json(dict_param):
    resp_json = json.dumps(dict_param)
    # send json to sse
    return resp_json