from django.urls import path
from . import views

urlpatterns = [
    #path('', views.index_role, name='index_role'),
    path('home/', views.index_main_patient, name='index_main_patient'),
    path('home/My-Data/', views.my_data, name='my_data'),
    path('home/My-Data/download_data', views.data_download, name='data_download'),
    path('home/Profile/', views.patient_profile, name='patient_profile'),
    path('home/Profile/edit/', views.edit_profile, name='edit_profile'),
    path('home/Profile/edit_ec/', views.edit_ec, name='edit_ec'),
    path('home/Episodes-Of-Care/', views.episodes_of_care, name='episodes_of_care'),
    path('home/Episodes-Of-Care/<int:id>/', views.episode_of_care, name='episode_of_care'),
    path('home/Episodes-Of-Care/<int:id>/get_org_info', views.get_org_info, name='get_org_info'),
    path('home/Allergy-Intolerance/', views.allergy_intolerance_main, name='allergy_intolerance_main'),
    path('home/Allergy-Intolerance/save_ai/', views.save_ai, name='save_ai'),
    path('home/Allergy-Intolerance/remove_ai/<int:ai_id>/', views.remove_ai, name='remove_ai'),
    path('home/Recent-Activities/', views.recent_activities, name='recent_activities'),
    path('home/Conditions/', views.conditions, name='conditions'),
    path('home/Conditions/save_condition/', views.save_condition, name='save_condition'),
    path('home/Conditions/remove_condition/<int:cond_id>/', views.remove_condition, name='remove_condition'),
    #path('home/Family-Medical-History/<int:fmh_id>/', views.detailed_fmh, name='detailed_fmh'),
    path('home/Family-Medical-History/', views.family_mh, name='family_mh'),
    path('home/Family-Medical-History/save_fmh/', views.save_fmh, name='save_fmh'),
    path('home/Family-Medical-History/remove_fmh/<int:fmh_id>/', views.remove_fmh, name='remove_fmh'),
    path('home/logout', views.index_logout, name='index_logout'),
]