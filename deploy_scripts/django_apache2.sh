# Deployment

# Update and install dependencies
sudo apt-get update
sudo apt-get install git
# We are using python3 so we install the dependencies for python3
sudo apt-get install python3-pip apache2 libapache2-mod-wsgi-py3

# Virtual environment
sudo pip3 install virtualenv

# Go to web directory and clone repository
cd /var/www/html/

# ASK GIT REPOSITORY TO ADMIN AND CLONE

sudo mv REPOSITY_NAME amc_demonstrator

###################
####### Installing Virtual Environment dependencies
sudo virtualenv venv
sudo source venv/bin/activate

#### Requirements 
sudo pip install django
sudo pip install django-widget-tweaks
sudo pip install django-phonenumber-field
sudo pip install phonenumbers
sudo pip install django-bootstrap-datepicker-plus

##################
####### Configuring Apache/sites-available .conf file
sudo vim /etc/apache2/sites-available/amc_demonstrator.conf

# Copy the yellow text below
"""
Alias /static /var/www/html/amc_demonstrator/static
<Directory /var/www/html/amc_demonstrator/static>
	Require all granted
</Directory>
<Directory /var/www/html/amc_demonstrator/asclepiosapi>
	<Files wsgi.py>
		Require all granted
	</Files>
</Directory>

#WSGIPythonPath /var/www/html/amc_demonstrator/:/var/www/html/amc_demonstrator/venv/lib/python3.6/site-packages
WSGIDaemonProcess amc python-path=/var/www/html/amc_demonstrator/asclepiosapi:/var/www/html/amc_demonstrator/venv/lib/python3.6/site-packages
WSGIProcessGroup amc
WSGIScriptAlias /controller /var/www/html/amc_demonstrator/asclepiosapi/wsgi.py
"""

sudo a2ensite amc_demonstrator.conf
sudo systemctl restart apache2

# Check if it worked on 127.0.0.1