from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from datetime import datetime
from .models import *

# Create your views here.

@api_view(['GET'])
def api_root(request):
    response = {
        "status" : 200,
        "description" : "REST-API, Contextual attribute provider." 
    }
    return Response(response)

"""
Under construction

@api_view(['GET'])
def acute_care_call_centre(request):
    if request.method == 'GET':        
        pass
    return HttpResponse(status=404)

# Create your views here.
@api_view(['GET'])
def acute_care_ambulance(request):
    if request.method == 'GET':        
        pass
    return HttpResponse(status=404)

# Create your views here.
@api_view(['GET'])
def hospital(request):
    if request.method == 'GET':        
        pass
    return HttpResponse(status=404)

"""
####### ENDPOINT to test with AC-ABAC
@api_view(['GET'])
def get_info(request):
    patient_ID = request.GET.get('patient-ID', None)
    ES_ID = request.GET.get('ES-ID', None)
    user_ID = request.GET.get('user-ID', None)

    response = {
        "status" : 200,
        "get_params":{
            "patient-ID" : patient_ID,
            "ES-ID" : ES_ID,
            "user-ID" : user_ID
        }
    }

    if patient_ID == None:
        response["error"] = "We need the patient-ID param to proceed with the queries."
        return Response(response)


    if ES_ID != None:
        ES_instance = Emergency_Section.objects.all()
    else:
        response["error"] = "We need the ES-ID param to proceed with the queries."
        return Response(response)


    if user_ID != None:
        EOC_instances = Episode_of_care.objects.all()
        teams_list = []
    else:
        response["error"] = "We need the user-ID param to proceed with the queries."
        return Response(response)


    return Response(response)

@api_view(['GET'])
def get_info_from_keyid(request):
    patient_ID = request.GET.get('keyid', None)
    print("Get info from key id, with key id = {}".format(keyid))

    response = {"status": 200}

    return Response(response)    


def get_patient_from_keyuiid():
    return None