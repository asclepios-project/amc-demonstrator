from django.apps import AppConfig


class AttributeProviderAbacConfig(AppConfig):
    name = 'attribute_provider_ABAC'
