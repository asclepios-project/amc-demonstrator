from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION, ContentType
from django.http import HttpResponseForbidden, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as django_logout
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from login.models import *
from django.db.models import Q
from login.forms import *
from datetime import datetime, timedelta, timezone
from django.utils import timezone as tz2
from django.core import serializers
import json
from django.utils.http import urlencode
from asclepiosapi.settings import internal_ip, default_login_url
from asclepiosapi import settings
import pytz

local_tz = 'Europe/Amsterdam'
# pytz.timezone(local_tz)

@login_required(login_url='{}'.format(default_login_url))
def index_login(request):
    if request.user.is_authenticated:
        return redirect("/ambulance/home")

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(username=username, password=password)
        print(user)
        if user is not None:
            print(user.user_type)
            if user.user_type is not 2:
                return HttpResponseForbidden()
            auth_login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect("/ambulance/home")
        else:
            return render(request, 'ambulance/login.html')
    else:
        return render(request, 'ambulance/login.html')
    return render(request, 'ambulance/login.html')


##############################################################################################
#########################  Home  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def home(request):
    # usr = User.objects.get(pk=request.user.pk)
    # usr.user_type = 2
    # usr.save()
    if request.user.user_type is not 2:
        raise PermissionDenied
    return render(request, 'ambulance/home.html')


##############################################################################################
#########################  Profile  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def profile(request):
    if request.user.user_type is not 2:
        raise PermissionDenied
    return render(request, 'ambulance/profile.html')

##############################################################################################
#########################  Emergency Sessions  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def emergency_sessions(request):
    if request.user.user_type is not 2:
        raise PermissionDenied
    
    teams = Care_Team.objects.filter(is_active=True)
    teams_part_of = Care_Team_Participants.objects.filter(participant_id=request.user.employee, identifier__in=teams)

    teams_po = []

    for team in teams:
        for t in teams_part_of:
            if team == t.identifier:
                teams_po.append(team)
                break
    
    ongoing_sessions = Episode_Of_Care.objects.filter(care_team__in=teams_po).order_by('start_datetime')
    
    for os in ongoing_sessions:
        os.enc_id = os.encounter.pk
        #os.start_date = str(os.episode_of_care_encounter.encounter_start_datetime)[0:10]
        #os.episode_of_care_start_datetime = str(os.episode_of_care_start_datetime)[10:19]
        os.status = "Active"
        os.status_color = "green"
        os.action = "Enter session"
        os.bttn_color = "#4679bd"
        os.ac = True

        if os.encounter.end_datetime != None:
            os.status = "Access revoked"
            os.status_color = "red"
            os.action = "No action allowed"
            os.bttn_color = "grey"
            os.ac = False
        
        elif os.end_datetime != None:
            if datetime.now(timezone.utc) < os.end_datetime + timedelta(minutes=os.care_team.time_to_revoke):
                os.status = "{} minutes left".format((os.end_datetime + timedelta(minutes=os.care_team.time_to_revoke) - datetime.now(timezone.utc)).seconds//60)
                os.status_color = "goldenrod"
                os.action = "Enter session"
                os.bttn_color = "#4679bd"
                os.ac = True
            else:
                os.status = "Access revoked"
                os.status_color = "red"
                os.action = "No action allowed"
                os.bttn_color = "grey"
                os.ac = False

    return render(request, 'ambulance/emergency_sessions.html', {
        "sessions" : ongoing_sessions
    })


def make_resume(eps):
    resp = [] 
    for i in range(len(eps)-1):
        if eps[i].tag == "Call Centre":
            form = Episode_Of_Care_Call_Centre_Form(instance=eps[i])
            resp.append(form)
    return resp

@login_required(login_url='{}'.format(default_login_url))
def emergency_session(request, id=None):
    if request.user.user_type is not 2:
        raise PermissionDenied
    
    encounter = get_object_or_404(Encounter, pk=id)
    encounter.eps = Episode_Of_Care.objects.filter(encounter=encounter)
    encounter.eps = make_resume(encounter.eps)

    if encounter.end_datetime is not None:        
        raise PermissionDenied
    
    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

    # Old Encounters
    encounters = Encounter.objects.filter(~Q(pk=encounter.pk), patient=encounter.patient).order_by('start_datetime', )
    for obj in encounters:
        obj.episodes = Episode_Of_Care.objects.filter(encounter=obj)
        obj.date = str(obj.start_datetime)[0:10]
        obj.eps = len(obj.episodes)

        print(obj.eps)

    # Olds Episode of care
    episodes_of_care = Episode_Of_Care.objects.filter(encounter=encounter).order_by('start_datetime')
    call_centre_eoc = episodes_of_care[0]

    episodes_of_care = episodes_of_care[0:(len(episodes_of_care)-1)]

    # This Episode of Care
    try:
        ep = get_object_or_404(Episode_Of_Care, encounter=encounter, care_team__in=teams)
    except:
        eps = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
        ep = eps[len(eps)-1]
    ep.visualized = True
    ep.save()
    
    if ep.end_datetime is not None:
        if (datetime.now(timezone.utc) + timedelta(minutes=30) < ep.end_datetime):
            raise PermissionDenied

    if request.method == "GET":
        episode_of_care_form = Episode_Of_Care_Ambulance_Form(instance=ep)
    if request.method == "POST":
        episode_of_care_form = Episode_Of_Care_Ambulance_Form(request.POST, instance=ep)
        if episode_of_care_form.is_valid():
            episode_of_care_form.save()
            
    # Patient history
    allergies = Allergy_Intolerance.objects.filter(subject=encounter.patient)
    conditions = Condition.objects.filter(subject=encounter.patient)
    fmh = Family_Member_History.objects.filter(subject=encounter.patient)

    # Organizations
    ambulance_orgs = Organization.objects.filter(~Q(type=3),~Q(type=4),~Q(pk=request.user.employee.org.pk))
    hospital_orgs = Organization.objects.filter(~Q(type=3),~Q(type=2),~Q(pk=request.user.employee.org.pk))

    req = Request.objects.filter(~Q(org_requested=request.user.employee.org), encounter=encounter)
    
    encounter.episodes_of_care = Episode_Of_Care.objects.filter(encounter=encounter)
    encounter.date = str(encounter.start_datetime)[0:10]    

    return render(request, 'ambulance/emergency_session.html',{
        "encounter" : encounter,
        "encounters" : encounters,
        "episode_of_care_form" : episode_of_care_form,
        "allergies" : allergies,
        "conditions" : conditions,
        "fmh": fmh,
        "hospital_orgs" : hospital_orgs,
        "ambulance_orgs" : ambulance_orgs,
        "reqs" : req,
        "call_centre_remarks" : call_centre_eoc,
        "ep" : ep,
        "episodes" : episodes_of_care,
        "keyid" : encounter.patient.key_id,
        "patient" : encounter.patient.pk,
        "updated" : ep.updated,
        "json_id" : ep.json_id,
        "internal_ip" : internal_ip,
        "organisation_name" : "{} ({})".format(ep.managing_organization.name, ep.managing_organization.alias),
        "organisation_address" : "{}, {}, {}".format(ep.managing_organization.address, ep.managing_organization.zipcode, ep.managing_organization.country),
        "organisation_phone" : ep.managing_organization.telecom,
        "ep_start_datetime" : str(ep.start_datetime),
        "ep_accepted_datetime" : str(ep.accepted_date),
        "ep_end_datetime" : str(ep.end_datetime),

    })

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def update_episode_of_care_info(request, id=None):    
    encounter = get_object_or_404(Encounter, pk=id)

    if request.method == "POST":
        print("updating ep info")
        encounter = get_object_or_404(Encounter, pk=id)
        json_id = request.POST["json_id"]
        
        care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
        teams = []
        for team in care_teams_part:
            teams.append(team.identifier)

        try:
            ep = get_object_or_404(Episode_Of_Care, encounter=encounter, care_team__in=teams)
        except:
            eps = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
            ep = eps[len(eps)-1]

        ep.json_id = json_id
        ep.updated = True
        ep.save()

        return JsonResponse({})

############################################################################################
################################# Check prmission ############################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def es_check_permission(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)
    permit = True

    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

     # This Episode of Care
    try:
        ep = get_object_or_404(Episode_Of_Care, encounter=encounter, care_team__in=teams)
    except:
        eps = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
        ep = eps[len(eps)-1]

    if encounter.end_datetime != None:
        permit = False

    if ep.end_datetime != None:
        if datetime.now(timezone.utc) + timedelta(minutes=ep.care_team.time_to_revoke) < ep.end_datetime:
            permit = False

    response = {
        "success" : 200,
        "permit" : permit
    }
    return JsonResponse(response)

##############################################################################################
#########################  Request Service  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def pick_patient(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)

    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

    # End call centre or hospital session
    eps = Episode_Of_Care.objects.filter(encounter=encounter)
    for i in range(len(eps)-1):
        if eps[i].end_datetime is None:
            eps[i].end_datetime = datetime.now(pytz.timezone(local_tz))
            eps[i].save()

            if eps[i].tag != "Call Centre":
                team_activities = Team_Activities.objects.create(
                    care_team=eps[i].care_team,
                    action_type=5,
                    title="Access Revoked",
                    message="ID {}".format(eps[i].encounter.pk),
                    encounter=eps[i].encounter,
                    activity_date=datetime.now(pytz.timezone(local_tz))
                )

    # This Episode of Care
    ep = Episode_Of_Care.objects.filter(encounter=encounter, care_team__in=teams)
    if len(ep) > 1:
        ep[1].patient_accepted = True
        ep[1].accepted_date = datetime.now(pytz.timezone(local_tz))
        ep[1].save()
    else:
        ep[0].patient_accepted = True 
        ep[0].accepted_date = datetime.now(pytz.timezone(local_tz))
        ep[0].save()
    return JsonResponse({})

##############################################################################################
#########################  Request Service  #####################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def get_eps(request, id=None):
    if request.user.user_type is not 2:
        raise PermissionDenied

    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=request.POST["id"])
        eps = Episode_Of_Care.objects.filter(episode_of_care_encounter=encounter)
        serdata = serializers.serialize("json", eps)
        return JsonResponse(json.loads(serdata), safe=False)

##############################################################################################
#########################  GET Episodes  #####################################################
def old_eps_view(request, id=None, enc=None):
    encounter = get_object_or_404(Encounter, pk=enc)
    # eps = Episode_Of_Care.objects.filter(episode_of_care_encounter=encounter)    
    return render(request, 'ambulance/episodes_summary.html', 
        {"internal_ip" : internal_ip, "keyid" : encounter.patient.key_id, "encounter" : encounter})

##############################################################################################
#########################  Request Service  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def request_service(request, id=None):
    if request.user.user_type is not 2:
        raise PermissionDenied
    encounter = get_object_or_404(Encounter, pk=id)

    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=id)
        org = get_object_or_404(Organization, pk=request.POST["org"])
        request = Request.objects.create(
            encounter=encounter,
            request_type=1,
            org_requesting=request.user.employee.org,
            org_requested=org,
            answered=False,
            request_date=datetime.now(pytz.timezone(local_tz))
        )
        return JsonResponse({})

##############################################################################################
#########################  Check new episodes  ###############################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def check_new_eoc(request):
    new = False
    care_teams_part =Care_Team_Participants.objects.filter(participant_id=request.user.employee)
    teams = []
    for team in care_teams_part:
        teams.append(team.identifier)

    eps = Episode_Of_Care.objects.filter(care_team__in=teams)
    for ep in eps:
        print(ep.visualized)
        if ep.visualized == False:
            new = True

    response = {
        "sucess" : 200,
        "new" : new
    }
    return JsonResponse(response)

##############################################################################################
#########################  Teams  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def teams(request):
    if request.user.user_type is not 2:
        raise PermissionDenied

    care_team_response = []

    filters = {
        "identifier" :request.GET.get('identifier'),
    }
    
    if filters["identifier"] != None and len(filters["identifier"]) > 3:
        try:
            care_team_response = Care_Team.objects.filter(identifier=filters["identifier"])
        except:
            pass
            
    care_teams_part = Care_Team_Participants.objects.filter(participant_id=request.user.employee)


    for team in care_teams_part:
        if team.identifier.is_active:
            team.status = "Active"
            team.color = "green"
        else:
            team.status = "Inactive"
            team.color = "red"
        team.created_date = str(team.identifier.start_datetime)[0:10]
    
    for care_team in care_team_response:
        care_team.created_date = str(care_team.start_datetime)[0:10]
        if care_team.is_active:
            care_team.status = "Active"
            care_team.color = "green"
        else:
            care_team.status = "Inactive"
            care_team.color = "red"

    print(care_team_response)

    return render(request, 'ambulance/teams.html', {"filters" : filters,
        "care_teams" : care_teams_part,
        "care_team_response" : care_team_response
    })

@login_required(login_url='{}'.format(default_login_url))
def team_instance(request, id=None):
    if request.user.user_type is not 2:
        raise PermissionDenied
    
    care_team = get_object_or_404(Care_Team, pk=id)
    participants = Care_Team_Participants.objects.filter(identifier=care_team)
    
    activities = Team_Activities.objects.filter(care_team=care_team)

    i = 1
    for act in activities:
        ###
        if i % 2 == 0:
            act.side = ""
        else:
            act.side = "timeline-inverted"
        i += 1
        ###  

    return render(request, 'ambulance/team_instance.html', {
        "care_team" : care_team,
        "participants" : participants,
        "activities" : activities
    })

##############################################################################################
#########################  Logout  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def logout(request):
    logout_url = settings.OIDC_OP_LOGOUT_ENDPOINT
    return_to_url = request.build_absolute_uri(settings.LOGOUT_REDIRECT_URL)
    django_logout(request)
    return redirect(logout_url + '?' + urlencode({'redirect_uri': return_to_url, 'client_id': settings.OIDC_RP_CLIENT_ID}))

##############################################################################################
#########################  Request Service  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def get_eps(request, id=None):
    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=request.POST["id"])
        eps = Episode_Of_Care.objects.filter(episode_of_care_encounter=encounter)
        serdata = serializers.serialize("json", eps)
        #print(serdata)
        return JsonResponse(json.loads(serdata), safe=False)