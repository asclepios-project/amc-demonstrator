var keycloak = Keycloak({
    // url: `http://${internal_ip}:8181/auth`,
    url: `http://161.74.31.93/auth`,
    realm: 'master',
    clientId: 'amc-client',                    
});

var username;
var ver_key;
var enc_key;
var episodes_of_care = [];
var json_ids = []

var current_id = null;

function initKeycloak() {                
    keycloak.init({ onLoad: 'login-required' }).then(function(authenticated) {
        //alert(authenticated ? 'Authenticated' : 'Not authenticated');                    
        keycloak.loadUserProfile().then(function(profile) {
            username = profile["username"];
            retrieve_sse_keys(key_id)
        }).catch(function() {
            //alert('Failed to load user profile');
        });
    }).catch(function() {
        //alert('Failed to initialize');
    });                                
}

function refresh_token(){
    keycloak.updateToken(30).then(function() {
        //console.log("Token refreshed succesfuly!");
        downloadFeatures();
    }).catch(function() {
        //alert('Failed to refresh token');
    });
    return
}

function retrieve_sse_keys(uuid) {
    console.log("Retrieving SSE keys...");
    console.log(uuid)
    console.log(keycloak.token)
    console.log(username)
    sse_keys = getSSEkeys(uuid , username, keycloak.token);
    var result = JSON.parse(sse_keys);
    enc_key = result["encKey"];
    ver_key = result["verKey"];
    //retrieve_data({"keyword":["user_related|211", "model|login.allergy_intolerance"],"condition":"(1*2)"})
    keyword = "encounter|"+encounter
    retrieve_data({"keyword":keyword})
    return
}

function show(id){
    console.log("Changing display for id");
    var changing = 'section-'+id;
    console.log(changing);
    document.getElementById(current_id).style.display = 'none';
    document.getElementById(changing).style.display = '';
    current_id = changing;
}

function get_org_information(pk){
  $.ajax({
      type: 'GET',
      url: 'get_org_info',
      data: { org_pk: pk },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(res) {
          console.log(res);
      }
    });
}

function retrieve_data(search_keyword){
    console.log ("Retrieving data...");
    result = search(search_keyword, ver_key, enc_key, key_id, iskey=true, isfe=false, keycloak.token);
    //console.log(result.objects)    
    innHtml = "";
    innHtml2 = "";    
    var first = true;
    var org_info = "";
    for (const obj in result.objects){      
        //console.log(result.objects[obj]);
        json_ids.push(result.objects[obj]["json_id"]);
        if(result.objects[obj]["model"] == "login.episode_of_care"){
            // innHtml += '<a id="div-'+ result.objects[obj]["json_id"] +'" class="list-group-item list-group-item-action flex-column align-items-start">';
            // innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'

            // innHtml += '<div class="col-sm-4 mt-auto mb-auto" style="padding: 0px;">';
            // innHtml += '<h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["managing_organization"] +'</h6></div>'

            // innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["accepted_date"].substring(11, 19) +'</h6></div>'

            // innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["start_datetime"].substring(11, 19) +'</h6></div>'

            // innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["end_datetime"].substring(11, 19) +'</h6></div>'            

            // innHtml += '<div class="col-sm-2 mt-auto mb-auto" style="padding: 0px"><div id='+ result.objects[obj]["json_id"] +' onclick="show(this.id)" class="options mr-auto ml-auto" style="padding: 2px; background-color: #4679bd; border-radius: 32px; width: 96px;"><h6 style="text-align: center; font-size: 12px; font-weight: bold; color: white; margin: 0px; padding: 2px;">Show </h6></div></div>'

            // innHtml += '</div></a>'
            // //###############################################
            
            // // org_info = get_org_information(result.objects[obj]["managing_organization"]);                      

            // if (first){
            //   current_id = "section-"+ result.objects[obj]["json_id"];
            //   innHtml2 += '<div id="section-'+ result.objects[obj]["json_id"] + '" style="padding: 16px;"><div class="card" style="box-shadow: 2px 2px 15px rgba(33,33,33,.2);border-radius: 16px; padding: 8px; background-color: #CED9E5;">';
            //   first = false;
            // }else{
            //   innHtml2 += '<div id="section-'+ result.objects[obj]["json_id"] + '"  style="padding: 16px; display: none;"><div style="box-shadow: 2px 2px 15px rgba(33,33,33,.2); border-radius: 16px; padding: 8px; background-color: #CED9E5;">';
            // }


            // innHtml2 += '<div class="row"><div class="col-sm-5"><p style="font-size: 14px;">Organization</p></div><div class="col-sm-7"><p style="font-size: 14px; font-weight: bold;">'+ org_info["name"] +'</p></div></div>';

            // innHtml2 += '<div class="row"><div class="col-sm-5"><p style="font-size: 14px;">Address</p></div><div class="col-sm-7"><p style="font-size: 14px; font-weight: bold;">' + org_info["address"] + '</p></div></div>';

            // innHtml2 += '<div class="row" style=""><div class="col-sm-5"><p style="font-size: 14px;">Phone</p></div><div class="col-sm-7"><p style="font-size: 14px; font-weight: bold;">' + org_info["phone"] + '</p></div></div></div>';

            innHtml2 = '<div id="{{ ep.pk }}" style="padding: 16px;">'

            innHtml2 += '<div style="box-shadow: 2px 2px 15px rgba(33,33,33,.2); border-radius: 16px; padding: 8px; background-color: #CED9E5;">';

            innHtml2 += '<div class="row"><div class="col-sm-5"><p style="font-size: 14px;">Organization</p></div><div class="col-sm-7"><p style="font-size: 14px; font-weight: bold;">';
            innHtml2 += result.objects[obj]["organisation_name"];
            innHtml2 += '</p></div></div>';            

            innHtml2 += '<div class="row"><div class="col-sm-5"><p style="font-size: 14px;">Address</p></div><div class="col-sm-7"><p style="font-size: 14px; font-weight: bold;">'
            innHtml2 += result.objects[obj]["organisation_address"];
            innHtml2 += '</p></div></div>';

            innHtml2 += '<div class="row"><div class="col-sm-5"><p style="font-size: 14px;">Phone number</p></div><div class="col-sm-7"><p style="font-size: 14px; font-weight: bold;">';
            innHtml2 += result.objects[obj]["organisation_phone"];
            innHtml2 += '</p></div></div>';   

            innHtml2 += '</div>';



            if(result.objects[obj]["tag"] == "call centre"){
              innHtml2 += '<div class="row" style="font-size: 14px; margin-top: 8px;"><div class="col-sm-5"><p id="name">Last time seen well</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["last_time_seen_well"] +'</strong></p>  </div></div>';       

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Time of the onset</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["time_of_the_onset"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Localion of the onset</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["local_of_the_onset"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Patient location</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["patient_location"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Who called</p></div><div class="col-sm-7"><p><strong>'+ OTHER_CODES[result.objects[obj]["who_called"]] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Remarks</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["call_centre_remarks"] +'</strong></p>  </div></div>';
            }

            if(result.objects[obj]["tag"] == "ambulance"){
              innHtml2 += '<div class="row" style="font-size: 14px; margin-top: 8px;"><div class="col-sm-5"><p id="name">Time of arrival</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["accepted_date"].substring(11,19) +'</strong></p>  </div></div>';       

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Heart rate (bpm)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["heart_rate"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Blood pressure systolic (mmHg)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["blood_pressure_systolic"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Blood pressure diastoli (mmHg)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["blood_pressure_diastolic"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Oxygen saturation (%)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["oxygen_saturation"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Breathing frequency (bpm)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["call_centre_remarks"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Glucose (mmol/L)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["glucose"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Usage of coagulant medicine</p></div><div class="col-sm-7"><p><strong>'+ OTHER_CODES[result.objects[obj]["usage_of_coagulant_medicine"]] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">International normalized ratio</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["international_normalized_ratio"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Emv score eyes</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["emv_score_eyes"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Emv score motor</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["emv_score_motor"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Emv score verbal</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["emv_score_verbal"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">ABCDE stability airways | breathing | circulation | disability | exposure</p></div><div class="col-sm-7"><p><strong>'+ OTHER_CODES[result.objects[obj]["ABCDE_stability_airways"]] + " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_breathing"]] + " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_circulation"]] + " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_disability"]] + " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_exposure"]] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Ambulance Remarks</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["ambulance_remarks"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Physical evaluation</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["physical_evaluation"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Applied medication</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["applied_medication"] +'</strong></p>  </div></div>';
            }

            if(result.objects[obj]["tag"] == "hospital"){
              innHtml2 += '<div class="row" style="font-size: 14px; margin-top: 8px;"><div class="col-sm-5"><p id="name">Time of arrival</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["accepted_date"].substring(11,19) +'</strong></p>  </div></div>';       

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Heart rate (bpm)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["heart_rate"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Blood pressure systolic (mmHg)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["blood_pressure_systolic"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Blood pressure diastoli (mmHg)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["blood_pressure_diastolic"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Oxygen saturation (%)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["oxygen_saturation"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Breathing frequency (bpm)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["call_centre_remarks"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Glucose (mmol/L)</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["glucose"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Usage of coagulant medicine</p></div><div class="col-sm-7"><p><strong>'+ OTHER_CODES[result.objects[obj]["usage_of_coagulant_medicine"]] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">International normalized ratio</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["international_normalized_ratio"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Emv score eyes</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["emv_score_eyes"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Emv score motor</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["emv_score_motor"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Emv score verbal</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["emv_score_verbal"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">ABCDE stability airways | breathing | circulation | disability | exposure</p></div><div class="col-sm-7"><p><strong>'+ OTHER_CODES[result.objects[obj]["ABCDE_stability_airways"]] + " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_breathing"]]+ " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_circulation"]] + " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_disability"]] + " | " + OTHER_CODES[result.objects[obj]["ABCDE_stability_exposure"]] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Neurological evaluation</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["neurological_evaluation"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Radiological imaging</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["radiological_imaging"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Image thrombus location</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["image_thrombus_location"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Image aspects</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["image_aspects"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Image collateral score</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["image_collateral_score"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Infart core size</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["infart_core_size"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Penumbra size</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["penumbra_size"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Medication</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["medication"] +'</strong></p>  </div></div>';

              innHtml2 += '<div class="row" style="font-size: 14px;"><div class="col-sm-5"><p id="name">Procedure</p></div><div class="col-sm-7"><p><strong>'+ result.objects[obj]["procedure"] +'</strong></p>  </div></div>';
            }
            innHtml2 += '</div>';            
            var divtest = document.createElement("div");
            divtest.classList.add('col-sm');
            divtest.classList.add('card');            
            divtest.style.height = "700px";
            divtest.style.minWidth = "500px";
            divtest.style.maxWidth = "500px";
            divtest.style.width = "500px";
            divtest.style.overflow = "auto";
            divtest.style.margin = "4px";
            divtest.style.padding = "4px";
            // div.style.box_shadow = "2px 2px 15px rgba(33,33,33,.2)";    
            divtest.innerHTML = innHtml2;
            // innHtml += innHtml2
            console.log(result.objects[obj]);
        }
        var objTo = document.getElementById('summary');
        objTo.appendChild(divtest);
    }
        
    document.getElementById("loader_ep").style.display = 'none'
    document.getElementById("summary").style.display = 'block';
    // var element = document.getElementById("episodes_of_care_objects");
    // element.innerHTML = innHtml;
    // var element = document.getElementById("episodes");
    // element.innerHTML = innHtml2;    
    return
}