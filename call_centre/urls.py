from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_login, name='index_login'),
    path('home/', views.home, name='home'),
    path('home/create_encounter', views.create_encounter, name='create_encounter'),
    path('home/logout', views.logout, name='logout'),
    path('home/Profile/', views.profile, name='profile'),
    path('home/Team/', views.team, name='team'),
    path('home/Start-Emergency-Session/<int:id>/', views.start_emergency_session, name='start_emergency_session'),
    path('home/Start-Emergency-Session/<int:id>/attr_patient', views.attr_patient, name='attr_patient'),
    path('home/Emergency-Session/', views.emergency_sessions, name='emergency_sessions'),
    path('home/Emergency-Session/<int:id>/', views.emergency_session, name='emergency_session'),
    path('home/Emergency-Session/<int:id>/update_episode_of_care_info', views.update_episode_of_care_info, name='update_episode_of_care_info'),
    path('home/Emergency-Session/<int:id>/Summary/<int:enc>/', views.old_eps_view, name='old_eps_view'),    
    path('home/Emergency-Session/<int:id>/get_eps', views.get_eps, name='get_eps'),
    path('home/Emergency-Session/<int:id>/request', views.request_service, name='request_service'),
    path('home/Emergency-Session/<int:id>/check_permission', views.es_check_permission, name='es_check_permission'),
]