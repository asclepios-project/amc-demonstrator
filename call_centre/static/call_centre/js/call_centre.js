// ==================================================================
// ==================================================================
// ==================================================================
var myWindow = null;

function retrieve(id){
  url = window.location.href + "Summary/" + id +"/";
  open(url, "", "height=720, width=500");
}

function start_emergency_session(){
    var answer = window.confirm("Do you really want to start an emergency session?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: '/call_centre/home/create_encounter',
        data: {},
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function(data) {
            location.href ='/call_centre/home/Start-Emergency-Session/'+ data["id"] + '/';
        }
      });
    }
}

function attr_patient(pk){
  var answer = window.confirm("Confirm patient?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'attr_patient',
        data: {pk : pk},
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function(data) {
            location.href = '/call_centre/home/Emergency-Session/'+ data["id"] + '/';
        }
      });
    }
}

function request_service(btn){
  var answer = window.confirm("Are you sure on this request?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'request',
        data: { org : btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function(data) {
            location.reload(true);
        }
      });
    }
}