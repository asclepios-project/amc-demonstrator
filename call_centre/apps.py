from django.apps import AppConfig


class CallCentreConfig(AppConfig):
    name = 'call_centre'
