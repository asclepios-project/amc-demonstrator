from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION, ContentType
from django.http import HttpResponseForbidden, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as django_logout
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from login.models import *
from login.forms import *
from datetime import datetime
from django.db.models import Q
import random, json
from django.core import serializers
from asclepiosapi import settings
from asclepiosapi.settings import internal_ip, default_login_url
from django.utils.http import urlencode
import pytz

local_tz = 'Europe/Amsterdam'
# pytz.timezone(local_tz)

@login_required(login_url='{}'.format(default_login_url))
def index_login(request):
    if request.user.is_authenticated:
        return redirect("/call_centre/home")

    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")

        user = authenticate(username=username, password=password)
        #print(user)
        if user is not None:
            print(user.user_type)
            if user.user_type is not 3:
                return HttpResponseForbidden()
            auth_login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect("/call_centre/home")
        else:
            return render(request, 'call_centre/login.html')
    else:
        return render(request, 'call_centre/login.html')
    return render(request, 'call_centre/login.html')
    
def check_call_center_permission(user_type):
    if user_type is not 3:
        raise PermissionDenied
    
@login_required(login_url='{}'.format(default_login_url))
def home(request):
    check_call_center_permission(request.user.user_type)
    return render(request, 'call_centre/home.html')

##############################################################################################
#########################  GET Episodes  #####################################################
@login_required(login_url='{}'.format(default_login_url))
def old_eps_view(request, id=None, enc=None):
    encounter = get_object_or_404(Encounter, pk=enc)
    eps = Episode_Of_Care.objects.filter(encounter=encounter)
    print(eps)
    for ep in eps:
        print(ep.tag)
    return render(request, 'call_centre/episodes_summary.html', {"internal_ip" : internal_ip, "keyid" : encounter.patient.key_id, "encounter" : encounter})

##############################################################################################
@login_required(login_url='{}'.format(default_login_url))
def gen_user():
    rand_id = random.randint(10000,99999)
    resp = "tp-"+str(rand_id)
    return resp
##############################################################################################
#########################  Start Emergency Session  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def start_emergency_session(request, id=None):
    check_call_center_permission(request.user.user_type)
    encounter = get_object_or_404(Encounter , pk=id)
    pats = []

    if request.method == "POST":
        new_pat = Patient_Profile_Form(request.POST)
        if new_pat.is_valid():
            new_pat.save()
            username = gen_user()
            user_pat = User.objects.create_user(username, "emptyemail@example.com", "pwd123!@")
            user_pat.first_name = request.POST["patient_name"]
            user_pat.last_name = request.POST["patient_surname"]
            user_pat.user_type = 1
            user_pat.save()

            new_pat.user = user_pat
            patient_profile = new_pat.save()
            patient_profile.user = user_pat
            patient_profile.save()

            LogEntry.objects.log_action(
                user_id=patient_profile.user.pk,
                content_type_id=ContentType.objects.get_for_model(Encounter).pk,
                object_id=encounter.pk,
                object_repr="Emergency Session Started",
                action_flag=ADDITION,
            )

            encounter.patient = patient_profile
            encounter.save()

            time = datetime.now(pytz.timezone(local_tz))
            episode_of_care = Episode_Of_Care.objects.create(
                encounter=encounter,
                tag="Call Centre",
                care_manager=request.user.employee,
                managing_organization=request.user.employee.org,
                start_datetime=time,
                accepted_date=time
            )

            return redirect('/call_centre/home/Emergency-Session/{}'.format(encounter.pk))


    if encounter.patient is not None:
        return redirect('/call_centre/home/Emergency-Session/')
    
    filters = {
        "name" :request.GET.get('name'),
        "surname": request.GET.get('surname'),
        "BSN" : request.GET.get('BSN'),
        "zipcode" : request.GET.get('zipcode'),
    }


    if filters["zipcode"] != None and len(filters["zipcode"]) > 3:
        try:
            pats = Patient_Profile.objects.filter(zipcode__icontains=filters["zipcode"])
        except:
            pass

    elif filters["name"] != None and len(filters["name"]) >= 3:
        pats = Patient_Profile.objects.filter(name__icontains=filters["name"])
        
    elif filters["surname"] != None and len(filters["surname"]) >= 3:
        pats = Patient_Profile.objects.filter(surname__icontains=filters["surname"])
        
    elif filters["BSN"] != None and len(filters["BSN"]) >= 3:
        pats = Patient_Profile.objects.filter(BSN=filters["BSN"])

    new_patient_form = Patient_Profile_Form()

    
    return render(request, 'call_centre/start_emergency_session.html', {
        "filters" : filters,
        "patients" : pats,
        "new_patient_form" : new_patient_form
    })

##############################################################################################
#########################  Start Emergency Session| CREATE ENCOUNTER  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def create_encounter(request):
    check_call_center_permission(request.user.user_type)
    new_encounter = Encounter.objects.create(
        starter=request.user.employee,
        org_starter=request.user.employee.org,
        start_datetime=datetime.now(pytz.timezone(local_tz)),
    )
    return JsonResponse({"id":new_encounter.pk})

##############################################################################################
#########################  Start Emergency Session| ATTR PATIENT  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def attr_patient(request, id=None):
    check_call_center_permission(request.user.user_type)

    encounter = Encounter.objects.get(pk=id)
    patient = Patient_Profile.objects.get(pk=request.POST["pk"])

    encounter.patient = patient
    encounter.save()

    LogEntry.objects.log_action(
        user_id=patient.user.pk,
        content_type_id=ContentType.objects.get_for_model(Encounter).pk,
        object_id=encounter.pk,
        object_repr="Emergency Session Started",
        action_flag=ADDITION,
    )

    episode_of_care = Episode_Of_Care.objects.create(
        encounter=encounter,
        tag="Call Centre",
        care_manager=request.user.employee,
        managing_organization=request.user.employee.org,
        start_datetime=datetime.now(pytz.timezone(local_tz))
    )

    response = {
        "success" : 200,
        "id" : encounter.pk,
    }
    return JsonResponse(response)

############################################################################################
################################# Check prmission ############################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def es_check_permission(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)
    permit = True
    ep = get_object_or_404(Episode_Of_Care, care_manager=request.user.employee, encounter=encounter)
    if ep.end_datetime != None:
        permit = False  
    response = {
        "success" : 200,
        "permit" : permit
    }
    return JsonResponse(response)

##############################################################################################
#########################  Emergency Session ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def emergency_sessions(request, id=None):
    encounters = Encounter.objects.filter(starter=request.user.employee)
    
    for obj in encounters:
        episodes = Episode_Of_Care.objects.filter(encounter=obj)
        if len(episodes) > 0:
            obj.episode = get_object_or_404(Episode_Of_Care, encounter=obj, managing_organization=request.user.employee.org)
            obj.avail = True
        
            if obj.episode.end_datetime is None:
                obj.ac = True
                obj.active = "Active"
                obj.color = "green"
                obj.action = "Enter episode"
                obj.bttn_color = "#4679bd"
            else:
                obj.ac = False
                obj.active = "Access revoked"
                obj.color = "red"
                obj.action = "No action allowed"
                obj.bttn_color = "grey"

    return render(request, 'call_centre/emergency_sessions.html',{
        "encounters" : encounters,
    })

##############################################################################################
#########################  Request Service  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def get_eps(request, id=None):
    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=request.POST["id"])
        eps = Episode_Of_Care.objects.filter(encounter=encounter)
        serdata = serializers.serialize("json", eps)
        return JsonResponse(json.loads(serdata), safe=False)


##############################################################################################
#########################  Emergency Session ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def emergency_session(request, id=None):
    encounter = get_object_or_404(Encounter, pk=id)
    encounter.date = str(encounter.start_datetime)[0:10]
    if encounter.end_datetime is not None:
        raise PermissionDenied

    # Old Encounters
    encounters = Encounter.objects.filter(~Q(pk=encounter.pk), patient=encounter.patient).order_by('start_datetime', )

    for obj in encounters:
        if obj.end_datetime != None:
            pass
        else:
            obj.color = "green"
        episodes = Episode_Of_Care.objects.filter(encounter=obj)
        obj.date = str(obj.start_datetime)[0:10]
        obj.eps = len(episodes)

    # Olds Episode of care
    episodes_of_care = Episode_Of_Care.objects.filter(encounter=encounter).order_by('start_datetime')


    # This Episode of Care
    ep = get_object_or_404(Episode_Of_Care, care_manager=request.user.employee, encounter=encounter)
    if ep.end_datetime is not None:
        raise PermissionDenied

    print(ep.updated)

    if request.method == "GET":
        episode_of_care_form = Episode_Of_Care_Call_Centre_Form(instance=ep)
    
    if request.method == "POST":
        episode_of_care_form = Episode_Of_Care_Call_Centre_Form(request.POST, instance=ep)
        if episode_of_care_form.is_valid():
            episode_of_care_form.save()

    # Patient history
    allergies = Allergy_Intolerance.objects.filter(subject=encounter.patient)
    conditions = Condition.objects.filter(subject=encounter.patient)
    fmh = Family_Member_History.objects.filter(subject=encounter.patient)

    # Organizations
    ambulance_orgs = Organization.objects.filter(~Q(type=3),~Q(type=4),~Q(pk=request.user.employee.org.pk))
    hospital_orgs = Organization.objects.filter(~Q(type=3),~Q(type=2),~Q(pk=request.user.employee.org.pk))

    req = Request.objects.filter(encounter=encounter)    

    return render(request, 'call_centre/emergency_session.html',{
        "encounter" : encounter,
        "encounters" : encounters,
        "episode_of_care_form" : episode_of_care_form,
        "ep" : ep,
        "allergies" : allergies,
        "conditions" : conditions,
        "fmh": fmh,
        "hospital_orgs" : hospital_orgs,
        "ambulance_orgs" : ambulance_orgs,
        "reqs" : req,
        "keyid" : encounter.patient.key_id,
        "patient" : encounter.patient.pk,
        "updated" : ep.updated,
        "json_id" : ep.json_id,
        "internal_ip" : internal_ip,
        "organisation_name" : "{} ({})".format(ep.managing_organization.name, ep.managing_organization.alias),
        "organisation_address" : "{}, {}, {}".format(ep.managing_organization.address, ep.managing_organization.zipcode, ep.managing_organization.country),
        "organisation_phone" : ep.managing_organization.telecom,
        "ep_start_datetime" : str(ep.start_datetime),
        "ep_accepted_datetime" : str(ep.accepted_date), 
        "ep_end_datetime" : str(ep.end_datetime)
    })

@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def update_episode_of_care_info(request, id=None):
    check_call_center_permission(request.user.user_type)
    encounter = get_object_or_404(Encounter, pk=id)

    if request.method == "POST":
        print("updating ep info")
        encounter = get_object_or_404(Encounter, pk=id)
        json_id = request.POST["json_id"]
        print(json_id)
        ep = get_object_or_404(Episode_Of_Care, care_manager=request.user.employee, encounter=encounter)
        ep.json_id = json_id
        ep.updated = True
        ep.save()

        return JsonResponse({})

##############################################################################################
#########################  Request Service  ###########################################################
@csrf_exempt
@login_required(login_url='{}'.format(default_login_url))
def request_service(request, id=None):
    check_call_center_permission(request.user.user_type)
    encounter = get_object_or_404(Encounter, pk=id)

    if request.method == "POST":
        encounter = get_object_or_404(Encounter, pk=id)
        org = get_object_or_404(Organization, pk=request.POST["org"])
        request = Request.objects.create(
            encounter=encounter,
            request_type=1,
            org_requesting=request.user.employee.org,
            org_requested=org,
            answered=False,
            request_date=datetime.now(pytz.timezone(local_tz))
        )
        return JsonResponse({})

##############################################################################################
#########################  Team  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def team(request):
    check_call_center_permission(request.user.user_type)
    return render(request, 'call_centre/team.html')

##############################################################################################
#########################  Profile  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def profile(request):
    check_call_center_permission(request.user.user_type)
    return render(request, 'call_centre/profile.html')

##############################################################################################
#########################  Logout  ###########################################################
@login_required(login_url='{}'.format(default_login_url))
def logout(request):
    check_call_center_permission(request.user.user_type)
    logout_url = settings.OIDC_OP_LOGOUT_ENDPOINT
    return_to_url = request.build_absolute_uri(settings.LOGOUT_REDIRECT_URL)
    django_logout(request)
    return redirect(logout_url + '?' + urlencode({'redirect_uri': return_to_url, 'client_id': settings.OIDC_RP_CLIENT_ID}))