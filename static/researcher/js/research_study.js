var keycloak = Keycloak({
    url: `http://${internal_ip}:8181/auth`,
    realm: 'master',
    clientId: 'amc-client',
});

var research_dataset_rows = []

function retrieve_sse_keys(uuid, feature) {
    console.log("Retrieving SSE keys...");
    console.log(uuid)
    sse_keys = getSSEkeys(uuid , username, keycloak.token);
    var result = JSON.parse(sse_keys);
    enc_key = result["encKey"];
    ver_key = result["verKey"];
    console.log(uuid);
    console.log(enc_key);
    console.log(ver_key);
    retrieve_feature(ver_key, enc_key, uuid, feature);
}

function retrieve_feature(ver_key, enc_key, keyid, feature){
    console.log (`Retrieving feature ${feature}...`);
    result = search({'keyword': [`feature_name|${feature}`]}, ver_key, enc_key, keyid, iskey=true, isfe=false, keycloak.token);
    console.log(result);    

    for (const [key, value] of Object.entries(result.objects)) {
        research_dataset_rows.push([value["feature_name"], value["feature_value"]]);
    }    
}

function download_csv(rows){
    let csvContent = "data:text/csv;charset=utf-8,";

    console.log(research_dataset_rows);

    research_dataset_rows.forEach(function(rowArray) {
        let row = rowArray.join(",");
        csvContent += row + "\r\n";
    });

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "research_dataset.csv");
    document.body.appendChild(link);

    link.click();
}

function initKeycloak() {                
    keycloak.init({ onLoad: 'login-required' }).then(function(authenticated) {
        //alert(authenticated ? 'Authenticated' : 'Not authenticated');                    
        keycloak.loadUserProfile().then(function(profile) {
            username = profile["username"];
        }).catch(function() {
            //alert('Failed to load user profile');
        });
    }).catch(function() {
        //alert('Failed to initialize');
    });                                
}

function refresh_token(){
    keycloak.updateToken(30).then(function() {
        //console.log("Token refreshed succesfuly!");
        downloadFeatures();
    }).catch(function() {
        //alert('Failed to refresh token');
    });
}


function download_features(){
    // $("#loadMe").modal({
    //     backdrop: "static", //remove ability to close modal with click
    //     keyboard: false, //remove option to close with keyboard
    //     show: true //Display loader!
    // });    
    console.log("Downloading chosen features!");    
    var count = 0;
    for (var i = 0; i < features_id_list.length; i++) {                    
        if ($(`#${features_id_list[i]}`).prop('checked')) {
            retrieve_sse_keys(keys_id_list[i], features_id_list[i])
            count++;
        }
    }

    if (count == 0){
        alert('Please select at least 1 Feature to download');
    }else{
        download_csv(research_dataset_rows);
        // setInterval(function(){        
        //     $("#loadMe").modal("hide");
        // }, 1000);
        window.alert("Features downloaded.")
    }

}