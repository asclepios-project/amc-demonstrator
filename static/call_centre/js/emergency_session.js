var keycloak = Keycloak({
    // url: `http://${internal_ip}:8181/auth`,
    url: `http://161.74.31.93/auth`,
    realm: 'master',
    clientId: 'amc-client',                    
});

var username;
var ver_key;
var enc_key;

var episode_of_care = null;

function initKeycloak() {                
    keycloak.init({ onLoad: 'check-sso' }).then(function(authenticated) {
        //alert(authenticated ? 'Authenticated' : 'Not authenticated');                    
        keycloak.loadUserProfile().then(function(profile) {
            username = profile["username"];
            console.log("Keycloak initialized")
            console.log(key_id);
            retrieve_sse_keys(key_id);            
        }).catch(function() {
            //alert('Failed to load user profile');
        });
    }).catch(function() {
        //alert('Failed to initialize');
    });                                
}

function refresh_token(){
    keycloak.updateToken(30).then(function() {
        //console.log("Token refreshed succesfuly!");
        downloadFeatures();
    }).catch(function() {
        //alert('Failed to refresh token');
    });
    return
}

function retrieve_sse_keys(uuid) {
    console.log("Retrieving SSE keys...");
    console.log(uuid)
    console.log(keycloak.token)
    console.log(username)
    sse_keys = getSSEkeys(uuid , username, keycloak.token);
    var result = JSON.parse(sse_keys);
    enc_key = result["encKey"];
    ver_key = result["verKey"];    
    keyword = "subject|"+user_related
    retrieve_data({"keyword":keyword})
    
    if (updated == "True"){      
      keyword = "patient|"+user_related;
      retrieve_episode_of_care({"keyword":keyword});      
    }
    return
}

function retrieve_episode_of_care(search_keyword){  
  console.log ("Retrieving episode of care...");
  result = search(search_keyword, ver_key, enc_key, key_id, iskey=true, isfe=false, keycloak.token);
  console.log(result.objects);
  for (const obj in result.objects){
    if (result.objects[obj].json_id == ep_json_id){    
      document.getElementById("last_time_seen_well").innerHTML = result.objects[obj]["last_time_seen_well"];
      document.getElementById("time_of_the_onset").innerHTML = result.objects[obj]["time_of_the_onset"];
      document.getElementById("local_of_the_onset").innerHTML = result.objects[obj]["local_of_the_onset"];
      document.getElementById("patient_location").innerHTML = result.objects[obj]["patient_location"];
      document.getElementById("who_called").innerHTML = result.objects[obj]["who_called"];
      document.getElementById("call_centre_remarks").innerHTML = result.objects[obj]["call_centre_remarks"];
      episode_of_care = result.objects[obj]      
    }  
  }  

  document.getElementById("loader_ep").style.display = 'none';
  document.getElementById("form").style.display = 'block';
}

function retrieve_data(search_keyword){
    console.log ("Retrieving data...");
    result = search(search_keyword, ver_key, enc_key, key_id, iskey=true, isfe=false, keycloak.token);
    console.log(result.objects);
    if (result.count == 0){
      document.getElementById("loader").style.display = 'none'
      document.getElementById("no_records").style.display = 'block';
    }
    else{
      var allergy_html = "";
      var condition_html = "";
      var fmh_html = "";
      for (const obj in result.objects){
        innHtml = "";
        //console.log("going");
        if(result.objects[obj]["model"] == "login.allergy_intolerance"){
            innHtml += '<a id="div-"'+ result.objects[obj]["json_id"] +' class="list-group-item list-group-item-action flex-column align-items-start">';
            innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'            

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["category"]] +'</h6></div>'            

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["type"]] +'</h6></div>'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">' + CODES[result.objects[obj]["code"]] +'</h6></div>'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["criticality"]] +'</h6></div>'

            innHtml += '</div></a>'

            allergy_html += innHtml      
        }

        if(result.objects[obj]["model"] == "login.condition"){
            innHtml += '<a id="div-"'+ result.objects[obj]["json_id"] +' class="list-group-item list-group-item-action flex-column align-items-start">';
            innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ CODES[result.objects[obj]["code"]] +'</h6></div>'            

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["clinicalStatus"]] +'</h6></div>'            

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ CODES[result.objects[obj]["bodySite"]] +'</h6></div>'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px;">';
            innHtml += '<h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["severity"]] +'</h6></div>'            

            innHtml += '</div></a>'

            condition_html += innHtml
            
        }

        if(result.objects[obj]["model"] == "login.family_member_history"){
            innHtml += '<a id="div-"'+ result.objects[obj]["json_id"] +' class="list-group-item list-group-item-action flex-column align-items-start">';
            innHtml += '<div class="row" style="text-align: center; padding: 0px; margin: 0px;">'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px;">';
            innHtml += '<h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["relationship"]] +'</h6></div>'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ OTHER_CODES[result.objects[obj]["sex"]] +'</h6></div>'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ result.objects[obj]["age"] +'</h6></div>'

            innHtml += '<div class="col-3 mt-auto mb-auto" style="padding: 0px"><h6 class="mb-1" style="padding: 0px; font-weight: bold; font-size: 14px;">'+ CODES[result.objects[obj]["condition"]] +'</h6></div>'          

            innHtml += '</div></a>'

            fmh_html += innHtml
        }

      }

      //console.log("going 2");
      var element = document.getElementById("allergy_int_objects");
      element.innerHTML = allergy_html;

      var element = document.getElementById("condition_objects");
      element.innerHTML = condition_html;

      var element = document.getElementById("fmh_objects");
      element.innerHTML = fmh_html;

      document.getElementById("loader").style.display = 'none';
      document.getElementById("medical_records").style.display = 'block';
    }    
    return
}

function upload_data(json, fileid){
    episode_of_care = json;
    uploadData(json, fileid.toString(), ver_key, enc_key, key_id, iskey=true, keycloak.token);
    $.ajax({
      type: 'POST',
      url: 'update_episode_of_care_info',
      data: {"json_id" : fileid},
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(data) {
        console.log("upload success");
      }
    });
    return
}

function update_data(old_json, new_json, fileid){
    //uploadData(json, fileid.toString(), ver_key, enc_key, key_id, iskey=true, keycloak.token);
    //data, file_id, sharedKey, Kenc, keyid, iskey=false, token="", callback=undefined
    json = {}

    Object.keys(old_json).forEach(function(key){
      if (old_json[key] != new_json[key]){
        json[key] = [old_json[key], new_json[key]]
      }              
    });

    console.log(json);
    updateData(json, fileid.toString(), ver_key, enc_key, key_id, iskey=true, keycloak.token);
    return    
}

form.addEventListener("submit", function(e) {
  e.preventDefault();
  var data = new FormData(form);
  var json = {}
  for (const [name,value] of data) {    
    json[name] = value    
  }
  fileid = makeid(16);
  //console.log(fileid)

  json["json_id"] = fileid;
  json["model"] = "login.episode_of_care";
  json["tag"] = "call centre";
  json["patient"] = user_related;
  json["encounter"] = encounter;
  json["organisation_name"] = name;
  json["organisation_address"] = address;
  json["organisation_phone"] = phone;
  json["accepted_date"] = start_datetime;
  json["start_datetime"] = start_datetime;
  json["end_datetime"] = end_datetime;
  //console.log(json);
  $("#loadMe").modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
  });

  if (episode_of_care == null){
    upload_data(json, fileid);      
  }else{
    console.log("updating data")
    json["json_id"] = episode_of_care["json_id"];
    console.log(json);
    console.log(episode_of_care);
    update_data(episode_of_care, json, episode_of_care["json_id"]);  
  }

  setInterval(function(){    
    $("#loadMe").modal("hide");
  }, 1000);
  
});

function makeid(length) {
    var result           = [];
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result.push(characters.charAt(Math.floor(Math.random() * 
 charactersLength)));
   }
   return result.join('');
}