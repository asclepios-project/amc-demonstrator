var id_tmp;
$('.cardlist a #identdiv').click(function() {
  id_tmp = $(this).attr('data-id'); 
});

function add_to_team(btn){
    //console.log(btn.id);
    var answer = window.confirm("Do you really want to add to this team?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'add_to_team',
        data: { id: btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function() {
            location.reload(true);
        }
      });
    }
}

function delete_from_team(btn){
    //console.log(btn.id);
    var answer = window.confirm("Do you really want to add to this team?");
    if (answer){
      $.ajax({
        type: 'POST',
        url: 'add_to_team',
        data: { id: btn.id },
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function() {
            location.reload(true);
        }
      });
    }
}

function add_to_team_organization(btn){
  //console.log(btn.id);
    $.ajax({
      type: 'POST',
      url: 'add_to_team',
      data: { id: btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          location.reload(true);
          alert('Added to the team!');
      }
    });
}



function req_transference_to_org(btn){
  //console.log(btn.id);
  var answer = window.confirm("Send request to this org?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'send_request_to_org',
      data: { req_id: btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          location.reload(true);
      }
    });
  }
}

function accept_transference_from_org(btn){
  //console.log(btn.id);
  var answer = window.confirm("Accept request from this org?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'accept_request_from_org',
      data: { req_id: btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          location.reload(true);
      }
    });
  }
}

function attr_team(btn){
  //console.log(btn.id);
    $.ajax({
      type: 'POST',
      url: 'attr_team',
      data: { team_id: btn.id,
        req_id : id_tmp },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          alert('Assigned!');
          location.reload(true);
      }
    });
}

function attr_patient_request_team(btn){
  //console.log(btn.id);
    $.ajax({
      type: 'POST',
      url: 'attr_patient_request_team',
      data: { pat_pk: btn},
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          alert('Patient selected !');
          location.href="/org/home/Emergency-Request/";
      }
    });
}

function pick_amb_org(btn){
  console.log(id_tmp);
  var answer = window.confirm("Request this organization to do the transfer?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'req_amb_org',
      data: { org_id: btn.id,
        req_id : id_tmp },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          location.reload(true);
      }
    });
  }
}

function start_emergency_session(){
    $.ajax({
      type: 'POST',
      url: 'create_encounter',
      data: {},
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(data) {
          location.href ='Start-Emergency-Session/'+ data["id"] + '/';
      }
    });
}

function end_encounter(btn){
  console.log(btn.id);
  var answer = window.confirm("Discharge patient and end session?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'end_encounter',
      data: { req_id : btn.id,
      },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          alert('Patient discharged');
          location.reload(true);
      }
    });
  }
}

function delete_team(){
  var answer = window.confirm("Are you sure you want to delete this team?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'delete',
      data: {},
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          location.href="/org/home/Teams/Manage";
      }
    });
  }
}

function deactivate_team(){
  var answer = window.confirm("Do you really want to deactivate this team?");
  if (answer){
    location.reload(true);
  }
}

// AMBULANCE ORG FUNCTS
function accept_transfer(btn){
  var answer = window.confirm("Accept this transfer?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'accept_request_from_org',
      data: { req_id : btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function() {
          location.reload(true);
      }
    });
  }
}