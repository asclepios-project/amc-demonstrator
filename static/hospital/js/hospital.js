var myWindow = null;

function changing_episode(id){

  $('#sect > div').map(function() {
    this.style.display = "none";
  });

  var y = document.getElementById((id+"_var"));
  y.style.display = "block";
  
}

function retrieve(id){
  url = window.location.href + "Summary/" + id +"/";
  open(url, "", "height=720, width=500");
}  

function req_transfer(btn){
    var answer = window.confirm("Request transference?");
      if (answer){
        $.ajax({
          type: 'POST',
          url: 'resq_transference',
          data: {
            org_id : btn.id
          },
          csrfmiddlewaretoken: '{{ csrf_token }}',
          success: function() {
              location.reload(true);
          }
        });
      }
  }

  function add_to_t(btn){
    var answer = window.confirm("Add to team?");
      if (answer){
        $.ajax({
          type: 'POST',
          url: 'add_to_team',
          data: {
            employee_id : btn.id
          },
          csrfmiddlewaretoken: '{{ csrf_token }}',
          success: function() {
              alert("added");
              location.reload(true);
          }
        });
      }
  }

  

  function retrieve_eps(id, new_ep){
    if (myWindow == null){
      myWindow = window.open("", "newWindow", "width=500,height=720");
    }else{
      myWindow.close();
      myWindow = window.open("", "newWindow", "width=500,height=720");
    }
    myWindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">');
    myWindow.document.write("<div class='container'>"),
    myWindow.document.write("<div class='row'>"),
        $.ajax({
          type: 'POST',
          url: 'get_eps',
          data: {
            id : id
          },
          csrfmiddlewaretoken: '{{ csrf_token }}',
          success: function(data) {
              if (data.length == 0){
                myWindow.close();
                alert("This emergency does not have any record");
              }else{
                if (new_ep == true){
                  var len = data.length - 1;
                }else{
                  var len = data.length;
                }

                for (var i = 0; i < (len); i++){
                  myWindow.document.write("<div class='col-sm'>");
                  myWindow.document.write("<h5 style='font-weight: bold;'>" + data[i].fields.episode_of_care_tag + "</h5>");
  
                  if (data[i].fields.episode_of_care_tag == "Call Centre"){
                    data[i].fields.last_time_seen_well = data[i].fields.last_time_seen_well || '-';
                    data[i].fields.time_of_the_onset = data[i].fields.time_of_the_onset || '-';
                    data[i].fields.local_of_the_onset = data[i].fields.local_of_the_onset || '-';
                    data[i].fields.local_patient = data[i].fields.local_patient || '-';
                    data[i].fields.who_called = data[i].fields.who_called || '-';
                    data[i].fields.call_centre_remarks = data[i].fields.call_centre_remarks || '-';
                    
                    myWindow.document.write("<h6 style='font-family: times;'>Last time seen well<strong style='padding:32px'>" + data[i].fields.last_time_seen_well + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Time of the onset<strong style='padding:32px'>" + data[i].fields.time_of_the_onset + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Local of the onset<strong style='padding:32px'>" + data[i].fields.local_of_the_onset + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Patient location<strong style='padding:32px'>" + data[i].fields.local_patient + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Who called<strong style='padding:32px'>" + data[i].fields.who_called + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Call centre remarks<strong style='padding:32px'>" + data[i].fields.call_centre_remarks + "</strong></h6>");
                  }
  
                  if (data[i].fields.episode_of_care_tag == "Ambulance"){
                    data[i].fields.time_of_arrival = data[i].fields.time_of_arrival || '-';
                    data[i].fields.heart_rate = data[i].fields.heart_rate || '-';
                    data[i].fields.blood_pressure_systolic = data[i].fields.blood_pressure_systolic || '-';
                    data[i].fields.blood_pressure_diastolic = data[i].fields.blood_pressure_diastolic || '-';
                    data[i].fields.oxygen_saturation = data[i].fields.oxygen_saturation || '-';
                    data[i].fields.breathing_frequency = data[i].fields.breathing_frequency || '-';
                    data[i].fields.glucose = data[i].fields.glucose || '-';
                    data[i].fields.usage_of_coagulant_medicine = data[i].fields.usage_of_coagulant_medicine || '-';
                    data[i].fields.international_normalized_ratio = data[i].fields.international_normalized_ratio || '-';
                    data[i].fields.physical_evaluation = data[i].fields.physical_evaluation || '-';
                    data[i].fields.emv_score_eyes = data[i].fields.emv_score_eyes || '-';
                    data[i].fields.emv_score_motor = data[i].fields.emv_score_motor || '-';
                    data[i].fields.emv_score_verbal = data[i].fields.emv_score_verbal || '-';
                    data[i].fields.ABCDE_stability_circulation = data[i].fields.ABCDE_stability_circulation || '-';
                    data[i].fields.ABCDE_stability_disability = data[i].fields.ABCDE_stability_disability || '-';
                    data[i].fields.ABCDE_stability_breathing = data[i].fields.ABCDE_stability_breathing || '-';
                    data[i].fields.ABCDE_stability_airways = data[i].fields.ABCDE_stability_airways || '-';
                    data[i].fields.ABCDE_stability_exposure = data[i].fields.ABCDE_stability_exposure || '-';


                    myWindow.document.write("<h6 style='font-family: times;'>Time of the arrival<strong style='padding:32px'>" + data[i].fields.time_of_arrival + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Heart rate<strong style='padding:32px'>" + data[i].fields.heart_rate + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Blood pressure systolic<strong style='padding:32px'>" + data[i].fields.blood_pressure_systolic + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Blood pressure diastolic<strong style='padding:32px'>" + data[i].fields.blood_pressure_diastolic + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Oxygen saturation<strong style='padding:32px'>" + data[i].fields.oxygen_saturation + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Breathing frequency<strong style='padding:32px'>" + data[i].fields.breathing_frequency + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Glucose<strong style='padding:32px'>" + data[i].fields.glucose + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Coagulant medicine<strong style='padding:32px'>" + data[i].fields.usage_of_coagulant_medicine + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>International normalized ratio<strong style='padding:32px'>" + data[i].fields.international_normalized_ratio + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>EMV score: eyes | motor | verbal<strong style='padding:32px'>" + data[i].fields.emv_score_eyes + " | " +data[i].fields.emv_score_motor +  " | " +data[i].fields.emv_score_verbal +"</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>ABCDE stability airways | breathing | circulation | disability | exposure<strong style='padding:32px'>" + data[i].fields.ABCDE_stability_airways +  " | " +data[i].fields.ABCDE_stability_breathing +
                    " | " +data[i].fields.ABCDE_stability_circulation +  " | " +data[i].fields.ABCDE_stability_disability +  " | " +data[i].fields.ABCDE_stability_exposure +"</strong></h6>")
                    myWindow.document.write("<h6 style='font-family: times;'>Physical evaluation<strong style='padding:32px'>" + data[i].fields.physical_evaluation + "</strong></h6>");
                  }
  
                  if (data[i].fields.episode_of_care_tag == "Hospital"){
                    data[i].fields.neurological_evaluation = data[i].fields.neurological_evaluation || '-';
                    data[i].fields.radiological_imaging = data[i].fields.radiological_imaging || '-';
                    data[i].fields.image_thrombus_location = data[i].fields.image_thrombus_location || '-';
                    data[i].fields.image_aspects = data[i].fields.image_aspects || '-';
                    data[i].fields.image_collateral_score = data[i].fields.image_collateral_score || '-';
                    data[i].fields.infart_core_size = data[i].fields.infart_core_size || '-';
                    data[i].fields.penumbra_size = data[i].fields.penumbra_size || '-';
                    data[i].fields.medication = data[i].fields.medication || '-';
                    data[i].fields.procedure = data[i].fields.procedure || '-';
                    data[i].fields.emv_score_eyes = data[i].fields.emv_score_eyes || '-';
                    data[i].fields.emv_score_motor = data[i].fields.emv_score_motor || '-';
                    data[i].fields.emv_score_verbal = data[i].fields.emv_score_verbal || '-';
                    data[i].fields.ABCDE_stability_circulation = data[i].fields.ABCDE_stability_circulation || '-';
                    data[i].fields.ABCDE_stability_disability = data[i].fields.ABCDE_stability_disability || '-';
                    data[i].fields.ABCDE_stability_breathing = data[i].fields.ABCDE_stability_breathing || '-';
                    data[i].fields.ABCDE_stability_airways = data[i].fields.ABCDE_stability_airways || '-';
                    data[i].fields.ABCDE_stability_exposure = data[i].fields.ABCDE_stability_exposure || '-';

                    myWindow.document.write("<h6 style='font-family: times;'>Neurological evaluatiom<strong style='padding:32px'>" + data[i].fields.neurological_evaluation + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Radiological imaging<strong style='padding:32px'>" + data[i].fields.radiological_imaging + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Image thrombus location<strong style='padding:32px'>" + data[i].fields.image_thrombus_location + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Image aspects<strong style='padding:32px'>" + data[i].fields.image_aspects + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Image collateral score<strong style='padding:32px'>" + data[i].fields.image_collateral_score + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Infart core size<strong style='padding:32px'>" + data[i].fields.infart_core_size + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Penumbra size<strong style='padding:32px'>" + data[i].fields.penumbra_size + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Medication<strong style='padding:32px'>" + data[i].fields.medication + "</strong></h6>");
                    myWindow.document.write("<h6 style='font-family: times;'>Procedure<strong style='padding:32px'>" + data[i].fields.procedure + "</strong></h6>");
                
                  }
  
                  myWindow.document.write("</div>");
                }
                myWindow.document.write("</div>");
                myWindow.document.write("</div>");
              }
          }
        })   
}