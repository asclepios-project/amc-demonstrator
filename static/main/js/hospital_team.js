function start_emergency_session(){
    console.log();
    var answer = window.confirm("Do you really want to start an emergency session?");
    if (answer){
        $.ajax({
        type: 'POST',
        url: '/main/Hospital-Team/create_encounter/',
        data: {},
        csrfmiddlewaretoken: '{{ csrf_token }}',
        success: function(response) {
            if (response["success"] == 200){
              window.location.href = "Emergency-Session/"+response["encounter_pk"];
            }
          }
      });
    }
}

function choose_patient(btn){
  console.log(btn.id);
  var answer = window.confirm("Are you sure that you selected the correct patient?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'attr_patient/'+btn.id+'/',
      data: { pk: btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(response) {
        if(response["success"] == 200){
          alert('Patient selected !');
          window.location = window.location.href.split("?")[0];
        }
      }
    });
  }
}

function close_emergency(btn){
  console.log(btn.id);
  var answer = window.confirm("Are you sure that you want to end this session?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'end_emergency/'+btn.id+'/',
      data: { pk: btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(response) {
        if(response["success"] == 200){
          alert('Session closed!');
          location.href='/main/Hospital-Team/';
        }
      }
    });
  }
}

function accept_emergency(btn){
  console.log(btn.id);
  var answer = window.confirm("Are you sure you want to accept this invite?");
  if (answer){
    $.ajax({
      type: 'POST',
      url: 'accept_emergency',
      data: { pk: btn.id },
      csrfmiddlewaretoken: '{{ csrf_token }}',
      success: function(response) {
          if(response["success"] == 200){
            alert('You have joined to this emergency!');
            location.reload(true);
          }
      }
    });
  }
}